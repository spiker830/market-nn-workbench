<?php

/* ========================================================================== *
 *
 * 
 *
 * 	@author: Jonathan Spiker
 *
 * ========================================================================== */

class UserLevelPrivilege extends DBObject
{
	/**
	 * Return the privilege item specified by the provided privilege id and
	 * employee level id.
	 * 
	 * @param int $levelId
	 * @param int $privilegeId
	 * @return boolean
	 */
	public static function findByLevel($levelId, $privilegeId)
	{
		// Make sure ids are safe
		if (!DB::isSafeValue($levelId))
		{
			Log::error("Tried to use unsupported id: " . $levelId);
			return false;
		}
		else if (!DB::isSafeValue($privilegeId))
		{
			Log::error("Tried to use unsupported id: " . $privilegeId);
			return false;
		}
		
		// Get the privilege item for the specified privilege id and level id
		$query = "select
			privilege.id,
			:level as employee_level_id,
			(employee_level_id is not null) as granted,
			name,
			description
			from
				(select
				privilege_id,
				employee_level_id
				from privilege_level
				where employee_level_id = :level)
			as privileges_for_level
			right join privilege
			on privileges_for_level.privilege_id = privilege.id
			where privilege.id = :privilege";
		
		$rows = DB::select($query, array(
			":level" => $levelId,
			":privilege" => $privilegeId
		));
		
		$privileges = static::rowsToDBObjects($rows, static::getClass());
		
		return $privileges[0];
	}
	
	public static function allByLevel($levelId)
	{
		// Make sure id is safe
		if (!DB::isSafeValue($levelId))
		{
			Log::error("Tried to use unsupported id: " . $levelId);
			return false;
		}

		// Get all privileges for the given level id
		$query = "select
			privilege.id,
			:level as employee_level_id,
			(employee_level_id is not null) as granted,
			name,
			description
			from
				(select
				privilege_id,
				employee_level_id
				from privilege_level
				where employee_level_id = :level)
			as privileges_for_level
			right join privilege
			on privileges_for_level.privilege_id = privilege.id";
		
		$rows = DB::select($query, array(
			":level" => $levelId,
		));
		
		$configItems = static::rowsToDBObjects($rows, static::getClass());
		
		return $configItems;
	}
	
	/**
	 * 
	 * @param type $db
	 * @return bool True on success, false on failure
	 */
	public function save()
	{
		$this->validate();
		
		// Just need to make sure the database reflects the current enabled state
		
		// Do we want to grant the privilege?
		$grant = $this->granted;
		
		// Is the privilege already granted?
		$query = "select
			*
			from privilege_level
			where privilege_id = :privilege
			and employee_level_id = :level";
		
		$granted = DB::select($query, array(
			":privilege" => $this->id,
			":level" => $this->employee_level_id
		));
		
		$success = false;
		
		// If privilege has already been granted
		if ($granted)
		{
			// And we want to revoke it
			if (!$grant)
			{
				$privilege_level = $granted[0]["id"];
		
				// Do delete
				$success = DB::delete("privilege_level", $privilege_level);
			}	
		}
		// If privilege hasn't already been granted
		else if (!$granted)
		{
			// And we want to grant it
			if ($grant)
			{
				// Do insert
				$success = DB::insert("privilege_level", array(
					"privilege_id" => $this->id,
					"employee_level_id" => $this->employee_level_id
				));
			}	
			
		}
		
		return $success;
	}

	public function validate()
	{
	}
	
	/**
	 * Determine if the specified employee is a direct report of the currently
	 * logged in user.
	 * 
	 * @param type $employeeId
	 * @return type
	 */
	public static function isDirectReportOfCurrent($employeeId)
	{
		$isDirectReport = false;
		
		// Get the employee id of the current user
		$curEmployeeId = Session::get('employeeId');
		
		$statement = DB::instance()->prepare("SELECT employeeId FROM direct_report
			WHERE employeeId = :employeeId
			AND directReportId = :directReportId");

		$statement->execute(array(
				':employeeId' => $curEmployeeId,
				':directReportId' => $employeeId
		));
		
		if ($statement->rowCount() > 0)
		{
			$isDirectReport = true;
		}

		return $isDirectReport;
	}

	/**
	 * Determine if a user is authorized to perform a specified action.
	 * 
	 * @param type $actionString
	 * @return bool
	 */
	public static function authorized($actionString)
	{
		$levelId = Session::get('level');
		$privilegeName = strtoupper(str_replace(' ', '_', $actionString));
		
		// Is the privilege granted?
		$query = "select
			*
			from privilege_level
			left join privilege
			on privilege_level.privilege_id = privilege.id
			where privilege.name = :privilege
			and employee_level_id = :level";
		
		$granted = DB::select($query, array(
			":privilege" => $privilegeName,
			":level" => $levelId
		));
		
		// Turn array into actual boolean
		return $granted ? true : false;
	}
	
	/**
	 * Return an array of levels authorized to perform the specified action.
	 * 
	 * @param string $actionString
	 * @return array
	 */
	public static function authorizedLevels($actionString)
	{
		$levels = array();
		
		$privilegeName = strtoupper(str_replace(' ', '_', $actionString));
		
		$query = "select
			distinct employee_level.level
			from privilege_level
			left join privilege
			on privilege_level.privilege_id = privilege.id
			left join employee_level
			on privilege_level.employee_level_id = employee_level.id
			where privilege.name = :privilege";
		
		$authorizedRow = DB::select($query, array(
			":privilege" => $privilegeName
		));
		
		foreach ($authorizedRow as $authorized)
		{
			$levels[] = $authorized["level"];
		}
		
		return $levels;
	}
	
	public static function canEnterTimeFor($employeeId) {
		
		$isDirectReport = static::isDirectReportOfCurrent($employeeId);
		$isSameEmployee = Session::get('employeeId') == $employeeId;
		$canEnterForAny = static::authorized("enter time for any");
		
		return ($isDirectReport || $isSameEmployee || $canEnterForAny);
		
	}
	
	public static function canBeDirectReport($levelName)
	{
		$action = strtoupper(str_replace(' ', '_', "be direct report"));

		return in_array($levelName, static::authorizedLevels($action));
	}
	
	public static function canHaveDirectReport($employeeId)
	{
		// Get level name
		$levelName = static::getLevelName($employeeId);
		
		// Check authorized levels array
		return in_array($levelName, static::authorizedLevels("HAS_REPORTS"));
	}

	private static function getLevelName($employeeId)
	{
		$statement = $this->db->prepare("SELECT employee_level.level
			FROM employee
			LEFT JOIN employee_level on employee.level = employee_level.id
			WHERE employee.id = :id");

		$statement->execute(array(
				':id' => $employeeId
		));

		$row = $statement->fetch(PDO::FETCH_ASSOC);

		if ($row)
		{
			$levelName = $row['level'];
		}
		else
		{
			$levelName = "user";
		}

		return $levelName;

	}
}

?>
