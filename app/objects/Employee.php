<?php

/* ========================================================================== *
 *
 * 	
 *
 * 	@author: Jonathan Spiker
 *
 * ========================================================================== */

class Employee extends DBObject
{
	static $table = "employee";
	static $hidden = array(
		"password"
	);
	
	public function validate()
	{
		
	}
	
	/**
	 * Return the array of this DBObject's attributes.
	 * 
	 * @return array An array of attributes.
	 */
	protected function to_array()
	{
		$data = array();
		
		// If the user can't view the employee page
		if (!UserLevelPrivilege::authorized("view employee page"))
		{
			// Only include the name in the data
			$data = array(
				"id" => $this->id,
				"firstName" => $this->firstName,
				"lastName" => $this->lastName
			);
			
		}
		else // If the user can view the employee page
		{
			// Include all attributes
			foreach ($this->attributes as $key => $value)
			{
				if (!isset(static::$hidden) || !in_array($key, static::$hidden))
				{
					$data[$key] = $value;
				}
			}
		}
		
		return $data;
	}
	
}

?>
