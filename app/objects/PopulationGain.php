<?php

/* ========================================================================== *
 *
 * 	
 *
 * 	@author: Jonathan Spiker
 *
 * ========================================================================== */

class PopulationGain extends DBObject
{
	static $table = 'population';
	
	public static function getBaseQuery() {
		return "select population.generation, avg(base_gain) as base_gain, min(gain) as min_gain, avg(gain) as avg_gain, max(gain) as max_gain
			from population_predictor_genome
			left join predictor_genome on population_predictor_genome.predictor_genome_id = predictor_genome.id
			left join population on population_predictor_genome.population_id = population.id
			left join ticker on ticker.id = population.ticker_id
			where gain is not null";
	}
	
	public static function getGroupBy() {
		return " group by population.id ";
	}
	
	public function validate()
	{
		
	}

}

?>
