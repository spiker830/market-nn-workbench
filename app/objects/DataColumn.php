<?php

/* ========================================================================== *
 *
 * 	
 *
 * 	@author: Jonathan Spiker
 *
 * ========================================================================== */

class DataColumn extends DBObject {
	
	static $table = "information_schema.COLUMNS";
	
	public function validate() {
		
	}
	
	public static function getBaseQuery() {
		
		return "select distinct COLUMN_NAME as name from information_schema.COLUMNS where TABLE_SCHEMA = 'market_prediction'";
		
	}
}

?>
