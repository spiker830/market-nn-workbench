<?php

/* ========================================================================== *
 *
 * 	
 *
 * 	@author: Jonathan Spiker
 *
 * ========================================================================== */

class TransformChainTransform extends DBObject {
	
	static $table = "transform_chain_transform";
	
	public function validate() {
		
	}
	
	public static function getBaseQuery() {
		
		return "select transform_chain_transform.id, transform_id, transform.class_name, transform.name, transform_chain_id "
		. "from transform_chain_transform "
		. "left join transform on transform.id = transform_chain_transform.transform_id";
	}
}

?>
