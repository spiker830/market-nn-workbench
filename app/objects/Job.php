<?php

/* ========================================================================== *
 *
 * 	
 *
 * 	@author: Jonathan Spiker
 *
 * ========================================================================== */

class Job extends DBObject {

	public function validate() {
		
	}

	protected static function getBaseQuery() {
		
		return "select "
				. "(@cnt := @cnt + 1) as id,"
				. "job.id as job_id,"
				. "training_start_date,"
				. "training_end_date,"
				. "backtesting_start_date,"
				. "backtesting_end_date,"
				. "target_symbol,"
				. "prediction_window,"
				. "evaluation_window,"
				. "bear_percent,"
				. "bull_percent,"
				. "hidden_neurons_layer_1,"
				. "base_gain,"
				. "gain,"
				. "gain - base_gain as improvement,"
				. "training_size,"
//				. "num_input_symbols,"
//				. "target_in_input,"
				. "datediff(training_end_date, training_start_date) as training_length,"
				. "analyzers,"
				. "short_name "
//				. "group_concat(job_trading_strategy.trading_strategy_id, ' (', round((gain-1)*100, 2), ')' separator ', ') as gain,"
//				. "group_concat(job_trading_strategy.trading_strategy_id, ' (', round((gain-base_gain)*100, 2), ')' separator ', ') as improvement "
				. "from " . static::getTable() . " "
				. "left join job_trading_strategy on job.id = job_trading_strategy.job_id "
				. "left join trading_strategy on trading_strategy.id = job_trading_strategy.trading_strategy_id "
				. "cross join (select @cnt := 0) as dummy"
				;
		
	}
	
	public static function create($data) {
		
		// Create job
		$jobConfig = array(
			"hidden_layers" => $data["hidden_layers"],
			"rate" => $data["rate"],
			"epochs" => $data["epochs"],
			"training_ratio" => $data["training_ratio"],
			"window_size" => $data["window_size"],
			"target_error" => $data["target_error"],
			"input_bias" => $data["input_bias"],
			"hidden_bias" => $data["hidden_bias"]
		);
		
		$job = parent::create($jobConfig);
		
		$jobId = $job->id;
		
		// Create job data
		$jobData = array(
			"job_id" => $jobId,
			"input_columns" => $data["input_columns"],
			"output_columns" => $data["output_columns"],
			"start_date" => $data["start_date"],
			"end_date" => $data["end_date"]
		);
		
		JobData::create($jobData);
		
		return $job;
	}

}

?>
