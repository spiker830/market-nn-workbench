<?php

/* ========================================================================== *
 *
 * 	
 *
 * 	@author: Jonathan Spiker
 *
 * ========================================================================== */

class AnalyzerOutput extends DBObject {
	
	static $table = "analyzer_output";

	public function validate() {
		
	}
	
	public static function all($filters = array()) {
		
		$analyzerIdFilter = static::getAnalyzerIdFilter($filters);
		
		if (!$analyzerIdFilter) {
			
			Log::error("Need to filter by analyzer_id when querying analyzer_output");
			
			return array();
		}
		
		$analyzerId = $analyzerIdFilter->getValue();
		$query = "select distinct date from analyzer_output where analyzer_id = " .
				$analyzerId;
		$dateRows = DB::query($query);
		
		$objects = array();
		
		foreach ($dateRows as $dateRow) {
			
			$rows = DB::query("select * from analyzer_output where analyzer_id = " .
					$analyzerId . " and date = '" . $dateRow["date"] . "'");
			
			$analyzerDataObject = array(
				"analyzer_id" => $analyzerId,
				"date" => $dateRow["date"]
			);
			
			Log::json($rows);
			
			foreach($rows as $seriesRow) {
			
				Log::json($seriesRow);
				
				$analyzerDataObject[$seriesRow["series_name"]] = $seriesRow["value"];
				
			}
			
			$objects[] = $analyzerDataObject;
		}
		
		return static::rowsToDBObjects($objects, static::getClass());
	}
	
	public static function getAnalyzerIdFilter($filters) {
		
		foreach ($filters as $filter) {
			
			if ($filter->getAttribute() === "analyzer_id") {
				
				return $filter;
				
			}
		}
		
		return false;
	}
}

?>
