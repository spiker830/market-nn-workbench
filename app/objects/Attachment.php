<?php

/* ========================================================================== *
 *
 * 
 *
 * 	@author: Jonathan Spiker
 *
 * ========================================================================== */

class Attachment extends DBObject
{
	static $table = "attachment";
	
	public function validate()
	{
		
	}
	
	public static function filter($filters)
	{
		Log::debug("Original filters:");
		Log::json($filters);
		
		if (isset($filters[0]))
		{
			$filter = $filters[0];
			
			if (!preg_match("/\.id$/", $filter->getAttribute()))
			{
				// Create filter for table
				$tableFilter = new Filter("object_table_name",
						Relation::EQUAL,
						str_replace("_id", "", $filter->getAttribute()));

				$filters[] = $tableFilter;

				// Create filter for id
				$idFilter = new Filter("object_id", Relation::EQUAL, $filter->getValue());

				$filters = array($tableFilter, $idFilter);
			}
			
		}
		
		return parent::filter($filters);
	}
	
	public static function create($data)
	{
		$object_table_name = null;
		$object_id = null;
		
		// Data is only going to have filters when doing a file upload
		foreach($data as $key => $value)
		{
			if (preg_match("/_id$/", $key))
			{
				$object_table_name = preg_replace("/_id$/", "", $key);
				$object_id = $value;
			}
		}
		
		$title = $_POST["title"];
		
		$file = $_FILES["qqfile"];
		$temporaryPath = $file["tmp_name"];
		$extension = pathinfo($file["name"], PATHINFO_EXTENSION);
		$filename = $object_id . "_" . time() . "." . $extension;
		
		// New create data
		$data = array(
			"object_table_name" => $object_table_name,
			"object_id" => $object_id,
			"title" => $title,
			"filename" => $filename
		);
		
		Log::json($data);

		$destinationPath = static::getFilePath($object_table_name, $filename);

		// TODO Create directories automatically
		// Attempt to move to permanent location
		$moved = move_uploaded_file($temporaryPath, $destinationPath);

		// If move succeeded
		if ($moved)
		{
			// Attempt to insert data into database
			$created = parent::create($data);
			
			Log::json($created);
			
			// If insert fails, delete file and return null
			if ($created == null)
			{
				unlink($destinationPath);
				
				return null;
			}
			else
			{
				// Hacky way to make sure fine uploader knows upload succeeded
				$created->success = true;
				
				return $created;
			}
			
		}
		else // If move failed
		{
			return null;
		}
	}
	
	/**
	 * 
	 * @return PDOStatement|bool PDOStatement object or false on failure.
	 */
	public function destroy()
	{
		$path = static::getFilePath($this->object_table_name, $this->filename);
		
		$success = parent::destroy();

		if (!$success)
		{
			Log::error("Failed to delete file: $this->filename");
		}
		else
		{
			return unlink($path);
		}
	}
	
	public static function getFilePath($object_table_name, $filename)
	{
		return Configuration::getValue("FILES_DIR") . "/$object_table_name/$filename";
	}
	
	public function download($attachment)
	{
		$filename = $attachment->filename;
		$directory = $attachment->object_table_name;
		
		$path = Configuration::getValue("FILES_DIR") . "/$directory/$filename";

		if (file_exists($path))
		{
			// @TODO Files should download using the original filename?
			
			header($_SERVER["SERVER_PROTOCOL"] . " 200 OK");
			header("Cache-Control: public"); // needed for i.e.
			header("Content-Type: " . mime_content_type($path));
			header("Content-Transfer-Encoding: Binary");
			header("Content-Length:" . filesize($path));
			header("Content-Disposition: attachment; filename=$filename");
			readfile($path);
		}
		else
		{
			throw new Exception("The requested file could not be found.");
		}
	}
}

?>
