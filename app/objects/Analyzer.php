<?php

/* ========================================================================== *
 *
 * 	
 *
 * 	@author: Jonathan Spiker
 *
 * ========================================================================== */

class Analyzer extends DBObject {
	
	static $fields = array(
		"class_name",
		"enabled",
		"ticker"
	);

	public function validate() {
		
	}
	
	public static function getBaseQuery() {
		
		DB::query("set session sql_mode='STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION'");
		
		return "select
			analyzer.id,
			analyzer.class_name,
			enabled,
			ticker,
			analyzer_data.transform_chain_id,
			group_concat(transform.name separator '_') as transform_chain_name,
			`table`,
			`column`
			from analyzer
			left join analyzer_data on analyzer_data.analyzer_id = analyzer.id
			left join transform_chain on transform_chain.id = analyzer_data.transform_chain_id
			left join transform_chain_transform on transform_chain_transform.transform_chain_id = analyzer_data.transform_chain_id
			left join transform on transform_chain_transform.transform_id = transform.id";
	}
	
	public static function getGroupBy() {
		return "group by analyzer.id";
	}
}

?>
