<?php

/* ========================================================================== *
 *
 * 
 *
 * 	@author: Jonathan Spiker
 *
 * ========================================================================== */

class Configuration extends DBObject
{
	public static function find($configId)
	{
		// Make sure config id is safe
		if (!DB::isSafeValue($configId))
		{
			Log::error("Tried to use unsupported id: " . $configId);
			return false;
		}

		// Get the configuration item for the specified config id
		$rows = DB::query("select
			config.id,
			config.name,
			config.description,
			config_type.name as type,
			config_value.value
			from config
			left join config_type on config_type.id = config.type
			left join config_value on config_value.config_id = config.id
			where config.id = $configId");
		
		$configs = static::rowsToDBObjects($rows, static::getClass());
		
		return $configs[0];
	}
	
	public static function all($filters = array())
	{
		// Get all configuration items
		$rows = DB::query("select
			config.id,
			config.name,
			config.description,
			config_type.name as type,
			config_value.value
			from config
			left join config_type on config_type.id = config.type
			left join config_value on config_value.config_id = config.id");

		$configItems = static::rowsToDBObjects($rows, static::getClass());
		
		return $configItems;
	}
	
	/**
	 * Get the value for a named configuration.
	 * 
	 * TODO: Need to cache these instead of asking for them so many times.
	 * 
	 * @param string $name The name of the configuration to retrieve
	 * @return string The configuration value
	 */
	public static function getValue($name)
	{
		// *** Since debug logging depends on configuration,
		// can't initiate debug statements from this function ;) ***
		
		// Get the value for the named configuration
		$rows = DB::query("select
			value, config_type.name as type
			from config_value
			left join config on config_value.config_id = config.id
			left join config_type on config.type = config_type.id
			where config.name = \"" . $name . "\"");
		
		$configItem = $rows->fetch();
		
		if ($configItem["type"] == "boolean")
		{
			return ($configItem["value"] == true);
		}
		else if ($configItem["type"] == "numeric")
		{
			return floatval($configItem["value"]);
		}
		else
		{
			return $configItem["value"];
		}
	}
	
	/**
	 * 
	 * @param type $db
	 * @return bool True on success, false on failure.
	 */
	public function save()
	{
		$this->validate();
		
		// Only need the updated value
		$data = array("value" => $this->value);
		
		// Do update
		return DB::updateById("config_value", $data, (int) $this->id);
	}
	
	public static function getConfigurationObject()
	{
		$config = array();
		
		$config['DEBUG'] = Configuration::getValue("DEBUG");
		$config['URL_BASE'] = Configuration::getValue("URL_BASE");
		
		$config['levelName'] = Session::get('levelName');
		
		$result = array('config' => $config);
		
		return $result;
	}

	public function validate()
	{
		if ($this->type == "numeric" && !is_numeric($this->value))
		{
			throw new RESTException("The value for this configuration must be numeric.");
		}
	}
}

?>
