
<div class="innerContent">

	<h2>Administration</h2>

	<h3>Configuration</h3>

	<div id="configuration" class="span12">

	</div>

	<h3>Authorization</h3>

	<div id="authorization" class="span12">

		<?php foreach (UserLevel::all() as $level): ?>

			<div class="well span2 privilege-level">
				<span><?php echo $level->level ?></span>
				<button title="View/Edit Privileges" class="btn btn-mini btn-primary pull-right edit-privileges">
					<i class="icon-white icon-cog"></i>
				</button>
				<input type="hidden" id="hidden-level-id" value="<?php echo $level->id ?>" />
				<input type="hidden" id="hidden-level-name" value="<?php echo $level->level ?>" />
			</div>

		<?php endforeach; ?>

	</div>

	<div class="modal hide" id="privileges-modal">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">×</button>
			<h3></h3>
		</div>
		<div class="modal-body">
			<div id="privilege-list">

			</div>
		</div>
		<div class="modal-footer">
			<a href="#" class="btn btn-primary save-privileges" >Save</a>
			<a href="#" class="btn done" data-dismiss="modal">Cancel</a>
		</div>
	</div>

</div>

<script type="text/template" id="configuration-item-template">
	<span style="font-weight: bold; font-size: 15px; height: 30px; display: inline-block"><%- name %></span>
	<button title="<%- name %>" data-content="<%- description %>" class="btn btn-mini btn-info pull-right">
		<i class="icon-white icon-info-sign"></i>
	</button>
	<br />
	<!--<br />-->
	<% if (type === "string")
	{
	%>
	<input type="text" style="margin-bottom: 0px" class="string" value="<%- value %>" />
	<%
	}
	else if (type === "numeric")
	{
	%>
	<input type="text" style="margin-bottom: 0px" class="numeric" value="<%- value %>" />
	<%
	}
	else if (type === "boolean")
	{
	%>
	<select style="margin-bottom: 0px" class="boolean">
		<option <% if (value == "1") { %> selected="selected" <% } %> value="1">True</option>
		<option <% if (value == "0") { %> selected="selected" <% } %> value="0">False</option>
	</select>
	<%
	}
	%>
	<button class="btn btn-primary pull-right save">
		<i class="icon-white icon-ok"></i>
		Save
	</button>
</script>

<script type="text/template" id="privilege-view-template">
	<input class="granted" type="checkbox" <% if (granted == 1) { %> checked="checked" <% } %> />
		   <button title="<%- name %>" data-content="<%- description %>" class="btn btn-mini btn-info">
		<i class="icon-white icon-info-sign"></i>
	</button>
	<span><%- name %></span>
</script>