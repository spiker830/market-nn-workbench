
<div class="innerContent">

	<div class="floatingHeader">Query Visualization</div>
	<div id="query-selection" class="row-fluid"></div>
	<div id="query-editor" class="row-fluid">
		<div style="height: 200px" id="editor">SELECT * FROM users</div>
	</div>
	<div id="query-control" class="row-fluid">
		<div id="query-name" class="span6"></div>
		<div id="query-save-execute" class="span6">
			<button id="execute" class="btn btn-primary">Execute</button>
		</div>
	</div>
	<div id="query-graph" class="row-fluid" style="height: 550px"></div>

</div>