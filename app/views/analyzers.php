
<div class="innerContent">

	<div class="floatingHeader">Analyzers</div>
	
	<div id="analyzers-view"></div>

</div>

<script type="text/template" id="analyzer-data-source-link-template">
	<button id="<%- id %>" class="btn btn-mini btn-primary manage-data-source">
		<i class="icon-white icon-hdd" />
	</button>
</script>

<script type="text/template" id="multi-select-button-template">
	<button type="button" data-id="<%- dataId %>" data-display-name="<%- displayName %>" class="btn btn-mini"><%- displayName %></button>
</script>