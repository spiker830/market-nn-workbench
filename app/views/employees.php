
<div class="innerContent">

	<div class="floatingHeader">Employees</div>

	<div id="toggleInactive">
		<button class="btn btn-primary">Show Inactive
		</button>
	</div>

	<table id="employees" class="table">
		<thead>
		<th>First</th>
		<th>Last</th>
		<th>Profile</th>
		<th>Files</th>
		<th>Login</th>
		<th>Username</th>
		<th></th>
		<th>Status</th>
		<th>Wages</th>
		<th>Level</th>
		<th></th>
		<th></th>
		<th></th>
		</thead>
		<tbody>
		</tbody>
	</table>

	<div class="modal hide" id="directReportsModal">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">×</button>
			<h3>Manage Direct Reports</h3>
		</div>
		<div class="modal-body">
			<table class="table table-condensed">
				<div id="possibleReports">

				</div>
				<thead>
				<th>
					Name
				</th>
				<th></th>
				</thead>
				<tbody>
				</tbody>
			</table>
		</div>
		<div class="modal-footer">
			<a href="#" class="btn" data-dismiss="modal">Done</a>
		</div>
	</div>

	<div class="modal hide" id="wagesModal">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">×</button>
			<h3>Manage Wages</h3>
		</div>
		<div class="modal-body">
			<table class="table table-condensed">
				<thead>
				<th>
					Wage Name
				</th>
				<th>
					Amount
				</th>
				<th></th>
				<th></th>
				</thead>
				<tbody>

				</tbody>
			</table>
		</div>
		<div class="modal-footer">
			<a href="#" class="btn" data-dismiss="modal">Done</a>
		</div>
	</div>

	<div class="modal hide" id="profileModal">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">×</button>
			<h3></h3>
		</div>
		<div class="modal-body">
			<form class="form-horizontal">
				<div class="control-group">
					<label class="control-label" for="inputEmail">Email</label>
					<div class="controls">
						<input type="text" id="inputEmail">
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="inputPhone">Phone</label>
					<div class="controls">
						<input type="text" id="inputPhone">
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="inputHired">Hired</label>
					<div class="controls">
						<input type="text" class="input-small" id="inputHired">
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="inputTerminated">Terminated</label>
					<div class="controls">
						<input type="text" class="input-small" id="inputTerminated">
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="inputNotes">Notes</label>
					<div class="controls">
						<textarea id="inputNotes"></textarea>
					</div>
				</div>
			</form>
		</div>
		<div class="modal-footer">
			<a href="#" class="btn" data-dismiss="modal">Cancel</a>
			<button class="btn btn-primary" id="saveProfile">Save</button>
		</div>
	</div>

	<div class="modal hide" id="filesModal">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">×</button>
			<h3></h3>
		</div>
		<div class="modal-body">
			
			<table id="tableFiles" class="table table-striped">
			</table>

			<div style="margin-bottom: 0px" class="form-inline">
				<label for="inputTitle">Title: </label>
				<input type="text" id="inputTitle" name="title" class="input-medium" placeholder="Title" />
				<label for="inputFile">File: </label>
				<div id="selectFile" class="btn btn-medium">
					<i class="icon-folder-close"></i>
					Browse
				</div>
				<button id="uploadFile" class="btn btn-medium btn-primary">
					<i class="icon-white icon-upload"></i>
					Upload
				</button>
			</div>
			
			<br />

			<div id="fine-uploader">
			</div>

		</div>
		<div class="modal-footer">
			<a href="#" class="btn" data-dismiss="modal">Done</a>
		</div>
	</div>

</div>