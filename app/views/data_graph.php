
<div class="innerContent">

	<div class="floatingHeader">Data Graph for Genome <?php echo $this->genomeId ?></div>
	
	<br />
	<br />
	
	<div id="data-graph" style="height: 550px"></div>

</div>

<script>
	genomeId = <?php echo $this->genomeId ?>;
</script>