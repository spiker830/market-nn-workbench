

<script type="text/javascript">
	var analyzers = "<?php echo $this->analyzers ?>";
	var targetSymbol = "<?php echo $this->targetSymbol ?>";
	var tradingStrategy = "<?php echo $this->tradingStrategy ?>";
</script>

<div class="innerContent">

	<h3 id="genome-header"></h3>

<!--	<div class="row-fluid">
		<div id="data-toggles"></div>
		<br />
		<br />
	</div>-->
<!--	<div class="row-fluid">
		<div id="job-configuration-graph" style="height:300px"></div>
	</div>
	<div class="row-fluid">
		<div id="job-timeline-graph" style="height:300px"></div>
	</div>-->

	<h4>Genome Details</h4>
	<div id="genome-view"></div>

</div>

<script type="text/template" id="genome-graphs-template">
	<a class="btn btn-warning" href="genomes/error_graph/<%- id %>"><i class="icon icon-signal" /></a>
	<a class="btn btn-success" href="genomes/data_graph/<%- id %>"><i class="icon icon-signal" /></a>
</script>

<script type="text/template" id="multi-select-button-template">
	<button type="button" data-id="<%- dataId %>" data-display-name="<%- displayName %>" class="btn btn-mini"><%- displayName %></button>
</script>
