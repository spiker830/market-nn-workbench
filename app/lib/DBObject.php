<?php

/* ========================================================================== *
 *
 * 
 *
 * 	@author: Jonathan Spiker
 *
 * ========================================================================== */

abstract class DBObject
{
	public $attributes;

	/**
	 * 
	 * @TODO Need to add automatic validation. Child classes
	 * would define the fields and restrictions on them such as:
	 *	- required or not
	 *	- format (number, date, phone number, etc)
	 * 
	 * An exception should be thrown if the validation fails. This should be
	 * caught by the controller logic (RESTfulController) which requires that
	 * code to be rewired.
	 * 
	 */
	public abstract function validate();
	
	public function __construct()
	{
		$this->attributes = array();
	}
	
	public function __set($name, $value)
	{
		$this->set_attribute($name, $value);
	}
	
	protected function set_attribute($name, $value)
	{
		$this->attributes[$name] = $value;
	}
	
	// Returning by array to support multidimensional arrays
	public function &__get($name)
	{
		return $this->get_attribute($name);
	}
	
	protected function &get_attribute($name)
	{
		return $this->attributes[$name];
	}
	
	public function __unset($name)
	{
		unset($this->attributes[$name]);
	}

	protected static function getTable()
	{
		return isset(static::$table) ? static::$table : strtolower(static::getClass());
	}
	
	/**
	 * Get the name of the class this static method is called in.
	 * 
	 * @return string
	 */
	protected static function getClass()
	{
		return get_called_class();
	}
	
	/**
	 * Return a DBObject matching the given id.
	 * 
	 * @param int $id The id of the object to search for.
	 * @return DBObject|null The found DBObject, or null if no object with the
	 * specified id exists.
	 */
	public static function find($id)
	{
		$filters = array();
		$filter = new Filter(static::getTable().".id", Relation::EQUAL, $id);
		$filters[] = $filter;
		
		$objects = static::filter($filters);
			
		if (count($objects) > 0)
		{	
			return $objects[0];
		}
		else
		{
			return null;
		}
	}
	
	/**
	 * 
	 * @return array Possibly empty array of DBObject objects.
	 */
	public static function all($filters = array())
	{
		return static::filter($filters);
	}
	
	/**
	 * Find entries in the database matching the given where clause.
	 * 
	 * @param string $column
	 * @param string $relation
	 * @param string $value
	 * @return array
	 */
	public static function where($column, $relation, $value)
	{
		$filters = array();
		$filter = new Filter($column, $relation, $value);
		$filters[] = $filter;
		
		return static::filter($filters);
	}
	
	protected static function getBaseQuery()
	{
		return "SELECT * FROM " . static::getTable();
	}
	
	protected static function getGroupBy()
	{
		return "";
	}
	
	public static function filterTable($table, $filters = array()) {
		
		Log::debug("Going to use filters:");
		Log::json($filters);
		
		$query = "SELECT * FROM $table";
		
		static::addFilters($query, $table, $filters);
		
		Log::debug("Attempting query: $query");
		
		$statement = DB::instance()->query($query);
		
		// Will want to log any error
		if (!$statement)
		{
			Log::error(json_encode(DB::errorStr()));
			
			return array();
		}
		else
		{
			return $statement->fetchAll(PDO::FETCH_ASSOC);
		}
	}
	
	public static function addFilters(&$query, $table, $filters) {
		
		// For each filter
		foreach ($filters as $filter) {
			
			$attribute = $filter->getAttribute();
			
			// Make sure column is fully qualified
			$column = null;
			
			// If attribute starts with the table name and a "." dot
			// OR
			// If attribute doesn't contain a dot
			if (strpos($attribute, "$table.") === 0 || strpos($attribute, ".") !== false) {
				
				// Use the attribute as is
				$column = $attribute;
				
			} else {
				
				// Add the table to the attribute
				$column = $table . "." . $attribute;
			}
			
			$relation = $filter->getRelation();
			$value = $filter->getValue();
			
			// Check for invalid column
			if (!DB::isSafeEntityName($column))
			{
				Log::error("Attempted to use unsupported column name: $column");
				return array();
			}

			// Check for invalid relation
			if (!DB::isSafeRelation($relation))
			{
				Log::error("Attempted to use unsupported relation: $relation");
				return array();
			}
			
			// Check for invalid value
			if (!DB::isSafeValue($value))
			{
				Log::error("Attempted to use unsupported value: $value");
				return array();
			}
			
			if (preg_match("/where|WHERE/", $query))
			{
				$query .= " AND $column $relation '$value'";
			}
			else
			{
				$query .= " WHERE $column $relation '$value'";
			}
		}
	}
	
	/**
	 * 
	 * @param Filter $filters
	 * @return type
	 */
	public static function filter($filters = array())
	{
		if ($filters == null) $filters = array();
		
		Log::debug("Going to use filters:");
		Log::json($filters);
		
		$class = static::getClass();
		
		$query = static::getBaseQuery();
		
		// For each filter
		foreach ($filters as $filter) {
			
			$table = static::getTable();
			$attribute = $filter->getAttribute();
			
			// Make sure column is fully qualified
			$column = strpos($attribute, "$table.") === 0 || strpos($attribute, ".") !== false ? $attribute : $table . "." . $attribute;
			
			$relation = $filter->getRelation();
			$value = $filter->getValue();
			
			// Check for invalid column
			if (!DB::isSafeEntityName($column))
			{
				Log::error("Attempted to use unsupported column name: $column");
				return array();
			}

			// Check for invalid relation
			if (!DB::isSafeRelation($relation))
			{
				Log::error("Attempted to use unsupported relation: $relation");
				return array();
			}
			
			// Check for invalid value
			if (!DB::isSafeValue($value))
			{
				Log::error("Attempted to use unsupported value: $value");
				return array();
			}
			
			if (preg_match("/where|WHERE/", $query))
			{
				$query .= " AND $column $relation '$value'";
			}
			else
			{
				$query .= " WHERE $column $relation '$value'";
			}
		}
		
		$query .= " " . static::getGroupBy();
		
		Log::debug("Attempting query: $query");
		
		$statement = DB::instance()->query($query);
		
		// Will want to log any error
		if (!$statement)
		{
			Log::error(json_encode(DB::errorStr()));
			
			return array();
		}
		else
		{
			$rows = $statement->fetchAll(PDO::FETCH_ASSOC);
		
			$objects = static::rowsToDBObjects($rows, $class);

			return $objects;
		}
		
	}
	
	/**
	 * Create a new object in the database using the specified data.
	 * 
	 * @param array $data An associative array of attributes with column names
	 * as keys.
	 * @return DBObject
	 */
	public static function create($data)
	{
		// Get class name to find inserted object
		$class = static::getClass();
		
		// Make sure nothing fishy is going on
		$cleansed = static::cleanData($data);
		
		$toInsert = static::updateEditedBy($cleansed);
		
		Log::json($toInsert);
		
		// Do insert
		DB::insert(static::getTable(), $toInsert);
		
		$lastInsertId = DB::instance()->lastInsertId();
		
		Log::debug($lastInsertId);
		
		// Return inserted object
		return $class::find($lastInsertId);
	}
	
	/**
	 * Update this object's attributes with the specified array of attributes.
	 * 
	 * @param array $data An associative array of attributes to update.
	 */
	public function update($data = array())
	{
		foreach ($data as $key => $value)
		{
			// Never try to update the id
			if ($key == "id")
				continue;
			
			$this->{$key} = $value;
		}
		
		Log::debug("Updated object to: " . $this->json());
	}
	
	/**
	 * 
	 * @param type $db
	 * @return bool True on success, false on failure.
	 */
	public function save()
	{
		$this->validate();
		
		$cleansed = static::cleanData($this->attributes);
		
		$toInsert = static::updateEditedBy($cleansed);
		
		// Do update
		return DB::update(static::getTable(), $toInsert, "id = " . (int) $this->id);
	}
	
	/**
	 * 
	 * @return PDOStatement|bool PDOStatement object or false on failure.
	 */
	public function destroy()
	{
		// Delete from database
		return DB::delete(static::getTable(), (int) $this->id);
	}
	
	/**
	 * Get clean set of attributes ready for inserting into database.
	 * 
	 * @param type $data
	 */
	protected static function cleanData($data = array())
	{
		$cleansedData = array();
		
		// If a set of fields has been defined
		if (isset(static::$fields))
		{
			// Insert only those fields into new array
			foreach($data as $key => $value)
			{
				if (in_array($key, static::$fields))
				{
					$cleansedData[$key] = $value;
				}
			}
		}
		else
		{
			$cleansedData = $data;
		}
		
		// Always exclude id
		unset($cleansedData['id']);
		
		return $cleansedData;
	}
	
	/**
	 * 
	 * 
	 * @param type $data
	 * @return type
	 */
	protected static function updateEditedBy($data)
	{
		// Update edited by if necessary
		if (isset(static::$editedBy) && static::$editedBy) {
			$data["last_edited_by"] = Session::get("employeeId");
		}
		
		return $data;
	}
	
	/**
	 * Converts an associative array of MySQL records to an array of DBObject
	 * instances using the given class name.
	 * 
	 * @param array $rows
	 * @param string $className
	 * @return array
	 */
	public static function rowsToDBObjects($rows, $className)
	{
		$objects = array();
		
		if (empty($rows)) return $objects;
		
		foreach ($rows as $row)
		{
			$object = new $className;
			
			foreach ($row as $key => $value)
			{
				$object->{$key} = $value;
			}
			
			$objects[] = $object;
		}
		
		return $objects;
	}
	
	/**
	 * Return a JSON encoded string of this DBObject's attributes.
	 * 
	 * @return string A JSON string.
	 */
	public function json()
	{
		return json_encode($this->to_array(), JSON_NUMERIC_CHECK);
	}
	
	/**
	 * Return the array of this DBObject's attributes.
	 * 
	 * @return array An array of attributes.
	 */
	protected function to_array()
	{
		$data = array();
		
		foreach ($this->attributes as $key => $value)
		{
			if (!isset(static::$hidden) || !in_array($key, static::$hidden))
			{
				$data[$key] = $value;
			}
		}
		
		return $data;
	}
	
	/**
	 * Encode an array of DBObjects as JSON
	 * 
	 * @param array $objects
	 * @return string
	 */
	public static function objectsToJSON($objects)
	{
		$response = array();
		
		foreach ($objects as $object)
		{
			$response[] = $object->to_array();
		}

		return json_encode($response, JSON_NUMERIC_CHECK);
	}
	
}

?>
