<?php

/* ========================================================================== *
 *
 * 	
 *
 * 	@author: Jonathan Spiker
 *
 * ========================================================================== */

class AttachmentController extends RESTfulController
{
	static $controllerName = "attachments";
	
	/**
	 * 
	 * @param type $resourceId
	 * @param Filter $filters
	 * @throws RESTException
	 */
	public function rest_get($resourceId = null, $filters = null)
	{
		// If trying to download a single file
		if (isset($resourceId))
		{
			$pluralResourceName = str_replace("_", " ", get_called_class());
		
			// Get file
			$resource = Attachment::find($resourceId);
			
			if (isset($resource))
			{
				// Download file
				$resource->download($resource);
			}
			// If file didn't exist
			else
			{
				throw new RESTException("Couldn't find any $pluralResourceName with an id of " . $resourceId, HTTP::HTTP_NOT_FOUND);
			}
		}
		else
		{
			parent::rest_get($resourceId, $filters);
		}
	}
}

?>
