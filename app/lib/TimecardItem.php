<?php

/* ========================================================================== *
 *
 *
 *
 * 	@author: Jonathan Spiker
 *
 * ========================================================================== */

class TimecardItem
{
	public $employeeId;
	public $projectId;
	public $date;
	public $employeeWage;
	public $wage;
	public $task;
	public $type;
	public $hours;
	public $chargeCodeArray;

	function __construct(
			$employeeID,
			$projectID,
			$date,
			$employeeWage,
			$wage,
			$chargeCodeArray,
			$task,
			$hours,
			$type)
	{
		$this->employeeId = $employeeID;
		$this->projectId = $projectID;
		$this->date = $date;
		$this->employeeWage = $employeeWage;
		$this->wage = $wage;
		$this->chargeCodeArray = $chargeCodeArray;
		$this->task = $task;
		$this->hours = $hours;
		$this->type = $type;
	}

	public static function projectWages($projectId)
	{
		$statement = DB::instance()->prepare("
			select id, name, wage
			from project_wage
			where projectId = :projectId
			");

		$statement->execute(array(":projectId" => $projectId));

		$wages = $statement->fetchAll(PDO::FETCH_ASSOC);

		return $wages;
	}

	public static function projectEmployeeWages($projectId)
	{
		$statement = DB::instance()->prepare("
			select id, name, wage
			from employee_wage
			where projectId = :projectId
			");

		$statement->execute(array(
				":projectId" => $projectId
		));

		$wages = $statement->fetchAll(PDO::FETCH_ASSOC);

		return $wages;
	}

	public static function employeeWages($employeeId)
	{
		$statement = DB::instance()->prepare("
			select id, name, wage
			from employee_wage
			where employeeId = :employeeId
			");

		$statement->execute(array(
				":employeeId" => $employeeId
		));

		$wages = $statement->fetchAll(PDO::FETCH_ASSOC);

		return $wages;
	}
	
	/**
	 * Get all employees wages relevant to an employee and a project.
	 * 
	 * @param type $employeeId
	 * @param type $projectId
	 */
	public static function allEmployeeWages($employeeId, $projectId)
	{
		$allWages = array();
		
		// Get all project employee wages
		$projectEmployeeWages = static::projectEmployeeWages($projectId);
		
		// Add type of project to each entry
		foreach ($projectEmployeeWages as $wage)
		{
			$wage['type'] = "project";
			$allWages[] = $wage;
		}
		
		// Get all employee wages
		$employeeWages = static::employeeWages($employeeId);
		
		// Add type of employee to each entry
		foreach ($employeeWages as $wage)
		{
			$wage['type'] = "employee";
			$allWages[] = $wage;
		}
		
		return $allWages;
	}

}
?>
