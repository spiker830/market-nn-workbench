<?php

/* ========================================================================== *
 *
 * 	
 *
 * 	@author: Jonathan Spiker
 *
 * ========================================================================== */

class Filter implements JsonSerializable
{
	private $attribute;
	private $relation;
	private $value;
	
	public function __construct($attribute, $relation, $value)
	{
		$this->attribute = $attribute;
		$this->relation = $relation;
		$this->value = $value;
	}
	
	public function getAttribute()
	{
		return $this->attribute;
	}
	
	public function getRelation()
	{
		return $this->relation;
	}
	
	public function getValue()
	{
		return $this->value;
	}

	public function jsonSerialize()
	{
		return array(
			"attribute" => $this->attribute,
			"relation" => $this->relation,
			"value" => $this->value
		);
	}
	
}

abstract class Relation
{
	const EQUAL = "=";
	const LESS_THAN = "<";
	const LESS_THAN_OR_EQUAL = "<=";
	const GREATER_THAN = ">";
	const GREATER_THAN_OR_EQUAL = ">=";
	const LIKE = "LIKE";
}

?>
