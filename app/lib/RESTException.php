<?php

/* ========================================================================== *
 *
 * 	
 *
 * 	@author: Jonathan Spiker
 *
 * ========================================================================== */

class RESTException extends Exception
{
	public function getHTTPStatusCode()
	{
		$code = parent::getCode();
		
		return $code == 0 ? HTTP::HTTP_INTERNAL_SERVER_ERROR : $code;
	}
}
?>
