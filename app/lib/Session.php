<?php

/* ========================================================================== *
 *
 *
 *
 * 	@author: Jonathan Spiker
 *
 * ========================================================================== */

class Session
{
	/*
	 * FIXME The way timeouts are handled is a little messy here.
	 * 
	 * The problem comes with the fact that a few things are happening:
	 * 
	 * The login, logout, and error controllers' index do not resume sessions (intentionally)
	 * The login, logout, and error pages all cause the config object to be loaded
	 *		This can cause a new timeout to be set before someone logs in
	 *		My workaround for this was to only check for timeouts if the user is logged in (messy)
	 */

	public static function init()
	{
		// If the session isn't already in progress (by another class in the same request)
		if (!isset($_SESSION))
		{
			// Start/resume session
			session_start();
			
			if (static::get("loggedIn"))
				static::handleTimeout();
			
			// Update time out at
			static::set("time_out_at", time() + Configuration::getValue("SESSION_TIMEOUT_MINUTES") * 60);

//			Log::debug("Time out updated to: " . date(DateTime::RFC1123, static::get("time_out_at")));
		}
		
//		Log::debug("\$_SESSION: " . json_encode($_SESSION));
		
	}

	public static function set($key, $value)
	{
		$_SESSION[$key] = $value;
	}

	public static function get($key)
	{
		if (isset($_SESSION[$key]))
		{
			return $_SESSION[$key];
		}
		else
		{
			return null;
		}
	}

	public static function destroy()
	{
		// Make sure we have access to the session
		if (!isset($_SESSION))
		{
			session_start();
		}
		
		unset($_SESSION);
		
		session_destroy();
	}
	
	private static function handleTimeout()
	{
		// If the time out at has been set
		if (static::get("time_out_at") != null)
		{
			// If we are timed out
			if (static::get("time_out_at") < time())
			{
//				Log::debug("Session timed out for " . static::get("employeeName") . " (" . static::get("employeeId") . "), destroying session");
				
				// Destroy session and throw error up to Bootstrap
				static::destroy();

				throw new Exception("Your session has timed out. Please log in again");
			}
		}
	}
	
	public static function redirectToLogin()
	{
		header("Location: " . Configuration::getValue("URL_BASE") . "login");
	}
	
	public static function requireLogin()
	{
		$loggedIn = Session::get('loggedIn');
		
		if (!$loggedIn)
		{
			static::redirectToLogin();
			die();
		}
	}

}
?>
