<?php

/* ========================================================================== *
 *
 * 	
 *
 * 	@author: Jonathan Spiker
 *
 * ========================================================================== */

class Dev_Admin extends Controller
{
	public function index()
	{
		// Set javascript files to be used
		$this->view->js[] = '../lib/dev/admin';
		
		// Render view
		$this->view->render('../lib/dev/admin');
	}
}

?>
