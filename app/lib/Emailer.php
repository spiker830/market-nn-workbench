<?php

/* ========================================================================== *
 *
 * 	
 *
 * 	@author: Jonathan Spiker
 *
 * ========================================================================== */

class Emailer
{
	/** @var Email $emails */
	private $emails;
	
	public function __construct()
	{
		$this->emails = array();
	}
	
	public function emailStarted($recipient)
	{
		return isset($this->emails[$recipient]);
	}
	
	public function setSubject($recipient, $subject)
	{
		// If the email hasn't been started, create it
		if (!$this->emailStarted($recipient)) $this->emails[$recipient] = new Email();
		
		$this->emails[$recipient]->setSubject($subject);
	}
	
	public function addToBody($recipient, $body)
	{
		// If the email hasn't been started, create it
		if (!$this->emailStarted($recipient)) $this->emails[$recipient] = new Email();
		
		$existingBody = $this->emails[$recipient]->getBody();
		
		$this->emails[$recipient]->setBody($existingBody . $body);
	}
	
	public function sendAll()
	{
		// Send each email
		foreach ($this->emails as $recipient => $email)
		{
			static::email($recipient, $email);
		}
		
		// Reinitialize emails (just in case)
		$this->emails = array();
	}

	/**
	 * Sends an email
	 * 
	 * @param string $recipient 
	 * @param Email $email
	 */
	private static function email($recipient, $email)
	{
		if (Configuration::getValue("NOTIFICATIONS_ENABLED"))
		{
			require 'lib/email/PHPMailerAutoload.php';

			$emailer = new PHPMailer();

			// Use SMTP configuration if required
			if (Config::getValue("MAIL_BACKEND") == 'smtp')
			{
					$emailer->isSMTP();                                      // Set mailer to use SMTP
					$emailer->Host = Config::getValue("SMTP_HOST");          // Specify main and backup server
					$emailer->Port = Config::getValue("SMTP_PORT");
					$emailer->SMTPAuth = true;                               // Enable SMTP authentication
					$emailer->Username = Config::getValue("SMTP_USERNAME");  // SMTP username
					$emailer->Password = Config::getValue("SMTP_PASSWORD");  // SMTP password
			}
			else
			{
					$emailer->isSendmail();
			}

			$emailer->From = Config::getValue("NOTIFICATIONS_FROM");
			$emailer->FromName = 'IronFS Timecard';
			$emailer->addAddress($recipient);                                // Add a recipient

			$emailer->Subject = $email->getSubject();
			$emailer->Body    = $email->getBody();

			if(!$emailer->send()) {

					Log::error("Failed to send email to " . $recipient);
					Log::error("Mailer Error: " . $emailer->ErrorInfo);

			} else {

					Log::debug("Successfully sent email to " . $recipient . " with body: " . $email->getBody());

			}
		}
	}
}

?>