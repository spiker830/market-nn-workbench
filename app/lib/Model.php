<?php

/* ========================================================================== *
 *
 * A Model accesses the database to retrieve information for the Controller.
 *
 * 	@author: Jonathan Spiker
 *
 * ========================================================================== */

class Model
{
	/** @var PDO */
	public $db;

	function __construct()
	{
		$this->db = DB::instance();

		$this->loadEmployeeStatusArray();
		$this->loadEmployeeLevelArray();
		$this->loadProjectStatusArray();
		$this->loadCustomerStatusArray();
		$this->loadTimecardItemStatusArray();
	}

	function loadEmployeeStatusArray()
	{
		$statement = $this->db->prepare("select id, status
			from employee_status");

		$statement->execute();

		$employeeStatuses = array();

		while ($row = $statement->fetch(PDO::FETCH_ASSOC))
		{
			$employeeStatuses[$row['status']] = (int) $row['id'];
		}

		$this->employeeStatuses = $employeeStatuses;
	}

	function loadEmployeeLevelArray()
	{
		$statement = $this->db->prepare("select id, level
			from employee_level");

		$statement->execute();

		$employeeLevels = array();

		while ($row = $statement->fetch(PDO::FETCH_ASSOC))
		{
			$employeeLevels[$row['level']] = (int) $row['id'];
		}

		$this->employeeLevels = $employeeLevels;
	}

	function loadProjectStatusArray()
	{
		$statement = $this->db->prepare("select id, status
			from project_status");

		$statement->execute();

		$projectStatuses = array();

		while ($row = $statement->fetch(PDO::FETCH_ASSOC))
		{
			$projectStatuses[$row['status']] = (int) $row['id'];
		}

		$this->projectStatuses = $projectStatuses;
	}

	function loadCustomerStatusArray()
	{
		$statement = $this->db->prepare("select id, status
			from customer_status");

		$statement->execute();

		$customerStatuses = array();

		while ($row = $statement->fetch(PDO::FETCH_ASSOC))
		{
			$customerStatuses[$row['status']] = (int) $row['id'];
		}

		$this->customerStatuses = $customerStatuses;
	}

	function loadTimecardItemStatusArray()
	{
		$statement = $this->db->prepare("select id, status
			from timecard_item_status");

		$statement->execute();

		$timecardItemStatuses = array();

		while ($row = $statement->fetch(PDO::FETCH_ASSOC))
		{
			$timecardItemStatuses[$row['status']] = (int) $row['id'];
		}

		$this->timecardItemStatuses = $timecardItemStatuses;
	}

}
?>
