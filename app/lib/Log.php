<?php

/* ========================================================================== *
 *
 * 	A logging utility class.
 *
 * 	@author: Jonathan Spiker
 *
 * ========================================================================== */

class Log
{
	const DEBUG = "DEBUG";
	const ERROR = "ERROR";
	
	private static $trace;

	public static function debug($message)
	{
		if (isset($message) && Configuration::getValue("DEBUG"))
			static::log_message(Log::DEBUG, $message);
	}

	public static function error($message)
	{
		if (isset($message))
			static::log_message(Log::ERROR, $message);
	}
	
	public static function json($variable)
	{
		static::log_message(Log::DEBUG, json_encode($variable, JSON_PRETTY_PRINT));
	}

	private static function log_message($level, $message)
	{
//		static::createFile(ini_get("error_log"));
		
		static::loadTrace();
		
		// Get file name
		$file = static::getCallerFile();
		
		// Get line number
		$line = static::getCallerLine();

		// Send to logger with our special format
		$logLine = "| $level | $file:$line | $message";
		
		error_log($logLine);
	}
	
	private static function createFile($path)
	{
		if (!file_exists($path))
		{
			// Create directory structure
			$dirs = preg_replace("/\/[^\/]*$/", "", $path);
			mkdir($dirs, 0777, true);

			// .htaccess file since this is brand spankin new
			file_put_contents($dirs."/.htaccess", "deny from all\n");
			
			// Create file
			touch($path);
		}
	}
	
	private static function getCallerFile()
	{
		$file = "?";
		$matches = array();
		
		if (isset(static::$trace[0]["file"]))
		{
			$file = basename(static::$trace[0]["file"]);
		}
		
		return $file;
	}
	
	private static function getCallerLine()
	{
		$function = "?";
		
		if (isset(static::$trace[0]["line"]))
			$function = static::$trace[0]["line"];
		
		return $function;
	}
	
	private static function loadTrace()
	{
		$options = false;// !DEBUG_BACKTRACE_PROVIDE_OBJECT | DEBUG_BACKTRACE_IGNORE_ARGS // Only in 5.3+;
		
		static::$trace = array_slice(debug_backtrace($options), 2, 3);
	}

}

?>
