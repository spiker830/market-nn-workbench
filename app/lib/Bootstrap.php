<?php

/* ========================================================================== *
 *
 * Uses the url to determine the Controller and Model to load.
 *
 * 	@author: Jonathan Spiker
 *
 * ========================================================================== */

class Bootstrap
{
	/**
	 * Handle the request by calling the appropriate Controller.
	 * 
	 */
	public static function run()
	{
		Log::debug("Received " .
				$_SERVER["REQUEST_METHOD"] .
				" request for " .
				(isset($_GET['url']) ? $_GET["url"] : "undefined"));
		
		// Initiate output bufferring... why?
		ob_start();
		
		try
		{
			// Get URL array
			$urlArray = static::getURLArray();
			
			// If dev URL
			if (isset($urlArray[0]) && $urlArray[0] == "dev")
			{
				$controller = new Dev_Admin();
				
				$controller->call(array("index"));
			}
			// If RESTful
			else if (static::isRESTful($urlArray))
			{
				// Do RESTful call
				static::doRESTfulCall(array_slice($urlArray, 1));
			}
			else
			{
				// Do non-RESTful call
				// TODO Deprecated and will hopefully disappear one day
				static::doNonRESTfulCall($urlArray);
			}
		}
		// If REST Exception occurs
		catch (RESTException $e)
		{
			Response::say($e->getMessage(), $e->getHTTPStatusCode());
		}
		// If controller creation fails or if exception is thrown from controller
		catch (Exception $e)
		{
			static::error($e->getMessage());
		}
	}
	
	/**
	 * Get the URL as an array, split by /'s.
	 * 
	 * @return array The URL split into an array of strings.
	 */
	private static function getURLArray()
	{
		$urlString = isset($_GET['url']) ? $_GET['url'] : Configuration::getValue("DEFAULT_PAGE");
		$url = rtrim(filter_var($urlString, FILTER_SANITIZE_URL), "/");
		
		return explode('/', $url);
	}
	
	/**
	 * Determine whether the call is a RESTful one or not.
	 * 
	 * @param type $urlArray The array of URL components
	 * @return boolean True if the call was RESTful, false otherwise
	 */
	private static function isRESTful($urlArray)
	{
		$restfulContext = Configuration::getValue("RESTFUL_CONTEXT");
		
		return $urlArray[0] == $restfulContext;;
	}
	
	/**
	 * Initiate a RESTful call using the URL components.
	 * 
	 * @param type $urlArray The array of URL components
	 */
	private static function doRESTfulCall($urlArray)
	{
		// Argument would be the last item in an even number of URL components
		$arguments = count($urlArray) % 2 == 0 ? array_slice($urlArray, -1) : null;
			
		// Get the controller name
		$controllerName = static::getRESTfulControllerName($urlArray);
			
		Log::debug("controllerName=$controllerName");
		
		$filters = static::getFilters($urlArray);

		// Do call on controller using filters
		static::doControllerCall($controllerName, $arguments, true, $filters);
	}
	
	private static function getFilters($urlArray) {
		
		$filters = array();
		
		// Include query parameters as filters
		static::getQueryParametersAsFilters($filters);
		
		// Get filter from URL components
		$filter = static::getFilter($urlArray);

		if (isset($filter)) {

			$filters[] = $filter;

		}

		Log::debug("filters:");
		Log::json($filters);
		
		return $filters;
	}
	
	private static function getQueryParametersAsFilters(&$filters) {
		
		foreach (Input::getQueryParameters() as $key => $value) {
			
			// Ignore "url" parameter
			if ($key != "url") {
				
				$filters[] = new Filter($key, Relation::EQUAL, $value);
				
			}
			
		}
		
		return $filters;
	}
	
	/**
	 * Get the filter to use for a given URL.
	 * 
	 * urlArray: /purchase_orders/1/items/3
	 * Filter: purchase_order_id = 1
	 * 
	 * urlArray: /purchase_orders/1/items/3/attachments/6
	 * Filter: purchase_order_item_id = 3
	 * 
	 * urlArray: /project/1/purchase_orders/5
	 * Filter: project_id = 1
	 * 
	 * @param type $urlComponents
	 * @return Filter
	 */
	private static function getFilter($urlComponents)
	{
		// Normalize to only the components we care about
		$urlComponents = static::trimRESTfulComponents($urlComponents);
	
		// The value should be the last element now
		$lastIndex = count($urlComponents)-1;
		$value = isset($urlComponents[$lastIndex]) ? $urlComponents[$lastIndex] : "";
		
		// Get the even numbered components in an array (the controller names)
		$evens = static::evenElements($urlComponents);
		
		// Get the foreign key
		$foreignKey = Terminology::getForeignKeyByResourceNames($evens);
		
		return isset($foreignKey) ? new Filter("$foreignKey", Relation::EQUAL, $value) : null;
	}
	
	private static function trimRESTfulComponents($urlComponents)
	{
		// If there are an even number of components
		if (count($urlComponents) % 2 == 0)
		{
			// Ignore the last two
			return array_slice($urlComponents, 0, -2);
		}
		// If there an odd number of components
		else
		{
			// Ignore the last one
			return array_slice($urlComponents, 0, -1);
		}
	}
	
	/**
	 * URL: /purchase_orders/1/items/3
	 * Controller: purchase_order_items
	 * 
	 * URL: /purchase_orders/1/items/3/attachments/6
	 * Controller: purchase_order_item_attachments
	 * 
	 * URL: /project/1/purchase_orders/5
	 * Controller: project_purchase_orders
	 * 
	 * @param type $urlComponents
	 * @return type
	 */
	private static function getRESTfulControllerName($urlComponents)
	{
		// Get the even numbered elements (the controller names)
		$controllerNames = static::evenElements($urlComponents);
		
		// Return the controller name
		return Terminology::getResourceByResourceNames($controllerNames);
	}
	
	private static function evenElements($array)
	{
		$odds = array();
		
		foreach ($array as $index => $value)
		{
			if ($index % 2 == 0)
			{
				$odds[] = $value;
			}
		}
		
		return $odds;
	}
	
	/**
	 * Initiate a non-RESTful call using the URL components.
	 * 
	 * @param type $urlArray The array of URL components
	 */
	private static function doNonRESTfulCall($urlArray)
	{
		$filters = static::getFilters($urlArray);
		
		// Delegate to regular controller
		static::doControllerCall($urlArray[0], array_slice($urlArray, 1), false, $filters);
	}
	
	/**
	 * Initiate a call on a controller.
	 * 
	 * @param type $controllerName
	 * @param type $arguments
	 * @param type $restful
	 * @param type $filters
	 */
	private static function doControllerCall($controllerName, $arguments, $restful, $filters = null)
	{
		$restfulContext = Configuration::getValue("RESTFUL_CONTEXT");

		$file = "controllers/" . ($restful ? $restfulContext . "/" : "") . $controllerName . ".php";

		try
		{
			$controller = static::getController($controllerName, $file);

			$controller->call($arguments, $filters, $restful);
		}
		catch (Exception $e)
		{
			if ($restful)
				throw new RESTException($e->getMessage());
			else
				throw $e;
		}
	}

	/**
	 * Instantiate and return the correct controller.
	 * 
	 * @param string $controllerName The name of the controller to instantiate.
	 * @param string $file The file containing the specified controller.
	 * @return Controller The controller object.
	 * @throws Exception If the controller file does not exist.
	 */
	private static function getController($controllerName, $file)
	{
		$controller = null;
		
		if (file_exists($file))
		{
			require($file);
			
			$controller = new $controllerName();
			
			// @TODO Model loading could instead be done by the controller itself
			$controller->loadModel($controllerName);
		}
		else // Controller doesn't exist
		{
			throw new Exception("The " . $controllerName . " page could not be found.");
		}

		return $controller;
	}

	/**
	 * Display the error page with a custom message.
	 * 
	 * @param string $message The message to be displayed on the error page.
	 */
	private static function error($message = "")
	{
		// Display error page
		require 'controllers/error.php';
		$controller = new Error();
		$controller->index($message);
	}

}

?>
