define(['jquery', 'backbone', 'util'],

function() {
//	var select = new Select({
//		url: urlBase + "resources/projects",
//		displayDataId: ["name", "address"],
//		valueDataId: "id",
//		filters: [
//			{
//				attribute: "status",
//				value: 2
//			},
//			{
//				lookupURL: urlBase + "resources/project_statuses",
//				lookupKey: "status", // key in lookup table
//				lookupValue: "active", // value in lookup table
//				attribute: "status" // attribute name in projects table
//			}
//		]
//	});
alert("hello");
	
	var select = new Select({
		url: urlBase + "resources/suppliers",
		displayDataId: "name",
		valueDataId: "id",
		addAbility: {
			field: "name", // field which the text input will be sent as
			itemName: "supplier"
		}
//		defaultValue: 4
//		addItemField: "name" // TODO allows creation of new models with single
//		text field input. Later, will want to be able to pass a list item view
//		(with defined fields and an added url) and use that to display a modal
//		for editing the single new item.
	});

	$("#select-container").append(select.render().el);
	
//	var projectListView = new ListView({
//		fields: [
//			{
//				displayName: "Number",
//				dataId: "number",
//				defaultValue: "number"
//			},
//			{
//				displayName: "Name",
//				dataId: "name",
//				defaultValue: "name"
//			},
//			{
//				displayName: "Address",
//				dataId: "address",
//				defaultValue: "address"
//			}
//		],
//		url: urlBase + "resources/projects"
//	});
//	
//	$("#project-list-container").append(projectListView.render().el);
	
	// Thought exercise for what I want out of a reusable attachment view
	var noRun = function() {
		
		// The attachment modal would use a list view to show existing files
		// The footer would have an add file button which would show the upload
		// file section at the bottom of the list.
		// Once the upload is complete, the upload file section would be hidden
		// and either the whole list is repopulated or a new model is added to
		// the list's collection.
		
		var attachmentModal = new AttachmentModal({
			url: urlBase + "resources/employees/" + someEmployeeId + "/attachments"
		});
		
		// Requests from the attachment modal
		// 
		// Download file:
		// GET resources/employees/[employee_id]/attachments/[attachment_id]
		// 
		// Get list of files (JSON):
		// GET resources/employees/[employee_id]/attachments
		// 
		// Upload a file:
		// POST resources/employees/[employee_id]/attachments
		// 
		// Delete a file:
		// DELETE resources/employees/[employee_id]/attachments/[attachment_id]
		
		// Or as an option in a list view
		var projectListView = new ListView({
			url: urlBase + "resources/projects",
			attachments: true
		});
		
		// As an option in a list view, the list would trigger an attachment
		// modal for each list item. The url for the attachment controller
		// could be inferred from the list view's url (resources/projects ->
		// resources/projects/[project_id]/attachments). When in a list,
		// the attachment button/trigger/link should not be shown until the list
		// item is saved. This prevents an error when trying to attach a file
		// using an initial id from Backbone (e.g. c100).
		
	};
	
});