<?php

/* ========================================================================== *
 *
 * 	
 *
 * 	@author: Jonathan Spiker
 *
 * ========================================================================== */

class Email
{
	private $subject;
	private $body;
	
	public function getSubject()
	{
		return $this->subject;
	}
	
	public function getBody()
	{
		return $this->body;
	}
	
	public function setSubject($subject)
	{
		$this->subject = $subject;
	}
	
	public function setBody($body)
	{
		$this->body = $body;
	}
}

?>