<?php

/* ========================================================================== *
 *
 * 	
 *
 * 	@author: Jonathan Spiker
 *
 * ========================================================================== */

class RESTfulController extends Controller
{
	static $controllerName;
	
	public function __construct()
	{
		parent::__construct();
	}
	
	protected function getDBObjectName()
	{
		if (isset(static::$controllerName))
		{
			return Terminology::getDBObjectByControllerName(static::$controllerName);
		}
		else
		{
			return Terminology::getDBObjectByControllerName(get_called_class());
		}
	}
	
	/**
	 * 
	 * @param type $resourceId
	 * @param Filter $filters
	 * @throws RESTException
	 */
	public function rest_get($resourceId = null, $filters = null)
	{
		Log::debug("Filters in the rest_get method: " . json_encode($filters));
		
		$resourceName = $this->getDBObjectName();
		$pluralResourceName = str_replace("_", " ", get_called_class());
		
		if (isset($resourceId))
		{
			// Get single resource
			$resource = $resourceName::find($resourceId);
			
			// If only getting single resource
			if (isset($resource))
			{
				Response::sayJSON($resource->json());
			}
			// If single resource didn't exist
			else
			{
				throw new RESTException("Couldn't find any $pluralResourceName with an id of " . $resourceId, HTTP::HTTP_NOT_FOUND);
			}
		}
		else
		{
			// Get all resources
			$resources = $resourceName::all($filters);
			
			Response::sayJSON(DBObject::objectsToJSON($resources));
		}
	}

	public function rest_put($resourceId)
	{
		$resourceName = $this->getDBObjectName();
		$pluralResourceName = str_replace("_", " ", get_called_class());
		
		// Get resource
		$resource = $resourceName::find($resourceId);
		
		// Update
		$input = Input::json();

			$resource->update($input);

			// Save
			if ($resource->save())
			{
				echo $resource::find($resourceId)->json();
			}
			else
			{
				throw new RESTException("Could not update $pluralResourceName resource", HTTP::HTTP_INTERNAL_SERVER_ERROR);
			}
		}

	function rest_post($filters = array())
	{
		$resourceName = $this->getDBObjectName();
		$pluralResourceName = str_replace("_", " ", get_called_class());
		
		$input = Input::json();
		
		// If there are filters, they should be added to the data
		foreach ((array)$filters as $filter)
		{
			// Only equivalence makes sense
			if ($filter->getRelation() == Relation::EQUAL)
			{
				$input[$filter->getAttribute()] = $filter->getValue();
			}
		}
		
		// Create new resource
		$resource = $resourceName::create($input);

		// Respond with success/failure
		if ($resource != null)
		{
			echo $resource->json();
		}
		else
		{
			throw new RESTException("Could not add resource to $pluralResourceName", HTTP::HTTP_INTERNAL_SERVER_ERROR);
		}
	}
	
	function rest_delete($resourceId)
	{
		$resourceName = $this->getDBObjectName();
		$pluralResourceName = str_replace("_", " ", get_called_class());
		
		// Get resource
		$resource = $resourceName::find($resourceId);

		// Delete
		if ($resource->destroy())
		{
			echo $resource->json();
		}
		else
		{
			throw new RESTException("Could not delete resource from $pluralResourceName", HTTP::HTTP_INTERNAL_SERVER_ERROR);
		}
	}
}

?>
