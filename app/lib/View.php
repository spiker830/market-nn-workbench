<?php

/* ========================================================================== *
 *
 * A View contains all the base JavaScript and CSS files, as well as utilizes
 * templates for page headers and footers. The View may change based on the
 * authorization of the user therefore it extends AuthComponent.
 *
 * 	@author: Jonathan Spiker
 *
 * ========================================================================== */

class View
{

	function __construct()
	{
		$this->js = array(
			"lib/jquery"
		);

		$this->css = array(
			"flick/jquery-ui",
			"bootstrap",
			"bootstrap-responsive",
			"scatter-matrix",
			"ironfs"
		);
	}

	function render($view)
	{
		$this->name = $view;

		require 'templates/header.php';

		require "views/$view.php";

		require 'templates/footer.php';
	}

}

?>
