<?php

/* ========================================================================== *
 *
 * A Controller contains both a View and a Model. It is responsible for
 * authorization therefore it extends AuthComponent. The Model loader is called
 * as part of the Bootstrap depending on the method (representing a page)
 * called.
 * 
 * Traditionally, Models are responsible for determining everything about the
 * information to be retured. In this implementation, however, the Controller is
 * responsible for filtering/restricting data based on the authorization of the
 * current user. This is done to separate the database access code and the
 * authorization code.
 *
 * 	@author: Jonathan Spiker
 *
 * ========================================================================== */

class Controller
{

	/** @var Model */
	protected $model;
	/** @var View */
	protected $view;
	
	protected static $public = false;

	function __construct()
	{
		Session::init();

		$this->view = new View();
		
		// If not open to everyone, confirm the user is logged in
		if (!static::$public)
			Session::requireLogin();

	}

	// @TODO Legacy model to be replaced by use of DBObjects
	// Need to move the call to loadModel() to each controller which uses it
	// and then this method would be the only piece of code related to models
	// still in the framework.
	function loadModel($name)
	{
		$path = 'models/' . $name . '_model.php';
		
		if (file_exists($path))
		{
			require $path;

			$modelName = $name . '_Model';
			$this->model = new $modelName();
		}
	}

	/**
	 * Call the appropriate function using a relative URL (as an array of strings).
	 * 
	 * @param type $arguments An array of string representing the relative URL.
	 * e.g. For the URL localhost/mvc/users/promote/5 this parameter would
	 * be an array of strings == array("promote", "5").
	 * @param type $restful Whether this controller uses RESTful function names.
	 */
	public function call($arguments, $filters = null, $restful = false)
	{
		// If this controller is acting in a RESTful capacity
		if ($restful)
		{
			// Forward to appropriate RESTful function
			$httpMethod = strtolower($_SERVER["REQUEST_METHOD"]);

			$this->errorIfNoFunction("rest_" . $httpMethod);
			
			// Authorize call
			$this->authorize($restful, $httpMethod);
			
			// Add any filters
			if (isset($filters))
			{
				// Set first argument to null when doing a get
				// TODO this is horrible looking, filters should be the first argument? anything but this
				if (empty($arguments) && $httpMethod == "get")
					$arguments[0] = null;
					
				$arguments[] = $filters;
			}

			call_user_func_array(array($this, "rest_" . $httpMethod), $arguments);
		}
		else if (count($arguments) == 0)
		{
			$this->errorIfNoFunction("index");
			
			// Assume index function if nothing to call
			call_user_func_array(array($this, "index"), array($filters));
		}
		else
		{
			$this->errorIfNoFunction($arguments[0]);

			call_user_func_array(array($this, $arguments[0]), array_slice($arguments, 1));
		}
	}

	private function errorIfNoFunction($function)
	{
		// Error if function doesn't exist
		if (!method_exists($this, $function))
		{
			Response::code(HTTP::HTTP_NOT_FOUND);

			throw new Exception("The $function page could not be found.");
		}
	}

	function response($success, $message = "An error occurred")
	{
		$response = array('success' => $success, 'message' => $message);

		return json_encode($response);
	}
	
	private function authorize($restful, $methodName)
	{
		// TODO NEED TO REMOVE FRONTEND CONFIGURATION RETRIEVAL FROM CONFIGURATIONS RESTFUL CONTROLLER!!!!!!
		
		// Generate privilege name
		$privilege = $this->generatePrivilegeName($restful, $methodName);
		
		Log::debug("Checking to see if current user is allowed to $privilege.");
		
		// Check if authorized
		if (!UserLevelPrivilege::authorized($privilege))
		{
			// Fall back to possible "edit" privilege
			if (strstr($privilege, "create") || strstr($privilege, "update") || strstr($privilege, "delete"))
			{
				$privilege = preg_replace(array("/^create/", "/^update/", "/^delete/"), "edit", $privilege);
				
				if (isset(static::$editPrivilege))
				{
					$privilege = static::$editPrivilege;
				}
				
				Log::debug("Fallback to checking if current user is allowed to $privilege.");
				
				if (UserLevelPrivilege::authorized($privilege))
				{
					return;
				}
			}
			
			Log::error("The current user is not allowed to $privilege.");
			
			throw new RESTException("You are not allowed to $privilege.", HTTP::HTTP_FORBIDDEN);

		}
		
	}
	
	private function generatePrivilegeName($restful, $methodName)
	{
		$controllerPortion = strtolower(str_replace("_", " ", get_called_class()));
		
		if ($restful)
		{
			$restVerbMap = array(
				"get" => "view",
				"post" => "create",
				"put" => "update",
				"delete" => "delete"
			);
			
			// Map REST method to "nicer" verb
			$actionPortion = $restVerbMap[$methodName];
			
			// Check for any overridden privilege names
			switch($actionPortion)
			{
				case "view":
					
					if (isset(static::$viewPrivilege))
						return static::$viewPrivilege;
					
					break;
					
				case "create":
					
					if (isset(static::$createPrivilege))
						return static::$createPrivilege;
					
					break;
					
				case "update":
					
					if (isset(static::$updatePrivilege))
						return static::$updatePrivilege;
					
					break;
					
				case "delete":
					
					if (isset(static::$deletePrivilege))
						return static::$deletePrivilege;
					
					break;
			}
			
		}
		else
		{
			$actionPortion = $methodName;
		}
		
		return $actionPortion . " " . $controllerPortion;
	}
	
	protected function attachFineUploaderResources()
	{
		// Set css
		$this->view->css[] = 'fineUploader';

		// Set javascript files to be used
		$this->view->js[] = 'fileUploader/util';
		$this->view->js[] = 'fileUploader/button';
		$this->view->js[] = 'fileUploader/ajax.requester';
		$this->view->js[] = 'fileUploader/deletefile.ajax.requester';
		$this->view->js[] = 'fileUploader/handler.base';
		$this->view->js[] = 'fileUploader/window.receive.message';
		$this->view->js[] = 'fileUploader/handler.form';
		$this->view->js[] = 'fileUploader/handler.xhr';
		$this->view->js[] = 'fileUploader/uploader.basic';
		$this->view->js[] = 'fileUploader/dnd';
		$this->view->js[] = 'fileUploader/uploader';
		$this->view->js[] = 'fileUploader/jquery-plugin';
	}
}

?>
