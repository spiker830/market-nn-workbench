<?php

/* ========================================================================== *
 *
 * 	
 *
 * 	@author: Jonathan Spiker
 *
 * ========================================================================== */

class Response
{

	public static function say($message, $responseCode = HTTP::HTTP_OK, $defer = false)
	{
		if ($defer) static::deferStart();

		static::code($responseCode);

		echo $message;
		
		if ($defer) static::deferFlush();
	}

	public static function json($toEncode, $responseCode = HTTP::HTTP_OK)
	{
		$json = json_encode($toEncode);

		static::say($json, $responseCode);
	}
	
	public static function sayJSON($json, $responseCode = HTTP::HTTP_OK)
	{
		header("Content-Type: application/json");
		
		static::say($json, $responseCode);
	}

	public static function code($responseCode)
	{
		header("Status: " . HTTP::getMessageForCode($responseCode), true, $responseCode);
//		return http_response_code($responseCode); // Only PHP 5.4+
	}

	public static function legacyResponse($success, $message, $responseCode = HTTP::HTTP_OK, $defer = false)
	{
		if ($defer) static::deferStart();
		
		$response = array('success' => $success, 'message' => $message);

		echo json_encode($response);
		
		if ($defer) static::deferFlush();
	}

	private static function deferStart()
	{
		ob_end_clean();
		header("Connection: close");
		ignore_user_abort(); // optional
		ob_start();
	}

	private static function deferFlush()
	{
		$size = ob_get_length();
		header("Content-Length: $size");
		ob_end_flush(); // Strange behaviour, will not work
		flush();			// Unless both are called !
	}

}

?>
