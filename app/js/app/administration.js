define(['jquery-ui', 'backbone', 'bootstrap', 'util'],
	function() {
		
		$(".tip").tooltip();
		
		var ConfigurationItemListView = Backbone.View.extend(
		{
			initialize: function()
			{	
				this.collection = new Backbone.Collection();
				this.collection.url = urlBase + "resources/configurations";
				
				// renders the list when the address collection is reset/synced/whatever
				this.listenTo(this.collection, "reset", this.addAll);
				this.listenTo(this.collection, "remove", this.addAll);
				this.listenTo(this.collection, "add", this.add);
			
				this.collection.fetch();
			
				this.render();
			},
			render: function()
			{
				// Nothing to do unless we have something like a header to change
				
				return this;
			},
			add: function(item)
			{
				var view = new ConfigurationItemView({
					model: item
				});
			
				this.$el.append(view.render().el);
			},
			addAll: function()
			{
				this.$(".configuration").remove();
			
				// Add view for each address
				this.collection.each(function(item)
				{
					var view = new ConfigurationItemView({
						model: item
					});
				
					this.$el.append(view.render().el);
				}, this);
			}
		});
	
		var ConfigurationItemView = Backbone.View.extend(
		{
			events:
			{
				"click .save": "save"
			},
			className: "configuration well span3",
			template: _.template($("#configuration-item-template").html()),
			initialize: function()
			{
				this.listenTo(this.model, "change", this.render);
				
				this.render();
			},
			render: function()
			{
				this.$el.html(this.template(this.model.toJSON()));
				
				this.$(".btn-info").popover({
					trigger: "hover"
				});
				
				return this;
			},
			save: function()
			{
				var that = this;
				var value;
				
				// Update model with new value
				if (this.model.get("type") === "boolean")
				{
					value = this.$("select").val();
				}
				else
				{
					value = this.$("input").val();
				}
			
				// Save model
				this.model.save({
					value: value
				},
				{
					wait: true,
					success: function()
					{
						alert("The configuration was successfully updated");
						
						that.render();
					},
					error: function(model, xhr, options)
					{
						alert(xhr.responseText);
						
						that.render();
					}
				});
			}
		});

		configurationItemListView = new ConfigurationItemListView({
			el: $("#configuration")
			});
		
		var PrivilegeListView = Backbone.View.extend(
		{
			el: $("#privileges-modal"),
			events:
			{
				"click .save-privileges": "savePrivileges",
				"click .done": "done"
			},
			initialize: function()
			{
				// Initialize collection
				this.collection = new Backbone.Collection();
				this.collection.url = urlBase + "resources/user_levels/" + this.options.levelId + "/privileges";
			
				this.privilegeContainer = this.$el.find("#privilege-list");
				this.header = this.$el.find(".modal-header h3");
				this.changedPrivileges = new Backbone.Collection();
				
				this.privilegeContainer.empty();
			
				// renders the list when the privilege collection is reset/synced/whatever
				this.listenTo(this.collection, "reset", this.addAll);
				this.listenTo(this.collection, "remove", this.addAll);
				this.listenTo(this.collection, "add", this.add);
				this.listenTo(this.collection, "change:granted", this.privilegeChanged);
			
				this.collection.fetch();
				
				this.render();
			},
			render: function()
			{
				// Since the modal already exists
				// Just need to update the header
				this.header.html("Privileges for " + this.options.levelName);
				
				// Make sure modal is shown
				this.$el.modal("show");
				
				return this;
			},
			add: function(item)
			{
				var view = new PrivilegeView({
					model: item
				});
			
				this.privilegeContainer.append(view.render().el);
			},
			addAll: function()
			{
				this.privilegeContainer.empty();
			
				// Add view for each privilege
				this.collection.each(function(item)
				{
					var view = new PrivilegeView({
						model: item
					});
				
					this.privilegeContainer.append(view.render().el);
					
				}, this);
			},
			privilegeChanged: function(model)
			{
				log("privilege changed for: ");
				log(model);
				
				// Add if not in collection (changed from initial state)
				if (!this.changedPrivileges.contains(model))
				{
					this.changedPrivileges.add(model);
				}
				// Remove if already in it (undid change)
				else
				{
					this.changedPrivileges.remove(model);
				}
			},
			savePrivileges: function()
			{
				var successful = true;
				
				// For each model which has changed
				this.changedPrivileges.each(function(model)
				{
					// Save model
					model.save({
						granted: model.get("granted")
					},
					{
						wait: true,
						success: function()
						{
						},
						error: function(model, xhr, options)
						{
							successful = false;
						}
					});
					
				}, this);
				
				if (successful)
				{
					alert("The privileges were successfully updated.")
				}
				else
				{
					alert("The privileges failed to be updated.");
				}
				
				this.changedPrivileges.reset();
				
				this.done();
				
			},
			done: function()
			{
				this.$el.modal("hide");
				
				this.remove();
			}
		});
	
		var PrivilegeView = Backbone.View.extend(
		{
			tagName: "div",
			className: "privilege-view",
			events:
			{
				"click .granted": "updateModel"
			},
			initialize: function()
			{
				// Renders when privilege model is changed
				this.listenTo(this.model, "change", this.render);
			},
			template: _.template($("#privilege-view-template").html()),
			render: function()
			{
				this.$el.html(this.template(this.model.toJSON()));
				
				this.$(".btn-info").popover({
					trigger: "hover"
				});
				
				return this;
			},
			updateModel: function()
			{
				// Update model to reflect granted checkbox
				var grant = this.$(".granted:checked").length > 0 ? "1" : "0";
				this.model.set("granted", grant);
				
			}
		});
		
		$(".edit-privileges").tooltip();
		
		$(".edit-privileges").click(function(){
			
			var levelId = $(this).parent().find("#hidden-level-id").val();
			var levelName = $(this).parent().find("#hidden-level-name").val();
			
			$("#privileges-modal").off();
			
			// Create view for this level
			var privilegeListView = new PrivilegeListView({
				levelId: levelId, 
				levelName: levelName
			});
			
		});
	});