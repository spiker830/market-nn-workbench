define(
		[
			'd3',
			'scatter-matrix',
			'util'
		],
		function ()
		{
			d3.json(urlBase + "resources/jobs", function(data) {
				new ScatterMatrix(data, "training-analysis-graph").render();
			});
		});