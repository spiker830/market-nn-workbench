define(
		[
			'backbone-util',
			'bootstrap',
			'util'
		],
		function ()
		{
			function getWinningRatio(model) {
				if (model.get("count_all") != 0) {
					return (model.get("count_winning") / model.get("count_all") * 100);
				} else {
					return 0;
				}
			}
			
			var jobAnalyzers = new Backbone.Collection(null, {
				url: urlBase + "resources/job_analyzers",
				comparator: function(a, b) {
//					return [model.get("name"), model.get("data")];
					return getWinningRatio(b) - getWinningRatio(a);
				}
			});
			
			var analyzerUsageView = new ListView({
				enableEditing: false,
				collection: jobAnalyzers,
				fields: [
//					{
//						displayName: "ID",
//						dataId: "id"
//					},
					{
						displayName: "Analyzer Name",
						dataId: "name"
					},
					{
						displayName: "Symbol/Data",
						dataId: "data"
					},
					{
						displayName: "Target Symbol",
						dataId: "target"
					},
					{
						displayName: "Trading Strategy",
						dataId: "strategy"
					},
					{
						displayName: "Count",
						dataId: "count_all"
					},
					{
						displayName: "All Winning",
						dataId: "all_winning"
					},
					{
						displayName: "Bull Winning",
						dataId: "bull_winning"
					},
					{
						displayName: "All Winning Ratio",
						dataId: "all_winning_ratio",
						value: function(model) {
							return model.get("all_winning_ratio").toFixed(2);
						}
					},
					{
						displayName: "Bull Winning Ratio",
						dataId: "bull_winning_ratio",
						value: function(model) {
							return model.get("bull_winning_ratio").toFixed(2);
						}
					}
				]
			});
			
			$("#analyzer-usage-view").append(analyzerUsageView.render().el);
			
		});