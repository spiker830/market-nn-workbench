define(
		[
			'backbone-util',
			'bootstrap',
			'util'
		],
		function ()
		{
			var jobAnalyzers = new Backbone.Collection(null, {
				url: urlBase + "resources/analyzer_combinations",
			});
			
			var analyzerCombinationsView = new ListView({
				enableEditing: false,
				collection: jobAnalyzers,
				fields: [
					{
						displayName: "Analyzers",
						dataId: "analyzers",
						template: _.template("<a href=\"" + urlBase + "jobs?analyzers=<%- analyzers %>&target_symbol=<%- target %>&trading_strategy=<%- strategy %>\"><%- analyzers %></a>")
					},
					{
						displayName: "Target Symbol",
						dataId: "target"
					},
					{
						displayName: "Trading Strategy",
						dataId: "strategy"
					},
					{
						displayName: "Count",
						dataId: "count_all"
					},
					{
						displayName: "Winning",
						dataId: "count_winning"
					},
					{
						displayName: "Winning Ratio",
						dataId: "winning_ratio",
						value: function(model) {
							return model.get("winning_ratio").toFixed(2);
						}
					}
				]
			});
			
			$("#analyzer-combinations-view").append(analyzerCombinationsView.render().el);
			
		});