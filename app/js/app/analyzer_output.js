define(
		[
			'jquery.canvasjs',
			'backbone-util',
			'bootstrap',
			'util'
		],
		function ()
		{
			var analyzerData = new Backbone.Collection({}, {
				url: urlBase + "resources/analyzer_data?analyzer_id=" + analyzerId
			});
			
			var analyzerOutput = new Backbone.Collection({}, {
				url: urlBase + "resources/analyzer_output?analyzer_id=" + analyzerId
			});
			
//			var networkSeries = new Backbone.Collection({}, {
//				url: urlBase + "resources/job_results?series_name=network&job_id=" + jobId,
//				comparator: "x"
//			});
//			
//			var backtestSeries = new Backbone.Collection({}, {
//				url: urlBase + "resources/job_results?series_name=backtest&job_id=" + jobId,
//				comparator: "x"
//			});
//			
//			var dataSeries = new Backbone.Collection({}, {
//				url: urlBase + "resources/ticker_data?job_id=" + jobId,
//				comparator: "x"
//			});
//			

			var options = {
				zoomEnabled: true,
				data: [],
				axisX: {
//					interval: 1,
//					intervalType: "month",
					labelAngle: -50
				},
				axisY: {
					minimum: 0
				},
				axisY2: {
					includeZero: false,
					gridThickness: 0
				},
				legend: {
//					horizontalAlign: "center", // "left", "center" , "right"
//					verticalAlign: "center",  // "center", "top" , "bottom"
					fontSize: 15
				}
			};

			analyzerData.fetch({
				success: function (collection) {
					
					if (collection.isEmpty()) return;
					
					if (collection.first().has("open")) {
						
						updateCandlestickDatasets(collection, "data");
						
					} else {
						
						// Need a method to render non-ticker data which could
						// include any number of unknown keys
//						updateDatasets(collection, "network", "column");
						
					}
					
				}
			});
			
			analyzerOutput.fetch({
				success: function (collection) {
					
					var series = {};
					
					// For each item in the response
					collection.each(function(item) {
						
						// For each key in the object (minus date and analyzer_id)
						item.keys().forEach(function(key) {
							
							if (key === "date" || key === "analyzer_id") {
								return;
							}
							
							if (parseInt(key) > 15) return;
							
							if (typeof series[key] === "undefined") {
							
								// Make sure series data exists
								series[key] = {
									type: "line",
									showInLegend: true,
									legendText: key,
				//					axisYType: "secondary",
									dataPoints: []
								};
							}
							
							// Add a data point to the correct series
							series[key].dataPoints.push({
								x: new Date(item.get("date")),
								y: item.get(key)
							});
						
						});
					});
					
					for (var key in series) {
					
						options.data.push(series[key]);
						
					}

					$("#analyzer-data-graph").CanvasJSChart(options);
					
				}
			});
			
//
//			networkSeries.fetch({
//				success: function (collection) {
//					updateDatasets(collection, "network", "column");
//				}
//			});
//
//			backtestSeries.fetch({
//				success: function (collection) {
//					updateDatasets(collection, "backtest", "line");
//				}
//			});
//
//			dataSeries.fetch({
//				success: function (collection) {
//					updateCandlestickDatasets(collection, "data");
//				}
//			});

			function updateDatasets(collection, name, type) {

				var seriesData = {
					type: type,
					showInLegend: true,
					legendText: name,
//					axisYType: "secondary",
					dataPoints: []
				};

				collection.each(function (item) {
					
					seriesData.dataPoints.push({
						x: new Date(item.get("x")),
						y: item.get("y")
					});
				});

				options.data.push(seriesData);

				$("#data-graph").CanvasJSChart(options);
			};

			function updateCandlestickDatasets(collection, name) {

				var seriesData = {
					type: "ohlc",
					axisYType: "secondary",
//					showInLegend: true,
//					legendText: name,
					dataPoints: []
				};
				
				collection.each(function (item) {
				
					var dateString = item.get("date");
					var year = parseInt(dateString.substring(0, 4));
					var month = parseInt(dateString.substring(5,7))-1;
					var day = parseInt(dateString.substring(8));
					var date = new Date(year, month, day, 0, 0, 0);
					
//					log(date);
					
					seriesData.dataPoints.push({
						x: date,
						y: [
							item.get("open"),
							item.get("high"),
							item.get("low"),
							item.get("close")
						]
					});
				});

				options.data.push(seriesData);

				$("#analyzer-data-graph").CanvasJSChart(options);
			};
		});