define(
		[
			'jquery.canvasjs',
			'backbone-util',
			'bootstrap',
			'util'
		],
		function ()
		{
			var errorSeries = new Backbone.Collection({}, {
				url: urlBase + "resources/job_results?series_name=error&job_id=" + jobId,
				comparator: "x"
			});
			
			var options = {
						zoomEnabled: true,
						data: []
					};

			errorSeries.fetch({
				success: function (collection) {

					var seriesData = {
						type: "line",
						dataPoints: []
					};

					collection.each(function (item) {
						seriesData.dataPoints.push({
							x: item.get("x"),
							y: item.get("y")
						});
					});

					options.data.push(seriesData);
					
					$("#error-graph").CanvasJSChart(options);
				}
			});
		});