define(
		[
			'backbone-util',
			'bootstrap',
			'util'
		],
		function ()
		{
			
			var analyzers = new Backbone.Collection(null, {
				url: urlBase + "resources/analyzers"
			});
			
			var analyzersView = new ListView({
//				editEnabled: true,
				addButton: {
					label: "Add Analyzer"
				},
				events: {
					"click .manage-data-source": function(event) {
						manageDataSource(event.currentTarget.id);
					}
				},
				editOnly: true,
				collection: analyzers,
				fields: [
					{
						displayName: "ID",
						dataId: "id"
					},
					{
						displayName: "Class Name",
						dataId: "class_name"
					},
					{
						displayName: "Table",
						dataId: "table",
						locked: true
					},
					{
						displayName: "Column",
						dataId: "column",
						locked: true
					},
					{
						displayName: "Transform Chain Name",
						dataId: "transform_chain_name",
						locked: true
					},
					{
						displayName: "Enabled",
						dataId: "enabled"
					},
					{
						displayName: "Ticker Analyzer",
						dataId: "ticker"
					},
					{
						displayName: "Data Source",
						template: _.template($("#analyzer-data-source-link-template").html())
					}
				]
			});
			
			var MultiSelectButtonView = Backbone.View.extend({
				className: "btn-group",
				initialize: function() {
					this.selected = [];
				},
				attributes: {
					"data-toggle": "buttons-checkbox",
					"style": "float:left"
				},
				buttonTemplate: _.template($("#multi-select-button-template").html()),
				events: {
					"click button": "clicked"
				},
				clicked: function (event) {
					
					if (!this.$(event.target).hasClass("selected")) {
						this.trigger("select", event);
					}
					
					// Have to do this to make sure class is available when
					// a click handler checks for selected (can't use .active)
					this.$(event.target).toggleClass("selected");
					this.trigger("click", event);
					
				},
				render: function () {

					// For each input, render the appropriate view
					_.each(this.options.buttons, this.renderButton, this);
					
					return this;
				},
				getSelected: function () {

					var selectedButtons = this.$(".selected");
					var selected = [];

					_.each(selectedButtons, function (selectedButton) {
						selected.push({
							dataId: $(selectedButton).attr("data-id"),
							displayName: $(selectedButton).attr("data-display-name")
						});
					});

					return selected;

				},
				renderButton: function (buttonConfig) {

					this.$el.append(this.buttonTemplate(buttonConfig));

				}
			});
			
			var TransformSelectionView = Backbone.View.extend({
				
				tag: "div",
				
				initialize: function(options) {
					
					var that = this;
					
					var transforms = new Backbone.Collection({}, {
						url: urlBase + "resources/transforms"
					});
					
					var buttons = [];
					
					transforms.fetch({
						success: function(collection) {
							log(collection);
							collection.each(function(model) {
								buttons.push({
									dataId: model.id,
									displayName: model.get("name")
								});
							});
						},
						
						async: false
					});
					
					// Transform buttons
					this.transformButtons = new MultiSelectButtonView({
						buttons: buttons
					});
					
					this.transformButtons.on("select", function(event) {
						
						var transformId = $(event.currentTarget).attr("data-id");
						
						// Add transform to chain
						that.transformList.collection.create({
							transform_id: transformId,
							transform_chain_id: that.options.chainId
						});
						
					});
					
					// Transform list
					this.transformList = new ListView({
						url: urlBase + "resources/transform_chains/" + this.options.chainId + "/transforms",
						fields: [
							{
								displayName: "ID",
								dataId: "transform_id"
							},
							{
								displayName: "Name",
								dataId: "name"
							},
							{
								displayName: "Class Name",
								dataId: "class_name"
							}
						]
					});
				},
				
				render: function() {
					
					this.$el.append(this.transformButtons.render().el);
					this.$el.append(this.transformList.render().el);
					
					return this;
				}
				
			});
			
			var DataSourceView = Backbone.View.extend({
				
				tag: "div",

				initialize: function(options) {
					
					this.model = analyzers.get(options.analyzerId);
					
					var table = "";
					var column = "";
					var transformChainId = 0;
					
					this.analyzerData = new Backbone.Collection({},
					{
						url: urlBase + "resources/analyzer_data"
					});
					
					this.analyzerData.fetch({
						success: function(collection) {
							if (typeof collection.at(0) !== "undefined") {
								transformChainId = collection.at(0).get("transform_chain_id");
							}
						},
						data: {
							analyzer_id: this.model.id
						},
						async: false
					});
					
					this.transformChains = new Backbone.Collection({}, {
						url: urlBase + "resources/transform_chains"
					});
					
					this.transformChains.fetch({
						success: function(collection) {
							if (typeof collection.get(transformChainId) !== "undefined") {
								table = collection.get(transformChainId).get("table");
								column = collection.get(transformChainId).get("column");
							}
						},
						async: false
					});
					
					// Table select
					this.tableSelect = new Select({
						url: urlBase + "resources/data_tables",
						displayDataId: "name",
						valueDataId: "name",
						name: "table",
						label: "Table",
						defaultValue: table
					});
					
					// Column select
					this.columnSelect = new Select({
						url: urlBase + "resources/data_columns",
						displayDataId: "name",
						valueDataId: "name",
						name: "column",
						label: "Column",
						defaultValue: column
					});
					
					this.saveDataButton = $("<button class=\"btn btn-primary save-data\">Save</button>");
					
					// Transform selection view
					this.transformSelection = new TransformSelectionView({
						analyzerId: options.analyzerId,
						chainId: transformChainId
					});
				},
				
				saveDataTableColumn: function() {
					
					var that = this;
					
					// Remove any previous analyzer_data
					this.analyzerData.each(function(model) {
						model.destroy();
					});
					
					// Create transform_chain with table and column
					this.transformChains.create({
						table: this.tableSelect.getValue(),
						column: this.columnSelect.getValue()
					}, {
						success: function(model) {
							
							log("Successfully created transform chain " + model.get("id"));
							
							// Create analyzer_data and reference transform_chain
							that.analyzerData.create({
								analyzer_id: that.model.id,
								transform_chain_id: model.id
							},
							{
								success: function(model) {
									alert("Successfully created analyzer_data and empty transform_chain");
									analyzersView.repopulate();
								}
							});
						}
					});
				},
				
				render: function() {
					
					this.$el.append(this.tableSelect.render().el);
					this.$el.append(this.columnSelect.render().el);
					this.$el.append(this.saveDataButton);
					this.saveDataButton.on("click", this.saveDataTableColumn.bind(this));
					this.$el.append(this.transformSelection.render().el);
					
					return this;
				}
			});
			
			function manageDataSource(analyzerId) {
				
				// Render data source modal
				var dataSourceModal = new ModalView({
					heading: "Data Source",
					body: new DataSourceView({
						analyzerId: analyzerId
					})
				});
			}
			
			$("#analyzers-view").append(analyzersView.render().el);
		});