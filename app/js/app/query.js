define(
		[
			'jquery.canvasjs',
			'backbone-util',
			'bootstrap',
			'util',
			'ace'
		],
		function ()
		{
			var editor = ace.edit("editor");
			editor.setTheme("ace/theme/monokai");
			editor.getSession().setMode("ace/mode/sql");
			
			$("#execute").click(function() {
				
				// Get query
				var query = editor.getValue().replace(/\n/g, " ");
				
				console.log(query);
				
				//return;
				
				// Get data for query
				var dataSeries = new Backbone.Collection({}, {
					url: urlBase + "resources/query?query=" + query
				});
				
				dataSeries.fetch({
					success: function(data) {
						updateDatasets(
								data,
								"x",
								"y",
								"",
								"scatter"
						);
					}
				});
				
				// Graph it
			});
			
			var options = {
//				zoomEnabled: true,
				data: [],
				axisX: {
//					interval: 1,
//					intervalType: "month",
					labelAngle: -50
				},
				axisY: {
					includeZero: false
				},
////				axisY2: {
////					includeZero: false,
////					gridThickness: 0
////				},
//				legend: {
////					horizontalAlign: "center", // "left", "center" , "right"
////					verticalAlign: "center",  // "center", "top" , "bottom"
//					fontSize: 15
//				}
			};
			
			function updateDatasets(collection, x, y, name, type) {
				
				options.data = [];

				var seriesData = {
					type: type,
//					showInLegend: true,
//					legendText: name,
//					axisYType: "secondary",
					dataPoints: []
				};

				collection.each(function (item) {
					
					seriesData.dataPoints.push({
						x: new Date(item.get(x)),
						y: parseFloat(item.get(y))
					});
				});

				options.data.push(seriesData);

				$("#query-graph").CanvasJSChart(options);
			};
			
//			var TableDataFiltersView = Backbone.View.extend({
//				
//				initialize: function() {
//					
//					var that = this;
//					this.filter = "<div id=\"filter\"><label style=\"display: inline\">= </label><input id=\"filter-input\" type=\"text\"></input></div>";
//					this.filter2 = "<div id=\"filter2\"><label style=\"display: inline\">= </label><input id=\"filter2-input\" type=\"text\" value=\"adjusted_close\" /></div>";
//					
//					// Column select
//					this.columnSelect = new Select({
//						url: urlBase + "resources/data_columns",
//						displayDataId: "name",
//						valueDataId: "name",
//						name: "column",
//						label: "Column to Graph"
//					});
//					
//					// Column filter
//					this.columnFilterSelect = new Select({
//						url: urlBase + "resources/data_columns",
//						displayDataId: "name",
//						valueDataId: "name",
//						name: "column",
//						label: "Filter"
//					});
//					
//					// Column filter
//					this.columnFilter2Select = new Select({
//						url: urlBase + "resources/data_columns",
//						displayDataId: "name",
//						valueDataId: "name",
//						name: "column",
//						label: "Filter"
//					});
//					
//					// Table select
//					this.tableSelect = new Select({
//						url: urlBase + "resources/data_tables",
//						displayDataId: "name",
//						valueDataId: "name",
//						name: "table",
//						label: "Table to Graph",
//						onChange: function () {
//							
//							console.log("table changed");
//							var table = that.tableSelect.getValue();
//							
//							that.columnSelect.$el.remove();
//							
//							// Column select
//							that.columnSelect = new Select({
//								url: urlBase + "resources/data_columns?table_name=" + table,
//								displayDataId: "name",
//								valueDataId: "name",
//								name: "column",
//								label: "Column to Graph",
//								defaultValue: "value"
//							});
//							
//							that.columnFilterSelect.$el.remove();
//							
//							// Column select
//							that.columnFilterSelect = new Select({
//								url: urlBase + "resources/data_columns?table_name=" + table,
//								displayDataId: "name",
//								valueDataId: "name",
//								name: "column",
//								label: "Filter",
//								defaultValue: "source_table"
//							});
//							
//							that.columnFilter2Select.$el.remove();
//					
//							// Column filter
//							that.columnFilter2Select = new Select({
//								url: urlBase + "resources/data_columns",
//								displayDataId: "name",
//								valueDataId: "name",
//								name: "column",
//								label: "Filter",
//								defaultValue: "source_column"
//							});
//							
//							that.$el.append(that.columnSelect.render().el);
//							
//							that.$el.append(that.columnFilterSelect.render().el);
//							that.$("#filter").remove();
//							that.$el.append(that.filter);
//							
//							that.$el.append(that.columnFilter2Select.render().el);
//							that.$("#filter2").remove();
//							that.$el.append(that.filter2);
//						}
//					});
//				},
//				
//				render: function() {
//					
//					this.$el.append(this.tableSelect.render().el);
//					this.$el.append(this.columnSelect.render().el);
//					this.$el.append(this.columnFilterSelect.render().el);
//					this.$el.append(this.filter);
//					this.$el.append(this.columnFilter2Select.render().el);
//					this.$el.append(this.filter2);
//					return this;
//				},
//				
//				getMap: function() {
//					
//					var map = {
//						"table": this.tableSelect.getValue()
//					};
//					
//					map[this.columnFilterSelect.getValue()] = $("#filter-input").val();
//					map[this.columnFilter2Select.getValue()] = $("#filter2-input").val();
//					
//					return map;
//				},
//				
//				getColumnToGraph: function() {
//					var column = this.columnSelect.getValue();
//					console.log("graphing column: " + column);
//					return column;
//				}
//			});
//			
//			var tableDataFiltersView = new TableDataFiltersView();
//			
//			$("#table-data-filters").append(tableDataFiltersView.render().el);
//			
//			var graphButton = $("<button class=\"btn\">Graph</button>");
//			
//			graphButton.click(function() {
//				console.log(tableDataFiltersView.getMap());
//			
//				var dataSeries = new Backbone.Collection({}, {
//					url: urlBase + "resources/data?" + $.param(tableDataFiltersView.getMap()),
//	//				comparator: "x"
//				});
//				
//				dataSeries.fetch({
//					success: function(data) {
//						updateDatasets(
//								data,
//								"date",
//								tableDataFiltersView.getColumnToGraph(),
//								"Table Data",
//								"line"
//						);
//					}
//				});
//			});
//			
//			$("#table-data-graph-button").append(graphButton);
//			
//			var options = {
//				zoomEnabled: true,
//				data: [],
//				axisX: {
////					interval: 1,
////					intervalType: "month",
//					labelAngle: -50
//				},
//				axisY: {
//					includeZero: false
//				},
////				axisY2: {
////					includeZero: false,
////					gridThickness: 0
////				},
//				legend: {
////					horizontalAlign: "center", // "left", "center" , "right"
////					verticalAlign: "center",  // "center", "top" , "bottom"
//					fontSize: 15
//				}
//			};
//
//			function updateDatasets(collection, x, y, name, type) {
//				
//				options.data = [];
//
//				var seriesData = {
//					type: type,
//					showInLegend: true,
//					legendText: name,
////					axisYType: "secondary",
//					dataPoints: []
//				};
//
//				collection.each(function (item) {
//					
//					seriesData.dataPoints.push({
//						x: new Date(item.get(x)),
//						y: parseFloat(item.get(y))
//					});
//				});
//
//				options.data.push(seriesData);
//
//				$("#table-data-graph").CanvasJSChart(options);
//			};
		});