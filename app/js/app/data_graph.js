define(
		[
			'jquery.canvasjs',
			'backbone-util',
			'bootstrap',
			'util'
		],
		function ()
		{
			var networkSeries = new Backbone.Collection({}, {
				url: urlBase + "resources/population_genome_backtest_series?series_name=network&population_genome_id=" + genomeId,
				comparator: "x"
			});
			
//			// Get the enabled training strategies
//			var strategies = new Backbone.Collection({}, {
//				url: urlBase + "resources/trading_strategies?enabled=1",
//				comparator: "x"
//			});
			
//			strategies.fetch({
//				success: function (strategies) {
//					
//					// For each strategy
//					strategies.each(function (strategy) {
//						
//						log("loading data for strategy: " + strategy.get("name"));
						
						// Load series
						var backtestSeries = new Backbone.Collection({}, {
							url: urlBase + "resources/population_genome_backtest_series?series_name=gain&population_genome_id=" + genomeId,
							comparator: "x"
						});

						backtestSeries.fetch({
							success: function (collection) {
								updateDatasets(collection, "trading", "line");
							}
						});
//					});
//				}
//			});

			var dataSeries = new Backbone.Collection({}, {
				url: urlBase + "resources/ticker_data?population_genome_id=" + genomeId,
				comparator: "x"
			});

			networkSeries.fetch({
				success: function (collection) {
					updateDatasets(collection, "network", "column");
				}
			});

			dataSeries.fetch({
				success: function (collection) {
					updateCandlestickDatasets(collection, "data");
				}
			});

			var options = {
				zoomEnabled: true,
				data: [],
				axisX: {
//					interval: 1,
//					intervalType: "month",
					labelAngle: -50
				},
				axisY: {
					//minimum: 0
				},
				axisY2: {
					includeZero: false,
					gridThickness: 0
				},
				legend: {
//					horizontalAlign: "center", // "left", "center" , "right"
//					verticalAlign: "center",  // "center", "top" , "bottom"
					fontSize: 15
				}
			};

			function updateDatasets(collection, name, type) {

				var seriesData = {
					type: type,
					showInLegend: true,
					legendText: name,
//					axisYType: "secondary",
					dataPoints: []
				};

				collection.each(function (item) {
					
					seriesData.dataPoints.push({
						x: new Date(item.get("x")),
						y: item.get("y")
					});
				});

				options.data.push(seriesData);

				$("#data-graph").CanvasJSChart(options);
			};

			function updateCandlestickDatasets(collection, name) {

				var seriesData = {
					type: "candlestick",
					axisYType: "secondary",
//					showInLegend: true,
//					legendText: name,
					dataPoints: []
				};
				
				collection.each(function (item) {
				
					var dateString = item.get("date");
					var year = parseInt(dateString.substring(0, 4));
					var month = parseInt(dateString.substring(5,7))-1;
					var day = parseInt(dateString.substring(8));
					var date = new Date(year, month, day, 0, 0, 0);
					
//					log(date);
					
					seriesData.dataPoints.push({
						x: date,
						y: [
							item.get("adjusted_open"),
							item.get("adjusted_high"),
							item.get("adjusted_low"),
							item.get("adjusted_close")
						]
					});
				});

				options.data.push(seriesData);

				$("#data-graph").CanvasJSChart(options);
			};
		});