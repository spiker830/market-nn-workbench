define(
		[
			'jquery-ui',
			'jquery.canvasjs',
			'backbone-util',
			'bootstrap',
			'util'
		],
		function ()
		{
			var gains;
			
			var tickerSymbolSelect = new Select({
				url: urlBase + "resources/tickers",
				valueDataId: "id",
				displayDataId: "symbol",
				name: "ticker-symbol",
				label: "Ticker Symbol",
				onChange: function() {
					fetch();
				}
			});
			
			$("#ticker-symbol-select").append(tickerSymbolSelect.render().el);
			
			var options = {
				zoomEnabled: true,
				data: [],
				axisX: {
					labelAngle: -50
				},
				axisY: {
					includeZero: false
				}
			};
			
			fetch();
			
			function fetch() {
				
				var symbol = tickerSymbolSelect.getValue();
				
				if (typeof symbol === "undefined") {
					return;
				}
			
				gains = new Backbone.Collection({}, {
					url: urlBase + "resources/population_gains?ticker_id=" + symbol,
					comparator: "x"
				});
			
				gains.fetch({
					success: function () {
						
						options.data = [];
						
						renderField("min_gain", "Minimum Gain", "line", "red");
						renderField("avg_gain", "Average Gain", "line", "orange");
						renderField("max_gain", "Maximum Gain", "line", "green");
						renderField("base_gain", "Base Gain", "line", "blue");
					}
				});
			
			}
			
			function renderField(field, name, type, color, axisYType) {

				var seriesData = {
					type: type,
					showInLegend: true,
					legendText: name,
//					toolTipContent: name + ": {y}<br />" +
//							"Improvement: {x}",
//					axisYType: axisYType,
					dataPoints: []
				};

				if (typeof color !== "undefined") {
					seriesData.color = color;
				}

				if (gains === null) {
					return;
				}
				
				gains.each(function (model) {
					
					var x = model.get("generation");
					var y = model.get(field);
					
					seriesData.dataPoints.push({
						x: x,
						y: y
					});
				});
				
				options.data.push(seriesData);

				$("#population-gains-graph").CanvasJSChart(options);
			};
			
		});