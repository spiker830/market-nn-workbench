define(
		[
			'backbone-util',
			'bootstrap',
			'util'
		],
		function ()
		{
			var jobAnalyzers = new Backbone.Collection(null, {
				url: urlBase + "resources/proven_combinations",
			});
			
			var provenCombinationsView = new ListView({
				enableEditing: false,
				collection: jobAnalyzers,
				fields: [
					{
						displayName: "Analyzers",
						dataId: "analyzers",
						template: _.template("<a href=\"" + urlBase + "jobs?analyzers=<%- analyzers %>&target_symbol=<%- target %>&trading_strategy=<%- strategy %>\"><%- analyzers %></a>")
					},
					{
						displayName: "Target Symbol",
						dataId: "target"
					},
					{
						displayName: "Trading Strategy",
						dataId: "strategy"
					},
					{
						displayName: "Count",
						dataId: "count_all"
					},
					{
						displayName: "All Winning",
						dataId: "all_winning"
					},
					{
						displayName: "Bull Winning",
						dataId: "bull_winning"
					},
					{
						displayName: "All Winning Ratio",
						dataId: "all_winning_ratio",
						value: function(model) {
							return model.get("all_winning_ratio").toFixed(2);
						}
					},
					{
						displayName: "Bull Winning Ratio",
						dataId: "bull_winning_ratio",
						value: function(model) {
							return model.get("bull_winning_ratio").toFixed(2);
						}
					}
				]
			});
			
			$("#proven-combinations-view").append(provenCombinationsView.render().el);
			
		});