define(
	[
	'jquery-ui',
	'backbone',
	'bootstrap',
	'util'
	],
	
	function()
	{
		var levels = {};
		var showInactive = false;
		var loadedEmployees;
		var uploader;
		
		// Get list of possible employee levels
		$.getJSON(urlBase + "employees/getLevels", function (userLevels)
		{
			levels = userLevels;
	
			updateEmployeesTable();
			
		});
		
		$("#toggleInactive button").click(function()
		{
			// Toggle boolean
			showInactive = !showInactive;
			
			// Determine button text based on new boolean value
			var buttonText = showInactive ? "Hide Inactive" : "Show Inactive";
			
			$("#toggleInactive button").text(buttonText);
			
			updateEmployeesTable();
		});

		function updateEmployeesTable()
		{
			var employeeListURL = urlBase + "employees/getDetailedList";

			// Call wage service with project id as query param
			$.getJSON(employeeListURL, function (employees)
			{
				loadedEmployees = employees;
				
				var employeesTbody = $('#employees tbody');
				employeesTbody.empty();

				$.each(employees, function(index, employee)
				{
					// If we aren't showing inactive and this employee is inactive, return
					if (!showInactive && employee["status"] == 2) return;
					
					tr = "<tr>";

					tr += "<td>" +
					getTextInput("firstName", "firstName", employee['firstName']) +
					"</td>";

					tr += "<td>" +
					getTextInput("lastName", "lastName", employee['lastName']) +
					"</td>";
				
					tr +=
					"<td>" +
					"	<button class='btn btn-mini btn-primary manageProfile' rel='" + employee['id'] + "'>" +
					"		<i class='icon-white icon-list'></i>" +
					"	</button>" +
					"</td>";
				
					tr +=
					"<td>" +
					"	<button class='btn btn-mini btn-primary manageFiles' rel='" + employee['id'] + "'>" +
					"		<i class='icon-white icon-file'></i>" +
					"	</button>" +
					"</td>";

					var canLogin =
					{
						enabled: 1,
						disabled: 0
					}

					tr += "<td>" +
					getSelect("canLogin", "canLogin", canLogin, employee['canLogin']) + "</td>";

					tr += "<td>" +
					getTextInput("username", "username", employee['username']) +
					"</td>";

					tr += "<td>";
					tr += "	<button class='btn btn-mini btn-primary changePassword' rel='" + employee['id'] + "'>";
					tr += "		<i class='icon-white icon-lock'></i>";
					tr += "	</button>";
					tr += "</td>";

					var status =
					{
						active: 1,
						inactive: 2
					}

					tr += "<td>" +
					getSelect("status", "status", status, employee['status']) +
					"</td>";
				
					tr += "<td>";
					tr += "	<button class='btn btn-mini btn-primary manageWages' rel='" + employee['id'] + "'>";
					//					tr += "		<i class='icon-white icon-pencil'></i>";
					tr += "&nbsp;$&nbsp;"
					tr += "	</button>";
					tr += "</td>";
				
					tr += "<td>" +
					getSelect("level", "level", levels, employee['level']) +
					"</td>";
				
					var levelName = employee['levelName'];
					
					// If this is a user which can have direct reports	
					if ($.inArray(levelName, config.HAS_REPORTS) != -1)
					{
						tr += "<td>";
						tr += "	<button class='btn btn-mini btn-primary manageReports' rel='" + employee['id'] + "'>";
						tr += "		<i class='icon-white icon-user'></i>";
						tr += "	</button>";
						tr += "</td>";
					}
					else
					{
						tr += "<td></td>";
					}

					tr += "<td>";
					tr += "	<button class='btn btn-mini btn-primary save' rel='" + employee['id'] + "'>";
					tr += "		<i class='icon-white icon-ok'></i>";
					tr += "	</button>";
					tr += "</td>";
					
					tr += "<td>";
					tr += "	<button class='btn btn-mini btn-danger delete' rel='" + employee['id'] + "'>";
					tr += "		<i class='icon-white icon-trash'></i>";
					tr += "	</button>";
					tr += "</td>";

					tr += "</tr>\n";

					employeesTbody.append(tr);
					
				});
				
				activateTooltips();

				var addEmployeeRow = getAddEmployeeRow();

				employeesTbody.append("<tr>" + addEmployeeRow + "</tr>");

				$('.add').click(function()
				{
					var tr = $(this).parents('tr').filter(':first');

					populateAddEmployeeRow(tr);
				});

				$('.save').click(function()
				{
					$(".save").tooltip('destroy');
					
					var tr = $(this).parents('tr').filter(':first');

					if (!validateEmployee(tr, false))
						return false;

					var data = serializeEmployee(tr);
					log(data);
	
					var employeeId = $(this).attr('rel');

					// Update employee on server
					updateEmployee(employeeId, data);

					return false;

				});

				$('.changePassword').click(function()
				{
					// Prompt for the updated password
					var password = prompt("Enter the new password");
					var employeeId = $(this).attr('rel');

					if (password == null) // Cancelled
					{
						return false;
					}
					else if (password == "") // Validation
					{
						alert("You must enter a valid password");
					}
					else
					{
						// Change the password
						$.ajax({
							url: urlBase + "employees/changePassword/" + employeeId,
							dataType: "json",
							type: "post",
							data: "password=" + password,
							success: function(response)
							{
								alert(response['message']);
							}
						});
					}

					return false;

				});
				
				$('.manageReports').click(function()
				{
					var employeeId = $(this).attr('rel');
					var firstName = $(this).parents('tr').filter(':first').find('#firstName').val();
					var lastName = $(this).parents('tr').filter(':first').find('#lastName').val();
					var name = firstName + " " + lastName;
					
					$('#directReportsModal .modal-header h3').html("Direct Reports for " + name);
					
					loadPossibleDirectReports($('#possibleReports'), employeeId);
					
					loadCurrentDirectReports($('#directReportsModal .modal-body tbody'), employeeId);
					
				});
				
				$('.manageWages').click(function()
				{
					// Get employee id
					var employeeId = $(this).attr('rel')
					
					// Load wages in modal (which then shows modal)
					loadWages($('#wagesModal .modal-body tbody'), employeeId);
					
				});
				
				$('.manageProfile').click(function()
				{
					// Get employee id
					var employeeId = $(this).attr('rel')
					
					// Load profile in modal (which then shows modal)
					loadProfile($('#profileModal'), employeeId);
					
				});
				
				$('.manageFiles').click(function()
				{
					// Get employee id
					var employeeId = $(this).attr('rel')
					
					// Load files in modal (which then shows modal)
					loadFiles($('#filesModal'), employeeId);
					
				});

				$('.delete').click(function()
				{
					var firstName = $(this).parents('tr').filter(':first').find('#firstName').val();

					if (confirm("Are you sure you want to delete " + firstName + "?"))
					{
						$(this).tooltip('destroy');
						
						var employeeId = $(this).attr('rel');

						// Delete the employee
						$.ajax({
							url: urlBase + "employees/delete/" + employeeId,
							dataType: "json",
							type: "get",
							success: function(response)
							{
								alert(response['message']);

								updateEmployeesTable();
							}
						});
					}

					return false;

				});

			});
		}
		
		function activateTooltips()
		{
			$(".manageReports").tooltip({
				title: "Manage direct reports"
			});
			
			$(".manageWages").tooltip({
				title: "Manage wages"
			});
			
			$(".changePassword").tooltip({
				title: "Change password"
			});
			
			$(".save").tooltip({
				title: "Save changes"
			});
			
			$(".delete").tooltip({
				title: "Delete"
			});
		}

		function getTextInput(id, name, value, size)
		{
			if (typeof size === "undefined" || size == null)
				size = "small";

			return "<input type='text' id='" + id + "' name='" + name + "' value='" + value + "' class='input-" + size + "' />";
		}

		/**
	 * Values object: { "name":"value", "name2":"value2" }
	 */
		function getSelect(id, name, values, selected, size)
		{
			var select;

			if (typeof size === "undefined" || size == null)
				select = "<select id='" + id + "' name='" + name + "'>";
			else
				select = "<select id='" + id + "' name='" + name + "' class='input-" + size + "'>";


			for (var property in values)
			{
				if (values[property] == selected)
					select += "<option selected='selected' value='" + values[property] + "'>" + property + "</option>";
				else
					select += "<option value='" + values[property] + "'>" + property + "</option>";

			}

			select += "</select>";

			return select;
		}

		function validateEmployee(tr, checkPassword)
		{
			if (tr.find('#firstName').val() == "")
			{
				alert("First name must not be empty");
				tr.find('#firstName').focus();
				return false;
			}
			else if (tr.find('#lastName').val() == "")
			{
				alert("Last name must not be empty");
				tr.find('#lastName').focus();
				return false;
			}
			else if (tr.find('#username').val() == "")
			{
				alert("Username must not be empty");
				tr.find('#username').focus();
				return false;
			}
			else if (checkPassword && tr.find('#password').val() == "")
			{
				alert("Password must not be empty");
				tr.find('#password').focus();
				return false;
			}
			else if (tr.find('#wage').val() == "")
			{
				alert("Wage must not be empty");
				tr.find('#wage').focus();
				return false;
			}
			else
				return true;
		}

		function validateProfile(container)
		{
			// Check notes
			if (container.find("#inputNotes").val().length > 255)
			{
				alert("The notes field cannot be larger than 255 characters");
				tr.find('#inputNotes').focus();
				return false;
			}
			else
			{
				return true;
			}
		}

		function serializeEmployee(tr)
		{
			var firstName = tr.find('#firstName').val();
			var lastName = tr.find('#lastName').val();
			var canLogin = tr.find('#canLogin :selected').val();
			var username = tr.find('#username').val();
			var status = tr.find('#status :selected').val();
			var wage = tr.find('#wage').val();
			var level = tr.find('#level :selected').val();

			var data =
			"firstName=" + firstName +
			"&lastName=" + lastName +
			"&canLogin=" + canLogin +
			"&username=" + username +
			"&status=" + status +
			"&wage=" + wage +
			"&level=" + level;

			return data;
		}

		function serializeProfile(container)
		{
			var email = container.find('#inputEmail').val();
			var phone = container.find('#inputPhone').val();
			var hired = container.find('#inputHired').val();
			var terminated = container.find('#inputTerminated').val();
			var notes = container.find('#inputNotes').val();

			var data =
			"email=" + email +
			"&phone=" + phone +
			"&hired=" + hired +
			"&terminated=" + terminated +
			"&notes=" + notes;

			return data;
		}
		
		function loadPossibleDirectReports(container, employeeId)
		{
			// Load possible direct reports
			$.ajax({
				url: urlBase + "employees/possibleReports/" + employeeId,
				dataType: "json",
				type: "get",
				success: function(response)
				{
					var reports = response.message;
					
					var select = getSelect(
						"possibleReportsSelect",
						"possibleReportsSelect", 
						reports, 
						null, 
						"medium");
						
					container.empty();
					container.append(select);
					container.append(
						" <button class='btn btn-primary addReport' rel='" + employeeId + "'>" +
						"<i class='icon-white icon-plus'></i>" +
						"</button>");
					
					//					$(".addReport").tooltip({
					//						title: "Add report"
					//					});
					
					// Add click handler for add
					$('.addReport').click(function()
					{
						// Get value of report to add
						var reportId = $('#possibleReportsSelect :selected').val();
						
						// Send request to add report
						$.ajax({
							url: urlBase + "employees/addReport/" + employeeId + "/" + reportId,
							dataType: "json",
							type:"get",
							success: function(response)
							{
								// Print message
								alert(response['message']);
						
								// Reload possible and current
								loadPossibleDirectReports($('#possibleReports'), employeeId);
								loadCurrentDirectReports($('#directReportsModal .modal-body tbody'), employeeId);
								
							}
							
						});
						
					});
					
				}
			});
		}
		
		function loadCurrentDirectReports(container, employeeId)
		{
			// Load existing direct reports for this user
			$.ajax({
				url: urlBase + "employees/currentReports/" + employeeId,
				dataType: "json",
				type: "get",
				success: function(response)
				{
					var reports = response.message;
							
					var rows;
							
					// For each report
					$.each(reports, function(index, report)
					{
						// Generate row
						rows += "<tr>";
								
						rows += "<td>" + report['firstName'] + " " + report['lastName'] + "</td>";
								
						rows +=
						"<td>" +
						"	<button class='btn btn-mini btn-danger deleteReport' rel='" + report['id'] + "'>" +
						"		<i class='icon-white icon-trash'></i>" +
						"	</button>" +
						"</td>";
								
						rows += "</tr>";
								
					});
					
					container.empty();
					container.append(rows);
							
					// Show modal
					$('#directReportsModal').modal("show");
					
					$(".deleteReport").tooltip({
						title: "Remove direct report"
					});
							
					// Handle delete click
					$('.deleteReport').click(function()
					{
						$(".deleteReport").tooltip("destroy");

						// Get id if report
						var reportId = $(this).attr('rel');
						
						// Send request to delete report
						$.ajax({
							url: urlBase + "employees/deleteReport/" + employeeId + "/" + reportId,
							dataType: "json",
							type:"get",
							success: function(response)
							{
								// Print message
								alert(response['message']);
						
								// Reload possible and current
								loadPossibleDirectReports($('#possibleReports'), employeeId);
								loadCurrentDirectReports($('#directReportsModal .modal-body tbody'), employeeId);
								
							}
						});
						
					});
					
				}
			});
		}
					
		function updateEmployee(employeeId, data)
		{
			$.ajax({
				url: urlBase + "employees/update/" + employeeId,
				dataType: "json",
				type: "post",
				data: data,
				success: function(response)
				{
					alert(response['message']);

					updateEmployeesTable();
				}
			});
		}

		function populateAddEmployeeRow(tr)
		{
			var row = "<td>" + getTextInput("firstName", "firstName", "") + "</td>";
			row += "<td>" + getTextInput("lastName", "lastName", "") + "</td>";
			row += "<td></td>";
			row += "<td></td>";
			//			row += "<td>" + getTextInput("email", "email", "") + "</td>";
			//			row += "<td>" + getTextInput("phone", "phone", "") + "</td>";

			var canLogin =
			{
				enabled: 1,
				disabled: 0
			}

			row += "<td>" + getSelect("canLogin", "canLogin", canLogin, "mini") + "</td>";
			row += "<td>" + getTextInput("username", "username", "") + "</td>";
			row += "<td>" + getTextInput("password", "password", "") + "</td>";

			var status =
			{
				active: 1,
				inactive: 2
			}

			row += "<td>" + getSelect("status", "status", status, "") + "</td>";
			row += "<td></td>";

			row += "<td>" + getSelect("level", "level", levels, "") + "</td>";
			row += "<td></td>";
			row += "<td>";
			row += "	<button class='btn btn-mini btn-primary add'>";
			row += "		<i class='icon-white icon-ok'></i>";
			row += "	</button>";
			row += "</td>";
			row += "<td>";
			row += "	<button class='btn btn-mini btn-danger cancel'>";
			row += "		<i class='icon-white icon-remove'></i>";
			row += "	</button>";
			row += "</td>";

			tr.empty();
			tr.append(row);

			$('.add').click(function()
			{
				var tr = $(this).parents('tr').filter(':first');

				if (!validateEmployee(tr, true))
					return false;

				var data = serializeEmployee(tr);

				data += "&password=" + tr.find('#password').val();

				$.ajax({
					url: urlBase + "employees/add",
					dataType: "json",
					type: "post",
					data: data,
					success: function(response)
					{
						alert(response['message']);

						updateEmployeesTable();
					}
				});

				return false;

			});

			$('.cancel').click(function()
			{
				var addEmployeeTR = $(this).parents('tr').filter(':first');

				addEmployeeTR.empty();
				addEmployeeTR.append(getAddEmployeeRow());

				$('.add').click(function()
				{
					var tr = $(this).parents('tr').filter(':first');

					populateAddEmployeeRow(tr);
				});
			});
		}

		function getAddEmployeeRow()
		{
			return "<td colspan='13'>" +
			"<button class='btn btn-primary add'>" +
			"<i class='icon-white icon-plus'></i> Add New Employee" +
			"</button>" +
			"</td>";
		}
		
		function loadWages(container, employeeId)
		{
			container.empty();
			
			// Change heading
			var heading = $('#wagesModal .modal-header h3');
			heading.html("Manage Wages");

			// Get the list of wages and display them
			$.ajax({
				url: urlBase + "wages/employeeWages/" + employeeId,
				dataType: "json",
				type: "get",
				success: function(response)
				{
					if (!response.success)
					{
						alert(response.message);
					}
					else
					{
						var wages = response.message;
					
						var html = "";
					
						$.each(wages, function(index, wage) {
						
							html += "<tr><td>" +
							getTextInput("wageName", "wageName", wage['name']) +
							"</td>";

							html += "<td>" +
							getTextInput("wageAmount", "wageAmount", wage['wage']) +
							"</td>";
					
							html +=	"<td>\n" +
							"	<button class='btn btn-mini btn-primary saveWage' rel='" + wage['id'] + "'>" +
							"		<i class='icon-white icon-ok'></i>" +
							"	</button>" +
							"</td>";

							html +=
							"<td>" +
							"	<button class='btn btn-mini btn-danger deleteWage' rel='" + wage['id'] + "'>" +
							"		<i class='icon-white icon-trash'></i>" +
							"	</button>" +
							"</td>";
					
						});

						container.append(html);

						var addWageRow = getAddWageRow();

						container.append("<tr>" + addWageRow + "</tr>");

						$('#wagesModal').modal('show');

						$('.addWage').click(function()
						{
							var tr = $(this).parents('tr').filter(':first');

							populateAddWageRow(tr, employeeId);
						});

						$('.saveWage').click(function()
						{
							var tr = $(this).parents('tr').filter(':first');

							if (!validateWage(tr))
								return false;

							var data = serializeWage(tr);
							log(data);

							var wageId = $(this).attr('rel');

							// Update wage on server
							updateWage(wageId, employeeId, data);

							return false;

						});

						$('.deleteWage').click(function()
						{
							// Confirm
							if (confirm("Deleting a wage will affect timecards which use this wage in an undesirable way. Are you sure you want to delete this wage?"))
							{
								var wageId = $(this).attr('rel');

								// Delete wage on server
								$.ajax({
									url: urlBase + "wages/deleteEmployeeWage/" + wageId,
									dataType: "json",
									type: "get",
									success: function(response)
									{
										alert(response['message']);

										loadWages($('#wagesModal .modal-body tbody'), employeeId);
									}
								});

							}

						});
					}
				
				}
			});

		}
		
		function loadProfile(container, employeeId)
		{
			// Remove save event handler
			container.find("#saveProfile").off();
			
			// Add datepickers
			container.find('#inputHired').datepicker({
				dateFormat: "yy-mm-dd",
				showOtherMonths: true,
				selectOtherMonths: true,
				changeMonth: true,
				changeYear: true
			});
			
			container.find('#inputTerminated').datepicker({
				dateFormat: "yy-mm-dd",
				showOtherMonths: true,
				selectOtherMonths: true,
				changeMonth: true,
				changeYear: true
			});
			
			// Get employee from loaded list of employees
			var employee = objectFromArray(loadedEmployees, "id", employeeId);
			var employeeName = employee["firstName"] + " " + employee["lastName"];
			
			// Change heading
			var heading = container.find('.modal-header h3');
			heading.html("Profile for " + employeeName);
			
			log(employee);
			
			// Populate the fields (email, phone, hired, terminated, notes)
			container.find("#inputEmail").val(employee.email);
			container.find("#inputPhone").val(employee.phone);
			container.find("#inputHired").val(employee.hired);
			container.find("#inputTerminated").val(employee.terminated);
			container.find("#inputNotes").val(employee.notes);
			
			// Show modal
			container.modal('show');
			
			// Attach save handler
			$("#saveProfile").click(function()
			{
				// If valid (alerts/focuses automatically)
				if (validateProfile(container))
				{
					var data = serializeProfile(container);
					
					// Call update service
					$.ajax({
						url: urlBase + "employees/updateProfile/" + employeeId,
						dataType: "json",
						type: "post",
						data: data,
						success: function(response)
						{
							// If successful
							if (response.success)
							{
								// Close modal
								container.modal("hide");
							}

							// Alert message
							alert(response.message);
							
							updateEmployeesTable();
						}
					});

				}
			});
		}
		
		function loadFiles(container, employeeId)
		{
			if (uploader !== undefined)
			{
				uploader.off();
				uploader.empty();
			}
			
			// Remove upload event handler
			container.find("#uploadFile").off();
			container.find("#uploadFile").attr("disabled", "disabled");
			container.find("#selectFile").off();
			
			// Empty table
			container.find("table .file").remove();
			
			// Get employee from loaded list of employees
			var employee = objectFromArray(loadedEmployees, "id", employeeId);
			var employeeName = employee["firstName"] + " " + employee["lastName"];
			
			// Change heading
			var heading = container.find('.modal-header h3');
			heading.html("Files for " + employeeName);
			
			// Populate list of files
			$.ajax({
				url: urlBase + "employees/files/" + employeeId,
				dataType: "json",
				type: "get",
				success: function(response)
				{
					// If unsuccessful
					if (!response.success)
					{
						// Alert message
						alert(response.message);
					}
					else
					{
						// For each file
						$.each(response.message, function(index, file)
						{
							// Add new row to table
							container.find("table").append(
								"<tr class='file'>" +
								"	<td>" +
								"		<a href='" + urlBase + "employees/file/" + file.value + "'>" +
								"			" + file.key +
								"		</a>" +
								"	</td>" +
								"	<td style='text-align:right'>" +
								"		<button class='btn btn-mini btn-danger deleteFile' rel='" + file.value + "'>" +
								"			<i class='icon-white icon-trash'></i>" +
								"		</button>" +
								"	</td>" +
								"</tr>");
						});
									
						// If no files
						if (response.message.length == 0)
						{
							// Add a no files row
							container.find("table").prepend("<tr class='file'><td>No files exist for this employee</td></tr>")
						}
						
						container.find(".deleteFile").click(function()
						{
							var filename = $(this).attr('rel');
							
							$.ajax({
								url: urlBase + "employees/deleteFile/" + filename,
								dataType: "json",
								type: "get",
								success: function(deleteResponse)
								{
									alert(deleteResponse.message);
									
									if (deleteResponse.success)
									{
										loadFiles(container, employeeId);
									}
								}
							});
						});
										
					}
					
					// Show modal
					container.modal("show");
				}

			});
					
			// Activate select/upload buttons
			uploader = $('#fine-uploader').fineUploader({
				request: {
					endpoint: 'employees/uploadFile',
					paramsInBody: true
				},
				autoUpload: false,
				multiple: false,
				button: $("#selectFile")[0],
				dragAndDrop: {
					disableDefaultDropzone: true
				},
				classes: {
					success: 'alert-success',
					fail: 'alert-error'
				}
			});
			
			uploader.on("complete", function(event, id, name, response)
			{
				log("complete event fired");
				
				if (response.success)
				{
					loadFiles(container, employeeId);
				}
			});
			
			uploader.on("submit", function(event, id, name)
			{
				log("submit event fired");
				
				container.find("#uploadFile").removeAttr("disabled");
			});
 
			container.find('#uploadFile').click(function() {
				
				var inputTitle = container.find("#inputTitle");
				
				// Validate title
				if (inputTitle.val() == "")
				{
					alert("You must specify a title");
					inputTitle.focus();
				}
				else
				{
					uploader.fineUploader("setParams",
					{
						employeeId: employeeId,
						title: inputTitle.val()
					});
					
					uploader.fineUploader('uploadStoredFiles');
				}
			});
			
		}

		function validateWage(tr)
		{
			if (tr.find('#wageName').val() == "")
			{
				alert("Wage name must not be empty");
				tr.find('#wageName').focus();
				return false;
			}
			else if (tr.find('#wageAmount').val() == "")
			{
				alert("Wage amount must not be empty");
				tr.find('#wageAmount').focus();
				return false;
			}
			else
				return true;
		}

		function serializeWage(tr)
		{
			var name = tr.find('#wageName').val();
			var wage = tr.find('#wageAmount').val();
			
			log (name + " " + wage);

			var data =
			"name=" + name +
			"&wage=" + wage;

			return data;
		}

		function updateWage(wageId, employeeId, data)
		{
			$.ajax({
				url: urlBase + "wages/updateEmployeeWage/" + wageId,
				dataType: "json",
				type: "post",
				data: data,
				success: function(response)
				{
					alert(response['message']);

					loadWages(container, employeeId);
				}
			});
		}

		function getAddWageRow()
		{
			return "<td colspan='10'>" +
			"	<button class='btn btn-primary addWage'>" +
			"		<i class='icon-white icon-plus'></i> Add New Wage" +
			"	</button>" +
			"</td>";
		}

		function populateAddWageRow(tr, employeeId)
		{
			var row = "";

			row += "<td>" + getTextInput("wageName", "wageName", "") + "</td>\n";
			row += "<td>" + getTextInput("wageAmount", "wageAmount", "") + "</td>\n";
			row += "<td>";
			row += "	<button class='btn btn-mini btn-primary addWage'>";
			row += "		<i class='icon-white icon-ok'></i>";
			row += "	</button>";
			row += "</td>";
			row += "<td>";
			row += "	<button class='btn btn-mini btn-danger cancelAddWage'>";
			row += "		<i class='icon-white icon-remove'></i>";
			row += "	</button>";
			row += "</td>";

			tr.empty();
			tr.append(row);

			$('.addWage').click(function()
			{
				var tr = $(this).parents('tr').filter(':first');

				if (!validateWage(tr))
					return false;

				var data = serializeWage(tr);

				$.ajax({
					url: urlBase + "wages/addEmployeeWage/" + employeeId,
					dataType: "json",
					type: "post",
					data: data,
					success: function(response)
					{
						alert(response['message']);

						loadWages($('#wagesModal .modal-body tbody'), employeeId);
					}
				});

				return false;

			});

			$('.cancelAddWage').click(function()
			{
				var addWageTR = $(this).parents('tr').filter(':first');

				addWageTR.empty();
				addWageTR.append(getAddWageRow());

				$('.addWage').click(function()
				{
					var tr = $(this).parents('tr').filter(':first');

					populateAddWageRow(tr, employeeId);
				});
			});
		}
		
	});