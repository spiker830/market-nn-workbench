define(
		[
			'jquery-ui',
			'jquery.canvasjs',
			'backbone-util',
			'bootstrap',
			'util'
		],
		function ()
		{
			var url = urlBase + "resources/population_genomes";
			var header = "Genomes";
			
			if (typeof analyzers !== "undefined" && analyzers !== "") {
				url += "?analyzers=" + analyzers;
				header += " using " + analyzers;
			}
			
			if (typeof targetSymbol !== "undefined" && targetSymbol !== "") {
				
				header += " predicting " + targetSymbol;
				
				if (url.indexOf("?") !== -1) {
					url += "&target_symbol=" + targetSymbol;
				} else {
					url += "?target_symbol=" + targetSymbol;
				}
				
			}
			
			if (typeof tradingStrategy !== "undefined" && tradingStrategy !== "") {
				
				header += " using " + tradingStrategy + " strategy";
				
				if (url.indexOf("?") !== -1) {
					url += "&trading_strategy=" + tradingStrategy;
				} else {
					url += "?trading_strategy=" + tradingStrategy;
				}
				
			}
			
			$('#genome-header').html(header);
			
			var genomes = new Backbone.Collection(null, {
				url: url
			});

			var genomeView = new ListView({
				collection: genomes,
				enableEditing: false,
				fields: [
					{
						displayName: "ID",
						dataId: "id"
					},
//					{
//						displayName: "Training Range",
//						dataId: "training_range",
//						value: function (model) {
//							return model.get("training_start_date") + " - " +
//									model.get("training_end_date");
//						}
//					},
//					{
//						displayName: "Days",
//						dataId: "training_length"
//					},
//					{
//						displayName: "Backtesting Range",
//						dataId: "backtesting_range",
//						value: function (model) {
//							return model.get("backtesting_start_date") + " - " +
//									model.get("backtesting_end_date");
//						}
//					},
//					{
//						displayName: "Days",
//						dataId: "backtesting_length"
//					},
//					{
//						displayName: "Target in Input?",
//						dataId: "target_in_input"
//					},
//					{
//						displayName: "Trading Strategy",
//						dataId: "short_name"
//					},
//					{
//						displayName: "Gain",
//						dataId: "gain",
//						value: function (model) {
//							return ((model.get("gain") - 1) * 100).toFixed(2);
//						}
//					},
//					{
//						displayName: "Base",
//						dataId: "base_gain",
//						value: function (model) {
//							return ((model.get("base_gain") - 1) * 100).toFixed(2);
//						}
//					},
//					{
//						displayName: "Improvement",
//						dataId: "improvement",
//						value: function (model) {
//							return (model.get("improvement") * 100).toFixed(2);
//						}
//					},
					{
						displayName: "Graphs",
						dataId: "graphs",
						template: _.template($("#genome-graphs-template").html())
					}
				]
			});

			$("#genome-view").append(genomeView.render().el);
			
//			function renderField(field, name, type, color, axisYType) {
//
//				var seriesData = {
//					type: type,
//					showInLegend: true,
//					legendText: name,
//					toolTipContent: name + ": {y}<br />" +
//							"Improvement: {x}",
//					axisYType: axisYType,
//					dataPoints: []
//				};
//
//				if (typeof color !== "undefined") {
//					seriesData.color = color;
//				}
//
//				genomes.each(function (model) {
//					
//					var x = model.get("improvement");
//					if (x === 0 || model.get("gain") === 1)
//						return;
//
//					var y = model.get(field);
//					
//					if (looksLikeDate(y)) {
//						y = (new Date(y)).getTime() / 1000000000;
//					}
//					
//					seriesData.dataPoints.push({
//						x: x,
//						y: y
//					});
//				});
//				
//				options.data.push(seriesData);
//
//				$("#job-configuration-graph").CanvasJSChart(options);
//			};
//
//			function renderFields(fields) {
//
//				var yField = fields[0].dataId;
//				var zField = fields[1].dataId;
//
//				options.axisX.title = "Improvement";
//				options.axisY.title = fields[0].displayName;
//
//				var seriesData = {
//					type: "bubble",
//					toolTipContent: fields[0].displayName + ": {y}<br />" +
//							fields[1].displayName + ": {z}<br />" +
//							"Improvement: {x}",
//					axisYType: "primary",
//					dataPoints: []
//				};
//
//				if (typeof color !== "undefined") {
//					seriesData.color = color;
//				}
//
//				genomes.each(function (model) {
//					
//					var y = model.get(yField);
//					var z = model.get(zField);
//
//					if (looksLikeDate(y)) {
//						y = (new Date(y)).getTime() / 1000000000;
//					}
//					if (looksLikeDate(z)) {
//						z = (new Date(z)).getTime() / 1000000000;
//					}
//
//					if (model.get("improvement") === 0 || model.get("gain") === 1)
//						return;
//
//					seriesData.dataPoints.push({
//						x: model.get("improvement"),
//						y: y,
//						z: z
//					});
//				});
//
//				options.data.push(seriesData);
//
//				$("#job-configuration-graph").CanvasJSChart(options);
//			};
//
//			function looksLikeDate(value) {
//				return (typeof value === "string" && value.length === 10);
//			}
//			
//			var MultiSelectButtonView = Backbone.View.extend({
//				className: "btn-group",
//				initialize: function() {
//					this.selected = [];
//				},
//				attributes: {
//					"data-toggle": "buttons-checkbox",
//					"style": "float:left"
//				},
//				buttonTemplate: _.template($("#multi-select-button-template").html()),
//				events: {
//					"click button": "clicked"
//				},
//				clicked: function (event) {
//					
//					// Have to do this to make sure class is available when
//					// a click handler checks for selected (can't use .active)
//					this.$(event.target).toggleClass("selected");
//					this.trigger("click");
//				},
//				render: function () {
//
//					// For each input, render the appropriate view
//					_.each(this.options.buttons, this.renderButton, this);
//					
//					return this;
//				},
//				getSelected: function () {
//
//					var selectedButtons = this.$(".selected");
//					var selected = [];
//
//					_.each(selectedButtons, function (selectedButton) {
//						selected.push({
//							dataId: $(selectedButton).attr("data-id"),
//							displayName: $(selectedButton).attr("data-display-name")
//						});
//					});
//
//					return selected;
//
//				},
//				renderButton: function (buttonConfig) {
//
//					this.$el.append(this.buttonTemplate(buttonConfig));
//
//				}
//			});
//
//			var dataToggles = new MultiSelectButtonView({
//				buttons: [
//					{
//						dataId: "prediction_window",
//						displayName: "Prediction Window"
//					},
//					{
//						dataId: "evaluation_window",
//						displayName: "Evaluation Window"
//					},
////					{
////						dataId: "training_length",
////						displayName: "Training Length"
////					},
////					{
////						dataId: "backtesting_length",
////						displayName: "Backtesting Length"
////					},
//					{
//						dataId: "bear_percent",
//						displayName: "Bear %"
//					},
//					{
//						dataId: "bull_percent",
//						displayName: "Bull %"
//					},
//					{
//						dataId: "training_size",
//						displayName: "Training Size"
//					},
//					{
//						dataId: "backtesting_start_date",
//						displayName: "Backtesting Start Date"
//					},
//					{
//						dataId: "training_start_date",
//						displayName: "Training Start Date"
//					},
//					{
//						dataId: "hidden_neurons_layer_1",
//						displayName: "Layer 1"
//					}
//				]
//			});

//			$("#data-toggles").append(dataToggles.render().el);
//
//			var options = {
//				zoomEnabled: true,
//				data: [],
//				axisX: {
//					labelAngle: -50,
//					gridThickness: 2,
//					interval: .1
//				},
//				axisY: {
//					includeZero: false,
//					gridThickness: 0
//				},
//				legend: {
//					fontSize: 15
//				}
//			};

//			$("#data-toggles").on("click", function (event) {
//
//				options.data = [];
//				options.axisX.title = "";
//				options.axisY.title = "";
//				var selected = dataToggles.getSelected();
//				
//				// If one, just plot the series as a scatter plot
//				if (selected.length === 1) {
//
//					dataId = selected[0].dataId;
//					displayName = selected[0].displayName;
//
//					renderField(dataId, displayName, "scatter", "#97b3fc", "primary");
//					renderTimelineField(dataId, displayName, 'scatter');
//
//				} else if (selected.length === 2) {
//
//					renderFields(selected);
//
//				}
//			});
//				
//			var timelineOptions = {
//				zoomEnabled: true,
//				data: [],
//				legend: {
//					fontSize: 15
//				}
//			};
//					
//			var tickerSeries = {
//				type: 'line',
//				showInLegend: true,
//				legendText: 'Adjusted Close',
//				axisYType: "secondary",
//				dataPoints: []
//			};
//			
//			function renderTimelineField(field, name, type) {
//				
//				timelineOptions.data = [];
//				
//				var bullWinningSeries = {
//					type: type,
//					showInLegend: true,
//					legendText: 'Bull Winning',
//					color: 'green',
//					dataPoints: []
//				};
//				
//				var allWinningSeries = {
//					type: type,
//					showInLegend: true,
//					legendText: 'All Winning',
//					color: 'orange',
//					dataPoints: []
//				};
//				
//				var losingSeries = {
//					type: type,
//					showInLegend: true,
//					legendText: 'Losing',
//					color: 'red',
//					dataPoints: []
//				};
//				
//				genomes.each(function (model) {
//					
//					var x = new Date(model.get("backtesting_start_date"));
//					
//					if (model.get("gain") === 1)
//						return;
//
//					var y = model.get(field);
//					
//					if (looksLikeDate(y)) {
//						y = (new Date(y)).getTime() / 1000000000;
//					}
//					
//					if (model.get("improvement") > 0) {
//						
//						if (model.get("base_gain") > 1) {
//							
//							bullWinningSeries.dataPoints.push({
//								x: x,
//								y: y
//							});
//							
//						} else {
//							
//							allWinningSeries.dataPoints.push({
//								x: x,
//								y: y
//							});
//						}
//						
//					} else {
//						losingSeries.dataPoints.push({
//							x: x,
//							y: y
//						});
//					}
//				});
//				
//				timelineOptions.data.push(allWinningSeries);
//				timelineOptions.data.push(bullWinningSeries);
//				timelineOptions.data.push(losingSeries);
//				timelineOptions.data.push(tickerSeries);
//				
//				$("#job-timeline-graph").CanvasJSChart(timelineOptions);
//			};
			
//			var tickerData = new Backbone.Collection(null, {
//				url: urlBase + "resources/ticker_data?symbol=" + targetSymbol
//			});
//			
//			tickerData.fetch({
//				success: function(collection) {
//					
//					tickerSeries.dataPoints = [];
//
//					collection.each(function (model) {
//
//						var x = new Date(model.get("date"));
//						var y = model.get("adjusted_close");
//
//						tickerSeries.dataPoints.push({
//							x: x,
//							y: y
//						});
//					});
//					
//					log(timelineOptions);
//					
//					timelineOptions.data.push(tickerSeries);
//					
//					$("#job-timeline-graph").CanvasJSChart(timelineOptions);
//				}
//			});
		});