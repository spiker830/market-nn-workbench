config = {};

if(typeof console === "undefined")
{
    console = {
        log: function() { }
    };
}

// Activate menus
$(function() {

	$("ul.nav li").hover(addMega, removeMega);

});

function addMega(){
	$(this).addClass("hovering");
}

function removeMega(){
	$(this).removeClass("hovering");
}

// Get global configuration
$.ajax({
	url: urlBase + "resources/configurations/frontend",
	dataType: "json",
	async: false, // Must be synchronous to make sure all configuration is loaded
	success: function(configObject)
	{	
		config = configObject['config'];

		log(config);
	}
});

/**
 * This method logs a message to the console if it exists and the system is
 * in debug.
 */
function log(object)
{
	if (config != null && config.DEBUG == 1)
		console.log(object);
}

/**
 * Recursively merge sort an array of objects using the supplied comparison
 * function.
 */
function merge_sort(array,comparison)
{
	if(array.length < 2)
		return array;

	var middle = Math.ceil(array.length/2);

	return merge(merge_sort(array.slice(0,middle),comparison),
		merge_sort(array.slice(middle),comparison),
		comparison);
}

/**
 * Merge two arrays in order based on comparison function.
 */
function merge(left,right,comparison)
{
	var result = new Array();
	
	// While there are still elements in both arrays
	while((left.length > 0) && (right.length > 0))
	{
		// If first element of left is "less than or equal" to first element of right
		if(comparison(left[0],right[0]) <= 0)
		{
			// Add element from left
			result.push(left.shift());
		}
		else
		{	
			// Add element from right
			result.push(right.shift());
		}
	}
	
	// Add any straggler from left array
	while(left.length > 0)
		result.push(left.shift());
	
	// Add any straggler from right array
	while(right.length > 0)
		result.push(right.shift());

	return result;
}

/**
 * Sort an array of objects alphabetically by a given property.
 */
function sortObjectsByPropertyName(array, property)
{
	return merge_sort(array, function(left, right)
	{
		if (isNaN(left[property]-right[property]))
			return left[property].toString().localeCompare(right[property].toString());
		else
			return left[property]-right[property];
	});
}

/**
 * Return an array of arrays which are groups of objects with the same specified
 * property.
 */
function splitObjectsByProperty(array, property)
{
	// First sort the array by the specified property
	array = sortObjectsByPropertyName(array, property);

	var splitArray = new Array();

	var tempArray = new Array();
	var propertyValue = "";

	// For each object
	$.each(array, function(index, object)
	{
		// If we see a new property value
		if (object[property] != propertyValue)
		{
			// Add tempArray to the splitArray (only if we've added to the tempArray)
			if (tempArray.length > 0)
			{
				splitArray.push(tempArray);
				tempArray = new Array();
			}

			propertyValue = object[property];
		}

		tempArray.push(object);
	});

	// Add the last set of objects if non-empty
	if (tempArray.length > 0)
		splitArray.push(tempArray);

	return splitArray;
}

/**
 * Return an array of objects who's specified property matches the specified
 * value.
 */
function filterObjectsByProperty(array, property, value)
{
	var filteredObjects = new Array();

	$.each(array, function(index, object)
	{
		if (object[property] == value)
			filteredObjects.push(object);
	});

	return filteredObjects;
}

/**
 * Return an array of objects who's specified property contains the specified
 * value.
 */
function searchObjectsByProperty(array, property, value)
{
	var filteredObjects = new Array();
	var pattern = new RegExp(".*" + value + ".*", "i");
	
	$.each(array, function(index, object)
	{
		if (pattern.test(object[property]))
			filteredObjects.push(object);
	});

	return filteredObjects;
}

/**
 * Return the sum of the specified property in the given array of objects.
 */
function sumByProperty(array, property)
{
	var sum = 0;

	$.each(array, function(index, object)
	{
		sum += parseFloat(object[property]);
	});

	return sum;
}

/**
 * Capitalize the first letter of a string
 */
function capFirstLetter(str)
{
	var title = str;
	
	if (typeof str === "string" && str != null) {
		
		title = str.charAt(0).toUpperCase() + str.slice(1);
	}
	
	return title;
}

/**
 * Using the loaded configuration, determine if the current user's level is
 * authorized for the given action.
 */
function authorized(actionString)
{
	var authorized = false;
	
	// If configuration exists
	if (config != null && typeof config != "undefined")
	{
		// Get current user level and variable name
		var level = config.levelName;
		var action = actionString.toUpperCase().replace(/ /g, "_");
		
		// Check if user is authorized to perform action
		if (config[action].indexOf(level) != -1)
		{
			authorized = true;
		}
		
	}
	
	return authorized;
}

function alertModal(message)
{
// Add modal to DOM
	
// Show modal
	
}

function confirmModal(message)
{
// Add modal to DOM
	
// Show modal
	
// Set handlers for OK/Cancel
	
}

/**
 * Format a number as a currency string (e.g. 1024 -> $1,024.00).
 */
function formatCurrency(num, includeSymbol)
{
	if (includeSymbol == undefined) includeSymbol = true;
	
	if (num == null) num = 0;
	
	// Get a plain number string with no dollar signs or commas
	num = num.toString().replace(/\$|\,/g, '');
	
	// Default to zero
	if (isNaN(num)) num = "0";
	
	// Is positive?
	positive = (num == (num = Math.abs(num)));
	
	// Shift number to the left three decimal places and round
	num = Math.floor(num * 100 + 0.50000000001);
	
	// Get cents from shifted number
	cents = num % 100;
	
	// Shift back and get string
	num = Math.floor(num / 100).toString();
	
	// Get string friendly version of cents
	if (cents < 100)
	{
//		cents = "0" + cents;
		
		if (cents < 10)
		{
			cents = "0" + cents;
		}
	}
	
	// Add commas
	for (var i = 0; i < Math.floor((num.length - (1 + i)) / 3); i++)
	{
		num = num.substring(0, num.length - (4 * i + 3)) + ',' + num.substring(num.length - (4 * i + 3));
	}
	
	// Return with final formatting
	return (((positive) ? '' : '-') + (includeSymbol ? "$" : "") + num + '.' + cents);
}

function formatRatio(num) {
	
	var ratio;
	
	if (typeof num === "number" || !isNaN(parseFloat(num))) {
		
		ratio = (parseFloat(num) * 100).toFixed(1) + " %";
		
	} else {
		
		ratio = num;
		
	}
	
	return ratio;
}

function multiplyCurrency( val1, val2 ) {
	
	return ( val1 * 1000 * val2 * 1000 ) / 1000000;
	
}

function s(str, length)
{
	return str != null && str.length > length ? str.substring(0,length) + "..." : str;
}

/**
 * Give a unique id, a name, a values object and a pre-selected value.
 *
 * Values object: { "name":"value", "name2":"value2" }
 *
 * Get the html for a select element describing the values object.
 */
function getSelect(id, name, values, selected)
{
	var select = "<select id='" + id + "' name='" + name + "'>";
	
//	if (typeof selected === "undefined" || selected === null) {
//		select += "<option>Select an Item</option>";
//	}

	for (var property in values)
	{
		if (values[property] == selected)
			select += "<option selected='selected' value='" + values[property] + "'>" + property + "</option>";
		else
			select += "<option value='" + values[property] + "'>" + property + "</option>";

	}

	select += "</select>";

	return select;
}

/**
 * Give a date string.
 *
 * Get true if this is a valid date, false otherwise.
 */
function validDate(date)
{
	valid = false;

	if (date != "")
		valid = true

	return valid;
}

function objectFromArray(array, property, value)
{
	var target = null;
	
	$.each(array, function(index, object)
	{
		if (object[property] == value)
		{
			target = object;
			
			return false;
		}
		else
		{
			return true;
		}
	})
	
	return target;
}

function getDateString(date)
{
	var year = date.getFullYear();
	var month = date.getMonth()+1;
	var day = date.getDate();
	
	if (month < 10)
	{
		month = "0" + month;
	}
	
	if (day < 10)
	{
		day = "0" + day;
	}
	
	return dateStr = year + "-" + month + "-" + day;

}

function intersection(array1, array2)
{
	var intersection = [];
	
	for (var i = 0; i < array1.length; i++)
	{
		if (array2.indexOf(array1[i]) != -1)
		{
			intersection.push(array1[i]);
		}
	}
	
	return intersection;
}

function union(array1, array2)
{
	var union = array1;
	
	for (var i = 0; i < array2.length; i++)
	{
		if (union.indexOf(array2[i]) == -1)
		{
			union.push(array2[i]);
		}
	}
	
	return union;
}