var ModalView = Backbone.View.extend(
{
	tag: "div",
	
	className: "modal hide",
	
	template: _.template($("#modal-template").html()),
				
	initialize: function(options) {
		
		// Handle options
		this.handleOptions(options);
		
		// Append rendered modal to body
		$("body").append(this.render().el);
		
		// Attach events
		this.attachEvents();
		
		// Show the modal
		this.$el.modal("show");
		
	},
	
	handleOptions: function(options) {
		
		this.buttons = options.buttons;
		this.heading = options.heading;
		this.subheading = options.subheading;
		this.body = options.body;
		
	},
	
	attachEvents: function() {
		
		var that = this;
		
		// Clean up the modal if hidden
		this.$el.on("hidden", function() {
			that.done();
		});
		
	},
	
	render: function() {

		this.$el.html(this.template());
		this.buttonsContainer = this.$(".modal-footer .buttons");
		
		// Update the header
		this.$(".header-heading").html(this.heading);
		this.$(".header-subheading").html(this.subheading);
		
		// Update the contents
		this.$(".modal-body").html(this.body.render().el);
		
		// Setup any additional buttons		
		this.buttonsContainer.empty();
		
		_.each(this.buttons, function(buttonConfig) {
			
			var button = $("<a href=\"#\" class=\"btn btn-primary\">" + buttonConfig.label + "</a>");
			
			this.buttonsContainer.append(button);
			
			button.on("click", buttonConfig.handler);
			
		}, this);
		
		return this;
	},
	
	done: function() {
		
		this.remove();
		
	}
});

var AttachmentsModalView = ModalView.extend({
	
	initialize: function(options) {
		
		var that = this;
		
		// Handle options
		this.handleOptions(options);
		
		this.attachmentsURL = options.url;
		this.attachmentModalSubHeadingDataId = options.attachmentModalSubHeadingDataId;
		this.onClose = options.onClose;
		
		// Add the custom attachments body
		this.initBody();
		
		if (this.buttons == undefined) this.buttons = [];
		
		this.buttons.push({
			label: "New Attachment",
			handler: function() {
				that.body.newItem(); // The body is a custom list view
			}
		});
		
		// Append rendered modal to body
		$("body").append(this.render().el);
		
		// Attach events
		this.attachEvents();
		
		// Show the modal
		this.$el.modal("show");
		
	},
	
	attachEvents: function() {
		
		var that = this;
		
		// Clean up the modal if hidden
		this.$el.on("hidden", function() {
			
			that.done();
			
			that.onClose();
			
		});
		
	},
	
	initBody: function() {
		
		var that = this;
		
		// Create custom list view
		var AttachmentsListView = ListView.extend({
	
			events: {
				"click #uploadFile": "upload",
				"click .cancel": "cancel"
			},
			
			newAttachmentRowTemplate: _.template($("#new-attachment-row-template").html()),
			
			newItem: function()
			{
				// Append the new attachment row
				this.newRow = $(this.newAttachmentRowTemplate());
				this.$el.append(this.newRow);
				
				// Activate select/upload buttons
				this.uploader = $('#fine-uploader').fineUploader({
					request: {
						endpoint: that.attachmentsURL,
						paramsInBody: true
					},
					autoUpload: false,
					multiple: false,
					button: $("#selectFile")[0],
					dragAndDrop: {
						disableDefaultDropzone: true
					},
					classes: {
						success: 'alert-success',
						fail: 'alert-error'
					}
				});
			},

			upload: function() {
				
				var that = this;

				var inputTitle = this.$("#inputTitle");

				// Validate title
				if (inputTitle.val() == "")
				{
					alert("You must specify a title");
					inputTitle.focus();
				}
				else
				{
					this.uploader.fineUploader("setParams",
					{
						title: inputTitle.val()
					});

					this.uploader.fineUploader('uploadStoredFiles');
				}
				
				this.uploader.on("complete", function(event, id, name, response)
				{
					if (response.success)
					{
						that.$("tr.new-attachment-row").remove();
						
						that.repopulate();
					}
				});
			},
			
			cancel: function() {
				
				this.newRow.remove();
				
				this.repopulate();
				
			}
			
		});
		
		// Set it as the body
		this.body = new AttachmentsListView({
			fields: [
			{
				displayName: "Title",
				dataId: "title",
				defaultValue: "title",
				template: _.template($("#list-item-field-file-title-template").html())
			}
			],
			url: that.attachmentsURL
		});
		
	//		log(this.body);
		
	}
	
});
		
var ListView = Backbone.View.extend(
{
	tagName: "table",
	
	className: "table",
	
	events:
	{
		"click .add": "newItem"
	},
	
	initialize: function(options)
	{
		this.listItemView = options.listItemView == undefined ? ListItemView : options.listItemView;
		this.fields = options.fields == undefined ? [] : options.fields;
		this.addButtonEnabled = options.addButton != undefined;
		// Default editing to true
		this.enableEditing = options.enableEditing == undefined ? true : options.enableEditing;
		
		if (this.addButtonEnabled) {
			this.addButtonLabel = options.addButton.label;
		}

		this.$el.append("<thead></thead><tbody></tbody>");
		
		if (options.collection == undefined) {
			log("creating collection from url and model");
			this.collection = new Backbone.Collection(
				null,
				{
					model: options.model,
					url: options.url
				}
			);
		} else {
			this.collection = options.collection;
		}
		
		log(this.collection);

		// renders the list when the collection is reset/synced/whatever
		//		this.listenTo(this.collection, "saved", this.repopulate);
		//		this.listenTo(this.collection, "destroy", this.renderItems);
//				this.listenTo(this.collection, "sync", this.renderItems);
				this.listenTo(this.collection, "all", this.printEvent);
		this.listenTo(this.collection, "add", this.add);
		
		this.resetListView();
		this.repopulate();
	},
	
	filter: function() {
		
		return this.collection.models;
		
	},
	
		printEvent: function(event)
		{
			log("event: " + event);
		},
	
	render: function()
	{
		// Re-render list
//		this.resetListView();
		this.renderItems();
		
		return this;
	},
	
	repopulate: function()
	{
//		log("repopulating!");
		
		// Refetch the items
		var that = this;
		
		this.collection.fetch({
			success: function(collection) {
				
				// Remove loading icon
				this.$("#loading-icon-row").remove();
				
				// Render empty list view if collection is empty
				if (collection.length === 0) {
					that.renderEmptyListView();
				}
			},
			error: function()
			{
				// Remove loading icon
				this.$("#loading-icon-row").remove();
				
				// Render an empty table
				log("error on fetch");
				that.collection.reset();
				that.renderEmptyListView();
			}
		});
		
		// Add loading icon
		this.renderLoadingIcon();
		
		if (this.addButtonEnabled) {
			
			// TODO not working on first render?
			this.renderAddButton();
		
		}
	},
	
	renderLoadingIcon: function() {
		
		this.$el.append("<tr id=\"loading-icon-row\">\n\
			<td\n\
				style=\"text-align:center\"\n\
				colspan=\"" + (this.options.fields.length+2) + "\">\n\
				<img src=\"" + urlBase + "app/img/spin.gif\" />\n\
			</td></tr>");
		
	},
	
	add: function(item)
	{
//		log("rendering single item");
		
		// Make sure there is no empty list row
		this.$("#empty-list-row").remove();
		
//		log("creating new view");
		
		// Create new view and render it
		var view = this.newView(item);

		this.$("tbody").append(view.render().el);

		if (item.isNew())
		{
			view.edit();
		}
	},
	
	renderItems: function()
	{
//		log("rendering all items");
		var models = this.filter();
		
		if (models.length < 0) {
		
			this.resetListView();
			
			// Add view for each model
			_.each(models, function(model)
			{
				log("creating new view");
				
				var view = this.newView(model);

				this.$("tbody").append(view.render().el);

			}, this);
			
		}
	},
	
	renderColumnHeaders: function() {
		
		var data = {};
		data.fields = this.fields;
		
		var html = "<tr>";
		html += _.template("<% _.each(fields, function(field) { %> <th><%- field.displayName %></th> <% }); %>", data);
		html += this.attachments ? "<th></th>" : "";
		html += this.enableEditing ? "<th></th><th></th>" : "";
		html += "</tr>";
		
		return html;
	},
	
	resetListView: function() {
		
		this.$("tbody").empty();
		
		this.$("thead").html(this.renderColumnHeaders());
		
	},
	
	renderEmptyListView: function() {
		
		this.resetListView();
		
		this.$el.append("<tr id=\"empty-list-row\"><td colspan=\"" + (this.options.fields.length+2) + "\">No items to display</td></tr>");
		
	},
	
	newView: function(item) {
		
		return new this.listItemView({
			model: item,
			fields: this.fields,
			attachments: this.attachments,
			attachmentModalSubHeadingDataId: this.attachmentModalSubHeadingDataId,
			enableEditing: this.enableEditing,
			editOnly: this.options.editOnly,
			deleteOnly: this.options.deleteOnly
		});
		
	},
	
	newItem: function()
	{
		// Create new model hash using default values
		var newModelHash = {};
		
		_.each(this.fields, function(field) {
			newModelHash[field.dataId] = field.defaultValue;
		});
		
		this.collection.add(new Backbone.Model(newModelHash));
	},
	
	renderAddButton: function() {
		
		var that = this;
		
		this.addButton = $("<button class=\"btn btn-primary\">" + this.addButtonLabel + "</button>");
		
		this.$el.append(this.addButton);
		
		this.addButton.on("click", function() {
			
			that.newItem();
			
		});
	}
});
	
var ListItemView = Backbone.View.extend(
{
	tagName: "tr",
	
	className: "list-item-view",
	
	events:
	{
		"click .edit": "edit",
		"click .cancel-edit": "cancel",
		"click .destroy": "destroy",
		"click .save": "save",
		"click .manage-attachments": "manageAttachments"
	},
	
	initialize: function(options)
	{
		this.fields = options.fields;
		this.enableEditing = options.enableEditing;
		
		// Renders when model is changed
		this.listenTo(this.model, "change", this.render);
		this.listenTo(this.model, "invalid", this.invalid);
	},
	
	adminButtonsTemplate: _.template($("#list-item-admin-buttons-template").html()),
	
	attachmentsTemplate: _.template($("#list-item-attachments-button-template").html()),
	
	render: function()
	{
		var that = this;
		
		this.$el.empty();
		
		_.each(this.fields, function(field) {
			
			var value = this.model.get(field.dataId);
			
			// Check for a value function
			if (field.value != undefined) {
				value = field.value(this.model);
			}
			
			// Check for an attachment type
			switch (field.type) {
				
				case "attachments":

					if (this.model.isNew()) {

						// Don't render attachments button if model isn't saved
						value = "";

						field.template = undefined;
						field.locked = true;

					} else {

						this.attachmentModalSubHeadingDataId = field.modalSubHeadingDataId;
						this.onClose = field.onClose;

						field.template = this.attachmentsTemplate;
						field.locked = false;

					}

					break;
					
			}
			
			// TODO was this used for something?
			//			var url = this.model.isNew() ? undefined : this.model.url();

			var customData = {
//				id: this.model.id,
				value: value,
				displayName: field.displayName,
				displayDataId: field.displayDataId,
				dataId: field.dataId,
				locked: field.locked,
				url: field.url,
				attachmentURL: this.model.url(),
				numAttachments: this.model.get("numAttachments"),
				type: field.type,
				defaultValue: this.model.get(field.dataId),
				alwaysEnabled: field.alwaysEnabled
			};
			
			for (var attribute in this.model.attributes) {
				customData[attribute] = this.model.attributes[attribute];
			}
			
			var fieldView = new ListItemFieldView({
				data: customData,
				template: field.template
			});
			
			// Listen for field view to say this view's model needs to be saved
			this.listenTo(fieldView, "save", function(newValue) {
				
				//				log("going to save field: " + field.dataId);
				
				var updateHash = {};
				updateHash[field.dataId] = newValue;
				
				this.model.save(updateHash,
				{
					wait: true,
					error: function(model, xhr, options)
					{
						alert(xhr.responseText);

						that.render();
					}
					
				});
				
			});
			
			this.$el.append(fieldView.render().el);
			
		}, this);
		
		// Render admin buttons if necessary
		if (this.enableEditing) {
//			log(this.$el);
			this.$el.append(this.adminButtonsTemplate());
			
			// Disable any buttons which shouldn't be used
			if (this.options.deleteOnly) {
				this.$(".edit-save-buttons").hide();
			} else if (this.options.editOnly) {
				this.$(".destroy").hide();
			}
		}

		// Hide edit mode elements
		this.$("input").hide();
		this.$(".save").hide();
		this.$(".cancel-edit").hide();
		
		return this;
	},
	
	edit: function()
	{
//		log("clicked edit button");
		
		// Hide text
		this.$("span").hide();

		// Show inputs
		this.$("input").show();

		// Hide edit and delete buttons
		this.$(".edit").hide();
		this.$(".destroy").hide();

		// Show save and cancel buttons
		this.$(".save").show();
		this.$(".cancel-edit").show();
	},
	
	destroy: function()
	{
//		log("destroying");
		
		var that = this;
		
		if (confirm("Are you sure you want to delete this item?"))
			this.model.destroy({
				wait: true,
				success: function(model, response, options) {
//					log(this);
					that.remove();
				},
				error: function(model, xhr, options) {
					alert(xhr.responseText);
					that.remove();
				}
			});
	},
	
	invalid: function(model, error) {

		alert(error);

	},
	
	save: function()
	{
		//		log("saving");
		
		var updateHash = {};
		
		// For each field, set the appropriate attribute on the model
		_.each(this.fields, function(field) {
			updateHash[field.dataId] = this.$("input." + field.dataId + "-value").val();
		}, this);
		
		//		log(updateHash);
			
		var that = this;

		// Save model
		this.model.save(updateHash, {
			wait: true,
			error: function(model, xhr, options)
			{
				alert(xhr.responseText);
				
				// Remove the model from its collection
				that.cancel();
			}
		});
	},
	
	cancel: function()
	{
//		log(this.model);
		
		// Delete model if this was a cancelled creation
		if (this.model.isNew()) {
			this.model.destroy();
			this.remove();
		} else {
			this.render();
		}
	},
	
	manageAttachments: function() {
		
//		log("manage clicked");
		
		// Trigger modal
		var attachmentsModal = new AttachmentsModalView({
			
			url: this.model.url() + "/attachments",
			heading: "Attachments",
			subheading: this.model.get(this.attachmentModalSubHeadingDataId),
			onClose: this.onClose
			
		});
		
	}
});

var ListItemFieldView = Backbone.View.extend({
	
	tagName: "td",
	
	template: _.template($("#list-item-field-template").html()),
	
	initialize: function(options) {
		
		this.data = options.data;
		
		if (options.template != undefined) {
			this.template = options.template;
		}
		
	},
	
	render: function() {
		
		var that = this;
		
		var type = this.data.type;
		
		this.$el.empty();
		
		// If select type, render a select instead
		if (type === "select") {
			
			var config = {
				url: this.data.url,
				//				label: this.data.displayName,
				displayDataId: this.data.displayDataId,
				valueDataId: "id",
				defaultValue: this.data.defaultValue
			};
			
			if (this.data.alwaysEnabled) {
				
				config.onChange = function(select) {
					that.trigger("save", select.getValue());
				}
			}
			
			var select = new Select(config);
			
			this.$el.html(select.render().el);
			
		} else {
		
			this.data.formattedValue = this.format(this.data.value, this.data.type);
			
			this.$el.html(this.template(this.data));
			
		}
		
		return this;
	},
	
	format: function(value, type) {
		
		switch (type) {
			
			case "currency":
				
				return formatCurrency(value);
				
			default:
				
				return value;
				
		}
	}
});

var Select = Backbone.View.extend({
	
	events: {
		
		"click .add": "newItem"
		
	},
	
	initialize: function(options) {
	
		this.displayDataId = options.displayDataId;
		this.valueDataId = options.valueDataId;
		this.defaultValue = options.defaultValue;
		this.addAbility = options.addAbility;
		this.name = options.name;
		this.label = options.label;
		//		this.selectId = options.selectId;
		this.disabled = options.disabled;
		this.onChange = options.onChange;
		
		var that = this;
		
		this.collection = new Backbone.Collection({},{
			url: options.url,
			comparator: $.isArray(this.displayDataId) ? this.displayDataId[0] : this.displayDataId
		});
		
		this.listenTo(this, "change select", this.onChange);
//		this.collection.on("add", this.add, this);
		
		this.collection.fetch({
			success: function()
			{
				if (options.filters == undefined) {
					that.populate();
					return;
				}
				
				// Only supporting one filter for now
				filter = options.filters[0];
				
				// If a lookup was specified, use it
				if (filter.lookupURL != undefined) {
					
					// Create collection to pull lookup items
					var collection = new Backbone.Collection({},{
						url: filter.lookupURL
					});
					
					collection.fetch({
						success: function() {
							
							// Get id of searched for lookup item
							var lookupWhere = {};
							lookupWhere[filter.lookupKey] = filter.lookupValue;
							
							var id = collection.findWhere(lookupWhere).id;
							
							var where = {};
							where[filter.attribute] = id;
							
							var filtered = that.collection.where(where);
							
							that.collection.reset(filtered);
							
							that.populate();
						}
					});
					
				} else { // No lookup was specified
					
					var where = {};
					where[filter.attribute] = filter.value;
					
					var filtered = that.collection.where(where);

					that.collection.reset(filtered);

					that.populate();
					
				}
			},
			error: function()
			{
				that.collection.reset();
				that.populate();
			}
		});

	},
	
	populate: function() {
		
		var that = this;
		
		this.$el.empty();
		
		var label = this.label == undefined ? "" : this.label + ": ";
		var selectId = this.cid + "-select";
		
		this.$el.append("<label for=\"" + selectId + "\" style=\"display:inline\">" + label);
		
		var disabledHTML = this.disabled ? "disabled=\"disabled\" " : "";
		
		this.$el.append("<select " + disabledHTML + "id=\"" + selectId + "\" class=\"input-large\"></select>");
		
		if (!this.collection.isEmpty()) {
			this.$("select").append("<option value=\"0\">Select</option>");
		}
		
		// TODO selects don't always select the value in the model
		
		// Create the options
		this.collection.each(function(item) {
			
			var display = "";
			var selected = false;
			
			// If an array was given for what to display
			if ($.isArray(this.displayDataId))
			{
				// Concatenate values together
				_.each(this.displayDataId, function(displayDataId) {
					
					var displayDataValue = item.get(displayDataId);
					
					// User friendly message instead of empty string
					var toDisplay = displayDataValue === "" ? "no " + displayDataId : displayDataValue;
					
					display += display == "" ? toDisplay : " | " + toDisplay;
					
				});
			}
			// If only displaying one data value
			else
			{
				display = item.get(this.displayDataId)
			}
			
			var value = item.get(this.valueDataId);
			var selectString = value === this.defaultValue ? " selected=\"selected\"" : "";
			
			this.$("select").append("<option value=\"" + value + "\"" + selectString + ">" + display + "</option>");
			
		}, this);
		
		this.$el.append("</select></label>");
		
		// Attach change handler if passed
		this.$("select").on("change", function() {
			if (that.onChange != undefined) {
				that.onChange(that);
			}
		});
		
		// If adding is enabled, render the add button
		if (this.addAbility != undefined && this.addAbility.field != undefined) {

			this.$el.append("<button class=\"btn add\" style=\"margin-bottom:9px\">New</button>");
			
		}
		
	},
	
	getValue: function() {
		
		return this.$("select").val();
		
	},
	
	newItem: function() {
		
		// Ask for value
		var value = prompt("Please enter the " + this.addAbility.field + " for the new " + this.addAbility.itemName + ".");
		
		// If the user enters a value
		if (value) {
			
			var newModelHash = {};
			newModelHash[this.addAbility.field] = value;
			
			// Add it as a new item
			this.collection.add(new Backbone.Model(newModelHash));
		}
		
	},
	
	add: function(item) {
		
		if (item.isNew()) {
			
			var that = this;

			item.save({}, {
				wait: true,
				success: function(model, response, options) {
					
					that.populate();
					
				},
				error: function(model, xhr, options)
				{
					that.collection.remove(item);
			
					that.populate();
				}
			});
			
		}
		
	}
});

var FormView = Backbone.View.extend({
	tagName: "form",
	className: "form-horizontal",
	textTemplate: _.template($("#form-input-text-template").html()),
	dateTemplate: _.template($("#form-input-date-template").html()),
	actionsTemplate: _.template($("#form-actions-template").html()),
	events: {
		"click .submit": "submit"
	},
	initialize: function () {

		this.listenTo(this, "click .submit", this.submit);

		this.reset();
	},
	render: function () {

		// For each input, render the appropriate view
		_.each(this.options.inputs, this.renderInput, this);

		// Render the actions
		this.$el.append(this.actionsTemplate());

		return this;
	},
	reset: function () {
		this.model = new Backbone.Model({}, {
			url: this.options.url
		});

		log(this.model);
	},
	submit: function (e) {

		var view = this;

		e.preventDefault();

		var modelAttributes = this.getModelAttributes();

		// Save the model
		this.model.save(modelAttributes, {
			success: function (model, response, options) {
				view.trigger("submitted");
			},
			error: function (model, response, options) {
				alert(response);
			}
		});

		this.reset();

	},
	getModelAttributes: function () {

		var attributes = {};

		// For each input, grab it's value from the form
		_.each(this.options.inputs, function (input) {

			// The html attributes use - instead of _
			var dataId = input.dataId.replace("-", "_");

			attributes[dataId] = this.$("#" + input.dataId).val();

		});

		return attributes;
	},
	renderInput: function (input) {

		switch (input.type) {

			case "date":
				
				this.$el.append(this.dateTemplate(input));
				
				this.$("#" + input.dataId).datepicker({
					dateFormat: "yy-mm-dd",
					showOtherMonths: true,
					selectOtherMonths: true,
					changeYear: true
				});

				break;

			default:

				this.$el.append(this.textTemplate(input));

		}

	},
	fillForm: function(model) {
		
		if (model == undefined) return;
		
		_.each(model.attributes, function(value, key) {

			// Get html attribute friendly dataId
			var dataId = key.replace("_", "-");

			// Set form field
			this.$("#" + dataId).val(value);
		});

	}
});