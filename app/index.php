<?php

/* ========================================================================== *
 *
 * Load all the necessary libraries and configuration, then run the bootstrap.
 *
 * 	@author: Jonathan Spiker
 *
 * ========================================================================== */

// Application level configuration parameters
date_default_timezone_set("America/New_York");
error_reporting(E_ALL & ~E_STRICT);

spl_autoload_register(function($class)
{
	$file = "lib/$class.php";
	
	if (file_exists($file))
		include_once($file);
	
});

spl_autoload_register(function($class)
{
	$file = "objects/$class.php";
	
	if (file_exists($file))
		include_once($file);
	
});

// Auto-require certain locations
requireDir("../config");

function requireDir($dirName)
{
	$files = glob($dirName . '/*.php');

	foreach ($files as $file) {
		include_once($file);
	}	
}

// Terminology map TODO put terminology map in database
define("TERMINOLOGY_MAP", serialize(array(
//	"resource name goes here" => "singular resource name goes here",
	"user_levels" => "user_level",
	"privileges" => "privilege",
	"configurations" => "configuration",
	"attachments" => "attachment",
	"employees" => "employee",
	"statuses" => "status",
	"jobs" => "job",
	"results" => "result",
	"data" => "data",
	"output" => "output",
	"analyzers" => "analyzer",
	"strategies" => "strategy",
	"transform_chains" => "transform_chain",
	"transforms" => "transform",
	"data_tables" => "data_table",
	"data_columns" => "data_column",
	"series" => "series",
	"population_genomes" => "population_genome",
	"population_gains" => "population_gain",
	"tickers" => "ticker"
)));

// Require mail if needed
if (Configuration::getValue("NOTIFICATIONS_ENABLED"))
{
	require 'Mail.php';
//	require 'Mail/mime.php';
}

// Start this shiz
Bootstrap::run();

?>
