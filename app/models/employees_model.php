<?php

/* ========================================================================== *
 *
 *
 *
 * 	@author: Jonathan Spiker
 *
 * ========================================================================== */

class Employees_Model extends Model
{

	function __construct()
	{
		parent::__construct();
	}

	function getList()
	{
		$deletedEmployeeStatus = $this->employeeStatuses["deleted"];

		$statement = $this->db->prepare("
			select employee.id, firstName, lastName, email, phone, canLogin, username, employee.status, employee.level, employee_level.level as levelName
			from employee
			left join employee_level on employee.level = employee_level.id
			where employee.status != $deletedEmployeeStatus
			");

		$statement->execute();

		$employees = $statement->fetchAll(PDO::FETCH_ASSOC);

		return $employees;
	}

	function getDetailedList()
	{
		$deletedEmployeeStatus = $this->employeeStatuses["deleted"];

		$statement = $this->db->prepare("
			select employee.id, firstName, lastName, email, phone, canLogin, username, employee.status, employee.level, hired, `terminated`, notes, employee_level.level as levelName
			from employee
			left join employee_level on employee.level = employee_level.id
			where employee.status != $deletedEmployeeStatus
			");

		$statement->execute();

		$employees = $statement->fetchAll(PDO::FETCH_ASSOC);

		return $employees;
	}

	function currentReports($employeeId)
	{
		$deletedEmployeeStatus = $this->employeeStatuses["deleted"];

		// Get collection of current reports
		$statement = $this->db->prepare("
			select employee.id, firstName, lastName
			from employee
			left join direct_report on employee.id = direct_report.directReportId
			where direct_report.employeeId = :employeeId
			and employee.status != $deletedEmployeeStatus
			");

		$statement->execute(array(
			":employeeId" => $employeeId
		));

		$employees = $statement->fetchAll(PDO::FETCH_ASSOC);

		return $employees;
	}

	function notCurrentReports($employeeId)
	{
		$notCurrentReports = array();

		// Get collection of current reports
		$currentReports = $this->currentReports($employeeId);

		$allEmployees = $this->getList();

		foreach ($allEmployees as $employee)
		{
			// If this employee is not a current report
			if (!$this->valueExistsInArray("id", $employee["id"], $currentReports))
			{
				$notCurrentReports[] = $employee;
			}
		}

		return $notCurrentReports;
	}

	function valueExistsInArray($searchKey, $searchValue, $array)
	{
		foreach ($array as $element)
		{
			if ($element[$searchKey] == $searchValue)
				return true;
		}

		return false;
	}

	function deleteReport($employeeId = 0, $reportId = 0)
	{
		$statement = $this->db->prepare("delete from direct_report
					where employeeId = :employeeId
					and directReportId = :directReportId");

		$success = $statement->execute(array(
			":employeeId" => $employeeId,
			":directReportId" => $reportId
				));

		return $success;
	}

	function addReport($employeeId = 0, $reportId = 0)
	{
		$statement = $this->db->prepare("insert into direct_report
					(employeeId, directReportId)
					values (:employeeId, :directReportId)");

		$success = $statement->execute(array(
			":employeeId" => $employeeId,
			":directReportId" => $reportId
				));

		return $success;
	}

	function getLevels()
	{
		$statement = $this->db->prepare("select id, level
			from employee_level");

		$statement->execute();

		$employeeLevels = array();

		while ($row = $statement->fetch(PDO::FETCH_ASSOC))
		{
			$employeeLevels[$row['level']] = (int) $row['id'];
		}

		return $employeeLevels;
	}

	// TODO MUST remove direct reports if changing to a level which can't have reports
	function update($employeeId, $firstName, $lastName, $canLogin, $username, $status, $level)
	{
		$statement = $this->db->prepare("update employee
					set firstName = :firstName,
					lastName = :lastName,
					canLogin = :canLogin,
					username = :username,
					status = :status,
					level = :level
					where id = :employeeId");

		$success = $statement->execute(array(
			":firstName" => $firstName,
			":lastName" => $lastName,
			":canLogin" => $canLogin,
			":username" => $username,
			":status" => $status,
			":level" => $level,
			":employeeId" => $employeeId
				));

		return $success;
	}

	function updateProfile($employeeId, $email, $phone, $hired, $terminated, $notes)
	{
		$statement = $this->db->prepare("update employee
					set email = :email,
					phone = :phone,
					hired = :hired,
					`terminated` = :terminated,
					notes = :notes
					where id = :employeeId");

		$fields = array(
			":email" => $email,
			":phone" => $phone,
			":hired" => $hired,
			":terminated" => $terminated,
			":notes" => $notes,
			":employeeId" => $employeeId
		);

		$success = $statement->execute($fields);

		if (!$success)
		{
			Log::error("Couldn't update the employee profile: " . json_encode($statement->errorInfo()));
		}

		return $success;
	}

	function add($firstName, $lastName, $canLogin, $username, $password, $status, $level)
	{
		$statement = $this->db->prepare("insert into employee
					(firstName, lastName, canLogin, username, password, status, level)
					values (
					:firstName,
					:lastName,
					:canLogin,
					:username,
					:password,
					:status,
					:level)");

		$success = $statement->execute(array(
			":firstName" => $firstName,
			":lastName" => $lastName,
			":canLogin" => $canLogin,
			":username" => $username,
			":password" => md5($password),
			":status" => $status,
			":level" => $level
				));

		return $success;
	}

	function changePassword($employeeId, $password)
	{
		$statement = $this->db->prepare("update employee
					set password = :password
					where id = :employeeId");

		$success = $statement->execute(array(
			":password" => md5($password),
			":employeeId" => $employeeId
				));

		return $success;
	}

	/*
	 * This function essentially deletes employees from the system. Reports and
	 * timecard entries will still show employee info, but the employee will not
	 * appear in any new data (similar to inactive, but should not show up in
	 * employee management screens either).
	 * 
	 * TODO Should remove any direct reports when deleting employee
	 */

	function delete($employeeId)
	{
		$deletedEmployeeStatus = $this->employeeStatuses["deleted"];

		$statement = $this->db->prepare("update employee
			set status = $deletedEmployeeStatus
			where id = :employeeId");

		$success = $statement->execute(array(
			":employeeId" => $employeeId
				));

		return $success;
	}

	function uploadFile($file, $employeeId, $title)
	{
		$temporaryPath = $file["tmp_name"];
		$extension = pathinfo($file["name"], PATHINFO_EXTENSION);
		$filename = $employeeId . "_" . time() . "." . $extension;

		$destinationPath = Configuration::getValue("FILES_DIR") . "/employees/" . $filename;

		DB::beginTransaction();

		// Also insert title and path into database
		$statement = DB::instance()->prepare(
				"insert into object_info (`object_table_name`, `object_id`, `type`, `key`, `value`)
				values (:object_table_name, :employeeId, :type, :key, :value)");

		$inserted = $statement->execute(array(
			":object_table_name" => "employee",
			":employeeId" => $employeeId,
			":type" => 3,
			":key" => $title,
			":value" => $filename
				));

		// Log if insert failed
		if (!$inserted)
		{
			Log::error("Couldn't add the file details to the database: " . DB::errorStr());
		}
		else // If insert was successful
		{
			$moved = move_uploaded_file($temporaryPath, $destinationPath);
		}

		// Commit only if insert AND move was successful
		$success = $inserted && $moved;

		DB::endTransaction($success);

		return $success;
	}

	function files($employeeId)
	{
		$statement = DB::instance()->prepare(
				"SELECT `id`, `key`, `value` FROM `object_info`
			WHERE `type` = 3
			AND object_table_name = 'employee'
			AND object_id = :employeeId");

		$statement->execute(array(
			":employeeId" => $employeeId
		));

		$files = $statement->fetchAll(PDO::FETCH_ASSOC);

		return $files;
	}

	function file($filename)
	{
		$path = Configuration::getValue("FILES_DIR") . "/employees/" . filter_var($filename, FILTER_SANITIZE_URL);

		if (file_exists($path))
		{
			header($_SERVER["SERVER_PROTOCOL"] . " 200 OK");
			header("Cache-Control: public"); // needed for i.e.
			header("Content-Type: " . mime_content_type($path));
			header("Content-Transfer-Encoding: Binary");
			header("Content-Length:" . filesize($path));
			header("Content-Disposition: attachment; filename=$filename");
			readfile($path);
		}
		else
		{
			throw new Exception("The requested file could not be found.");
		}
	}

	function deleteFile($filename)
	{
		$path = Configuration::getValue("FILES_DIR") . "/employees/" . filter_var($filename, FILTER_SANITIZE_URL);

		$statement = DB::instance()->prepare(
				"delete from object_info
					where object_table_name = 'employee'
					and `value` = :filename");

		$deleted = $statement->execute(array(
			":filename" => $filename
				));

		if (!$deleted)
		{
			Log::error("Failed to delete file: $filename " . DB::errorStr());
		}
		else
		{
			unlink($path);
		}

		return $deleted;
	}

}

?>
