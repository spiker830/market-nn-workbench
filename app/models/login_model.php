<?php

/* ========================================================================== *
 *
 *
 *
 * 	@author: Jonathan Spiker
 *
 * ========================================================================== */

class Login_Model extends Model
{

	function __construct()
	{
		parent::__construct();
	}

	function run()
	{
		$username = $_POST['username'];
		$password = $_POST['password'];

		$statement = $this->db->prepare("SELECT id FROM employee
			WHERE username = :username
			AND password = :password
			AND canLogin = true");

		$statement->execute(array(
				':username' => $username,
				':password' => md5($password)
		));

		return $statement;
	}

	function getLevel($employeeId)
	{
		$statement = $this->db->prepare("SELECT level FROM employee
			WHERE id = :id");

		$statement->execute(array(
				':id' => $employeeId
		));

		$row = $statement->fetch(PDO::FETCH_ASSOC);

		if ($row)
		{
			$level = $row['level'];
		}
		else
		{
			$level = $this->employeeLevels["user"];
		}

		return $level;

	}

	function getLevelName($employeeId)
	{
		$statement = $this->db->prepare("SELECT employee_level.level
			FROM employee
			LEFT JOIN employee_level on employee.level = employee_level.id
			WHERE employee.id = :id");

		$statement->execute(array(
				':id' => $employeeId
		));

		$row = $statement->fetch(PDO::FETCH_ASSOC);

		if ($row)
		{
			$levelName = $row['level'];
		}
		else
		{
			$levelName = "user";
		}

		return $levelName;

	}

	function getEmployeeName($employeeId)
	{
		$statement = $this->db->prepare("SELECT firstName
			FROM employee
			WHERE employee.id = :id");

		$statement->execute(array(
				':id' => $employeeId
		));

		$row = $statement->fetch(PDO::FETCH_ASSOC);

		if ($row)
		{
			$employeeName = $row['firstName'];
		}

		return $employeeName;

	}

}

?>
