<?php

/* ========================================================================== *
 *
 *
 *
 * 	@author: Jonathan Spiker
 *
 * ========================================================================== */

class Login extends Controller
{
	protected static $public = true;

	function index()
	{
		$this->view->render('login');
	}

	function run()
	{
		Session::init();
		
		$statement = $this->model->run();

		if ($statement->rowCount() > 0)
		{
			Session::set('loggedIn', true);

			$row = $statement->fetch(PDO::FETCH_ASSOC);
			$employeeId = $row['id'];

			Session::set('employeeId', $employeeId);

			Session::set('level', $this->model->getLevel($employeeId));
			Session::set('levelName', $this->model->getLevelName($employeeId));
			Session::set('employeeName', $this->model->getEmployeeName($employeeId));
		}
		else
		{
			Session::set('loggedIn', false);
		}

		header('Location: ' . Configuration::getValue("URL_BASE") . Configuration::getValue("DEFAULT_PAGE"));
	}

}

?>
