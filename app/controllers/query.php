<?php

/* ========================================================================== *
 *
 *
 *
 * 	@author: Jonathan Spiker
 *
 * ========================================================================== */

class Query extends Controller
{
	function index($queryId = null)
	{
		$this->view->queryId = $queryId;
		$this->view->render('query');
	}
}
?>
