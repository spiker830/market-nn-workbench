<?php

/* ========================================================================== *
 *
 *
 *
 * 	@author: Jonathan Spiker
 *
 * ========================================================================== */

class Employees extends Controller
{
	function index()
	{
		// If the user can't view this page, throw exception
		if (!UserLevelPrivilege::authorized("view employee page"))
			throw new Exception("You are not authorized to view the employee page.");

		$this->attachFineUploaderResources();
		
		$this->view->render('employees');
	}

	function getList()
	{
		echo json_encode($this->model->getList());
	}

	function getDetailedList()
	{
		if (!UserLevelPrivilege::authorized("view employee details"))
		{
			echo $this->response(false, "You are not authorized to view employee details");
		}
		else
		{
			echo json_encode($this->model->getDetailedList());
		}
	}

	function currentReports($employeeId = 0)
	{
		if ($employeeId == 0)
		{
			echo $this->response(false, "An employee was not specified in the request");
		}
		else
		{
			$message = $this->model->currentReports($employeeId);

			echo $this->response(true, $message);
		}
	}

	/**
	 * Possible reports are employees who can be a direct report but aren't
	 * already a direct report of the given employee.
	 * 
	 * @param type $employeeId
	 */
	function possibleReports($employeeId = 0)
	{
		if ($employeeId == 0)
		{
			echo $this->response(false, "An employee was not specified in the request");
		}
		else
		{
			// Get all employees who are not a direct report of the given employee
			$notCurrentReports = $this->model->notCurrentReports($employeeId);

			$possibleReports = array();

			// Only include employees who can be a direct report
			foreach ($notCurrentReports as $report)
			{
				if (UserLevelPrivilege::canBeDirectReport($report['levelName']))
				{
					$possibleReports[$report['firstName'] . " " . $report['lastName']] = $report['id'];
				}
			}

			echo $this->response(true, $possibleReports);
		}
	}

	function deleteReport($employeeId = 0, $reportId = 0)
	{
		if ($employeeId == 0 || $reportId == 0)
		{
			echo $this->response(false, "Either an employee id or a direct report id was not specified.");
		}
		else if (!UserLevelPrivilege::authorized('delete direct report'))
		{
			echo $this->response(false, "You are not authorized to delete direct reports.");
		}
		else
		{
			if ($this->model->deleteReport($employeeId, $reportId))
			{
				$message = "The report was successfully removed from this employee";
			}
			else
			{
				$message = "The report could not be removed from this employee";
			}

			echo $this->response(true, $message);
		}
	}

	function addReport($employeeId = 0, $reportId = 0)
	{
		if ($employeeId == 0 || $reportId == 0)
		{
			echo $this->response(false, "Either an employee id or a direct report id was not specified.");
		}
		else if (!UserLevelPrivilege::authorized('add direct report'))
		{
			echo $this->response(false, "You are not authorized to add direct reports.");
		}
		else if (!UserLevelPrivilege::canHaveDirectReport($employeeId))
		{
			echo $this->response(false, "This employee is not able to have direct reports.");
			
		}
		else
		{
			if ($this->model->addReport($employeeId, $reportId))
			{
				$message = "The report was successfully added to this employee";
			}
			else
			{
				$message = "The report could not be added to this employee";
			}

			echo $this->response(true, $message);
		}
	}

	function update($employeeId = 0)
	{
		if (!UserLevelPrivilege::authorized('edit employee'))
		{
			echo $this->response(false, "You are not authorized to edit employees.");
		}
		else
		{
			if ($employeeId == 0)
				echo $this->response(false, "An employee was not specified in the request");
			else
			{
				// Get POST parameters
				$firstName = $_POST['firstName'];
				$lastName = $_POST['lastName'];
				$canLogin = $_POST['canLogin'];
				$username = $_POST['username'];
				$status = $_POST['status'];
				$level = $_POST['level'];

				// Ask model to update employee
				$success = $this->model->update($employeeId, $firstName, $lastName, $canLogin, $username, $status, $level);

				if ($success)
				{
					echo $this->response($success, "The employee was updated successfully");
				}
				else
				{
					echo $this->response($success, "The employee was not updated");
				}
			}
		}
	}

	function updateProfile($employeeId = 0)
	{
		if (!UserLevelPrivilege::authorized('edit employee'))
		{
			echo $this->response(false, "You are not authorized to edit employees.");
		}
		else
		{
			if ($employeeId == 0)
				echo $this->response(false, "An employee was not specified in the request");
			else
			{
				// Get POST parameters
				$email = $_POST['email'];
				$phone = $_POST['phone'];
				$hired = $_POST['hired'];
				$terminated = $_POST['terminated'];
				$notes = $_POST['notes'];

				// Ask model to update employee
				$success = $this->model->updateProfile($employeeId, $email, $phone, $hired, $terminated, $notes);

				if ($success)
				{
					echo $this->response($success, "The employee was updated successfully");
				}
				else
				{
					echo $this->response($success, "The employee was not updated");
				}
			}
		}
	}

	function getLevels()
	{

		echo json_encode($this->model->getLevels());
	}

	function add()
	{
		if (!UserLevelPrivilege::authorized('add employee'))
		{
			echo $this->response(false, "You are not authorized to add employees.");
		}
		else
		{
			// Get POST parameters
			$firstName = $_POST['firstName'];
			$lastName = $_POST['lastName'];
			$canLogin = $_POST['canLogin'];
			$username = $_POST['username'];
			$password = $_POST['password'];
			$status = $_POST['status'];
			$level = $_POST['level'];

			// Ask model to add employee
			$success = $this->model->add($firstName, $lastName, $canLogin, $username, $password, $status, $level);

			if ($success)
			{
				echo $this->response($success, "The employee was added successfully");
			}
			else
			{
				echo $this->response($success, "The employee was not added");
			}
		}
	}

	function changePassword($employeeId = 0)
	{
		if ($employeeId == 0)
		{
			echo $this->response(false, "An employee was not specified in the request");
		}
		else if (!UserLevelPrivilege::authorized("change passwords"))
		{
			echo $this->response(false, "You are not authorized to change passwords");
		}
		else
		{
			// Get POST parameters
			$password = $_POST['password'];

			// Ask model to change the password
			$success = $this->model->changePassword($employeeId, $password);

			if ($success)
			{
				echo $this->response($success, "The password was changed");
			}
			else
			{
				echo $this->response($success, "The password was not changed");
			}
		}
	}

	function delete($employeeId = 0)
	{
		if ($employeeId == 0)
		{
			echo response(false, "An employee was not specified in the request");
		}
		else if (!UserLevelPrivilege::authorized("delete employee"))
		{
			echo $this->response(false, "You are not authorized to delete employees.");
		}
		else
		{
			// Ask model to change the password
			$success = $this->model->delete($employeeId);

			if ($success)
			{
				echo $this->response($success, "The employee was deleted");
			}
			else
			{
				echo $this->response($success, "The employee was not deleted");
			}
		}
	}

	function uploadFile()
	{
		$file = $_FILES["qqfile"];
		
		$employeeId = $_POST["employeeId"];
		$title = $_POST["title"];
		
		$success = $this->model->uploadFile($file, $employeeId, $title);
		
		$message = $success ? "The file was successfully uploaded" : "The file was not uploaded";
		
		echo $this->response($success, $message);
	}

	function files($employeeId = 0)
	{
		if (!UserLevelPrivilege::authorized('view employee files'))
		{
			echo $this->response(false, "You are not authorized to view employee files.");
		}
		else
		{
			// Get the list of files for the given employee
			echo $this->response(true, $this->model->files($employeeId));
		}
	}

	function file($filename)
	{
		if (!UserLevelPrivilege::authorized('view employee files'))
		{
			throw new Exception("You are not authorized to view employee files.");
		}
		else
		{
			$this->model->file($filename);
		}
	}
	
	function deleteFile($filename)
	{
		if (!UserLevelPrivilege::authorized('delete employee files'))
		{
			echo $this->response(false, "You are not authorized to delete employee files.");
		}
		else
		{
			$deleted = $this->model->deleteFile($filename);
			
			$message = $deleted ? "The file was successfully deleted" : "The file was not deleted";
			
			echo $this->response($deleted, $message);
		}
	}

}

?>
