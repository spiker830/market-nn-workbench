<?php

/* ========================================================================== *
 *
 *
 *
 * 	@author: Jonathan Spiker
 *
 * ========================================================================== */

class Analyzer_Data extends RESTfulController {

	protected static $viewPrivilege = "view analyzers";
	protected static $editPrivilege = "edit analyzers";

}

?>
