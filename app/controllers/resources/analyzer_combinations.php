<?php

/* ========================================================================== *
 *
 *
 *
 * 	@author: Jonathan Spiker
 *
 * ========================================================================== */

class Analyzer_Combinations extends RESTfulController {

	protected static $viewPrivilege = "view analyzers";

	public function rest_get($resourceId = null, $filters = null) {

		if (isset($resourceId)) {

			parent::rest_get($resourceId, $filters);
		}
		else {

			$resources = array();
			$resourceId = 1;

			$combinations = DB::select("select
				count(*) as count, base_gain, sum(if(gain-base_gain > 0 and gain !=1, 1, 0)) as winning, target_symbol, analyzers, short_name
				from job
				left join job_trading_strategy on job_trading_strategy.job_id = job.id
				left join trading_strategy on trading_strategy.id = job_trading_strategy.trading_strategy_id
				group by target_symbol, analyzers, short_name
				order by count desc");
			
			// For each combination which has run
			foreach ($combinations as $combination) {
				
				$count_all = $combination["count"];
				$count_winning = $combination["winning"];
				$winning_ratio = 0;
				
				if ($count_all != 0) {
					$winning_ratio = $count_winning / $count_all * 100;
				}
				
				$resource = array(
					"id" => $resourceId++,
					"analyzers" => $combination["analyzers"],
					"target" => $combination["target_symbol"],
					"strategy" => $combination["short_name"],
					"count_all" => $count_all,
					"count_winning" => $count_winning,
					"winning_ratio" => $winning_ratio
				);
				
				$resources[] = $resource;
			}
			
			// Obtain a list of columns
			foreach ($resources as $key => $resource) {
				$count[$key]  = $resource["count_all"];
				$winning[$key] = $resource["winning_ratio"];
			}
			
			// Sort by winning ratio, then by overall count
			array_multisort($winning, SORT_DESC, $count, SORT_DESC, $resources);
			
			Response::sayJSON(json_encode($resources, JSON_NUMERIC_CHECK));
		}
	}

}

?>
