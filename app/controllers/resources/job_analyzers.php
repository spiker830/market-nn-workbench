<?php

/* ========================================================================== *
 *
 *
 *
 * 	@author: Jonathan Spiker
 *
 * ========================================================================== */

class Job_Analyzers extends RESTfulController {

	protected static $viewPrivilege = "view analyzers";

	public function rest_get($resourceId = null, $filters = null) {

		if (isset($resourceId)) {

			parent::rest_get($resourceId, $filters);
		}
		else {

			$resources = array();
			$resourceId = 1;

			$analyzer_usages = DB::select("
				select * from (
				select
					count(*) as count,
					sum(if(base_gain > 1, 1, 0)) as bull_count,
					sum(if(gain-base_gain > 0 and gain !=1, 1, 0)) as all_winning,
					sum(if(gain-base_gain > 0 and gain !=1 and base_gain > 1, 1, 0)) as bull_winning,
					short_name,
					analyzer_id,
					data,
					target_symbol,
					analyzer.name
				from job
				left join job_analyzer on job_analyzer.job_id = job.id
				left join analyzer on job_analyzer.analyzer_id = analyzer.id
				left join job_trading_strategy on job_trading_strategy.job_id = job.id
				left join trading_strategy on job_trading_strategy.trading_strategy_id = trading_strategy.id
				group by short_name, analyzer_id, data, target_symbol
				) as counts
				order by bull_winning/bull_count desc, all_winning/count desc
				limit 100");
			
			foreach($analyzer_usages as $analyzer_usage) {
				
				$count_all = $analyzer_usage["count"];
				$count_bull = $analyzer_usage["bull_count"];
				$all_winning = $analyzer_usage["all_winning"];
				$bull_winning = $analyzer_usage["bull_winning"];
				$all_winning_ratio = 0;
				$bull_winning_ratio = 0;
				
				if ($count_all != 0) {
					$all_winning_ratio = $all_winning / $count_all * 100;
				}
				
				if ($count_bull != 0) {
					$bull_winning_ratio = $bull_winning / $count_bull * 100;
				}
				
				$resource = array(
					"id" => $resourceId++,
					"name" => $analyzer_usage["name"],
					"target" => $analyzer_usage["target_symbol"],
					"data" => $analyzer_usage["data"],
					"strategy" => $analyzer_usage["short_name"],
					"count_all" => $count_all,
					"all_winning" => $all_winning,
					"bull_winning" => $bull_winning,
					"all_winning_ratio" => $all_winning_ratio,
					"bull_winning_ratio" => $bull_winning_ratio
				);
				
				$resources[] = $resource;
			}
			
//			// Obtain a list of columns
//			foreach ($resources as $key => $resource) {
//				$all_winning_ratios[$key] = $resource["all_winning_ratio"];
//				$bull_winning_ratios[$key] = $resource["bull_winning_ratio"];
//			}
//			
//			// Sort by winning ratio, then by overall count
//			array_multisort($bull_winning_ratios, SORT_DESC, $all_winning_ratios, SORT_DESC, $resources);
			
			Response::sayJSON(json_encode($resources, JSON_NUMERIC_CHECK));
		}
	}

}

?>
