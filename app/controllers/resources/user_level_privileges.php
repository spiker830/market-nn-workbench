<?php

/* ========================================================================== *
 *
 *
 *
 * 	@author: Jonathan Spiker
 *
 * ========================================================================== */

class User_Level_Privileges extends Controller
{
	static $viewPrivilege = "view privileges";
	static $editPrivilege = "edit privileges";
	
	function rest_get($privilegeId = null, $filters = array())
	{
		// First filter is the one from the nesting
		$levelId = isset($filters[0]) ? $filters[0]->getValue() : null;
		
		// If privilege is specified
		if (isset($privilegeId))
		{
			// Get single privilege
			Response::json(UserLevelPrivilege::findByLevel($levelId, $privilegeId));
		}
		else
		{
			// Get all privileges for level
			$privileges = UserLevelPrivilege::allByLevel($levelId);
			echo UserLevelPrivilege::objectsToJSON($privileges);
		}
	}

	function rest_put($privilegeId, $filters = array())
	{
		// First filter is the one from the nesting
		$levelId = isset($filters[0]) ? $filters[0]->getValue() : null;
		
		// Get phone
		$privilege = UserLevelPrivilege::findByLevel($levelId, $privilegeId);

		// Update
		$input = Input::json();

		$privilege->update(array(
			"granted" => $input["granted"]
		));

		// Save
		if ($privilege->save())
		{
			echo $privilege->json();
		}
		else
		{
			throw new RESTException("Privilege was not updated", HTTP::HTTP_INTERNAL_SERVER_ERROR);
		}
	}

	function rest_post($filters = array())
	{
		throw new RESTException("Privileges can only be added by the development team.", HTTP::HTTP_FORBIDDEN);
	}
	
	function rest_delete($privilegeId, $filters = array())
	{
		throw new RESTException("Privileges can only be removed by the development team.", HTTP::HTTP_FORBIDDEN);
	}

}

?>
