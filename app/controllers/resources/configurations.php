<?php

/* ========================================================================== *
 *
 *
 *
 * 	@author: Jonathan Spiker
 *
 * ========================================================================== */

class Configurations extends RESTfulController
{
	public function rest_get($configId = null)
	{
		// If specifying a group, give subset
		if ($configId == "frontend")
		{
			Response::json(Configuration::getConfigurationObject());
		}
		else
		{
			parent::rest_get($configId);
		}
	}

	public function rest_post()
	{
		// Shouldn't be able to add new config values for now, only edit existing ones
		throw new RESTException("Must use PUT to edit a configuration. POST is not supported", HTTP::HTTP_METHOD_NOT_ALLOWED);
	}

	public function rest_delete($configId)
	{
		// Shouldn't be able to delete config values for now, only edit existing ones
		throw new RESTException("Must use PUT to edit a configuration. DELETE is not supported", HTTP::HTTP_METHOD_NOT_ALLOWED);
	}
}

?>
