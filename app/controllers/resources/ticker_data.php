<?php

/* ========================================================================== *
 *
 *
 *
 * 	@author: Jonathan Spiker
 *
 * ========================================================================== */

class Ticker_Data extends RESTfulController
{
	protected static $viewPrivilege = "view jobs";
	protected static $editPrivilege = "edit jobs";

	public function rest_get($resourceId = null, $filters = null) {
		
		// Only care if the job_id was supplied
		foreach ($filters as $index => $filter) {
			
			if ($filter->getAttribute() == "population_genome_id") {
				
				// Get ticker for this genome
				$results = DB::select("select ticker.symbol
					from population
					left join population_genome on population_genome.population_id = population.id
					left join ticker on ticker.id = population.ticker_id
					where population_genome.id = :genomeId",
						array(":genomeId" => $filter->getValue()));
				
				if ($results) {
					
					$symbol = strtolower($results[0]["symbol"]);
					
//					unset($filters[$index]);

					$tickerData = DB::select("select * from $symbol "
							. "where date > '2016-01-01' "
							. "and date < now()");
					
					$objects = DBObject::rowsToDBObjects($tickerData, "TickerData");
					
					Response::sayJSON(DBObject::objectsToJSON($objects));
					
					return;
				}
			}
		}
		
		Response::say("Request must include a genome id", HTTP::HTTP_BAD_REQUEST);
	}
}

?>
