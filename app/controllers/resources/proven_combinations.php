<?php

/* ========================================================================== *
 *
 *
 *
 * 	@author: Jonathan Spiker
 *
 * ========================================================================== */

class Proven_Combinations extends RESTfulController {

	protected static $viewPrivilege = "view analyzers";

	public function rest_get($resourceId = null, $filters = null) {

		if (isset($resourceId)) {

			parent::rest_get($resourceId, $filters);
		}
		else {

			$resources = array();
			$resourceId = 1;

			$combinations = DB::select("
				select * from
				(
					select
						count(*) as count,
						sum(if(base_gain > 1, 1, 0)) as bull_count,
						sum(if(gain-base_gain > 0 and gain !=1, 1, 0)) as all_winning,
						sum(if(gain-base_gain > 0 and gain !=1 and base_gain > 1, 1, 0)) as bull_winning,
						target_symbol,
						analyzers,
						short_name
					from job
					left join job_trading_strategy on job_trading_strategy.job_id = job.id
					left join trading_strategy on trading_strategy.id = job_trading_strategy.trading_strategy_id
					group by target_symbol, analyzers, short_name
					order by count desc
				) as counts
				where all_winning/count > .4
				or bull_winning/bull_count > .4");
			
			// For each combination which has run
			foreach ($combinations as $combination) {
				
				$count_all = $combination["count"];
				$count_bull = $combination["bull_count"];
				$all_winning = $combination["all_winning"];
				$bull_winning = $combination["bull_winning"];
				$all_winning_ratio = 0;
				$bull_winning_ratio = 0;
				
				if ($count_all != 0) {
					$all_winning_ratio = $all_winning / $count_all * 100;
				}
				
				if ($count_bull != 0) {
					$bull_winning_ratio = $bull_winning / $count_bull * 100;
				}
				
				$resource = array(
					"id" => $resourceId++,
					"analyzers" => $combination["analyzers"],
					"target" => $combination["target_symbol"],
					"strategy" => $combination["short_name"],
					"count_all" => $count_all,
					"all_winning" => $all_winning,
					"bull_winning" => $bull_winning,
					"all_winning_ratio" => $all_winning_ratio,
					"bull_winning_ratio" => $bull_winning_ratio
				);
				
				$resources[] = $resource;
			}
			
			// Obtain a list of columns
			foreach ($resources as $key => $resource) {
//				$count[$key]  = $resource["count_all"];
				$all_winning_ratios[$key] = $resource["all_winning_ratio"];
				$bull_winning_ratios[$key] = $resource["bull_winning_ratio"];
			}
			
			// Sort by winning ratio, then by overall count
			array_multisort($bull_winning_ratios, SORT_DESC, $all_winning_ratios, SORT_DESC, $resources);
			
			Response::sayJSON(json_encode($resources, JSON_NUMERIC_CHECK));
		}
	}

}

?>
