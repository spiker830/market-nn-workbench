<?php

/* ========================================================================== *
 *
 *
 *
 * 	@author: Jonathan Spiker
 *
 * ========================================================================== */

class Table_Data extends RESTfulController
{
	protected static $viewPrivilege = "view jobs";
	protected static $editPrivilege = "edit jobs";

	public function rest_get($resourceId = null, $filters = null) {
		
		$resources = [];
		
		foreach ($filters as $index => $filter) {
			
			if ($filter->getAttribute() == "table") {
				
				$table = $filter->getValue();
				unset($filters[$index]);
				
				$resources = DBObject::filterTable($table, $filters);
				break;
			}
		}
		
		Response::sayJSON(json_encode($resources));
	}
}

?>
