<?php

/* ========================================================================== *
 *
 *
 *
 * 	@author: Jonathan Spiker
 *
 * ========================================================================== */

class Query extends RESTfulController
{
	protected static $viewPrivilege = "view jobs";
	protected static $editPrivilege = "edit jobs";

	public function rest_get($resourceId = null, $filters = null) {
		
		$resources = [];
		
		foreach ($filters as $index => $filter) {
			
			if ($filter->getAttribute() == "query") {
				
				$statement = DB::instance()->query($filter->getValue());
				
				// Will want to log any error
				if (!$statement)
				{
					Log::error(json_encode(DB::errorStr()));
				}
				else
				{
					$resources = $statement->fetchAll(PDO::FETCH_ASSOC);
				}
			}
		}
		
		Response::sayJSON(json_encode($resources, JSON_NUMERIC_CHECK));
	}
}

?>
