<?php

/* ========================================================================== *
 *
 *
 *
 * 	@author: Jonathan Spiker
 *
 * ========================================================================== */

class Jobs extends RESTfulController
{
	public function rest_get($resourceId = null, $filters = null) {
		
		
		foreach ($filters as $index => $filter) {
			
			if ($filter->getAttribute() == "trading_strategy") {
				
				unset($filters[$index]);
				
				$filters[] = new Filter('trading_strategy.short_name', '=', $filter->getValue());
			}
		}
		
		parent::rest_get($resourceId, $filters);
	}

}

?>
