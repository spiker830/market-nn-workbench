<?php

/* ========================================================================== *
 *
 *
 *
 * 	@author: Jonathan Spiker
 *
 * ========================================================================== */

class Data extends RESTfulController
{
	protected static $viewPrivilege = "view jobs";
	protected static $editPrivilege = "edit jobs";

	public function rest_get($resourceId = null, $filters = null) {
		
		$resources = [];
		
		$query = DB::buildQuery($filters);
		
		Log::debug("Attempting query: $query");
		
		$statement = DB::instance()->query($query);
		
		// Will want to log any error
		if (!$statement)
		{
			Log::error(json_encode(DB::errorStr()));
		}
		else
		{
			$resources = $statement->fetchAll(PDO::FETCH_ASSOC);
		}
		
		Response::sayJSON(json_encode($resources));
	}
}

?>
