<?php

/* ========================================================================== *
 *
 *
 *
 * 	@author: Jonathan Spiker
 *
 * ========================================================================== */

class Data_Tables extends RESTfulController
{
	static $viewPrivilege = "view analyzers";
	
	public function rest_get($resourceId = null, $filters = null)
	{
		Log::debug("Filters in the rest_get method: " . json_encode($filters));
		
		$resourceName = $this->getDBObjectName();
		$pluralResourceName = str_replace("_", " ", get_called_class());
		
		if (isset($resourceId))
		{
			// Get single resource
			$resource = $resourceName::find($resourceId);
			
			// If only getting single resource
			if (isset($resource))
			{
				Response::sayJSON($resource->json());
			}
			// If single resource didn't exist
			else
			{
				throw new RESTException("Couldn't find any $pluralResourceName with an id of " . $resourceId, HTTP::HTTP_NOT_FOUND);
			}
		}
		else
		{
			// Get all resources
			$resources = $resourceName::all($filters);
			
			$tickerDataTable = new DataTable();
			$tickerDataTable->name = "TICKER";
			
			$resources[] = $tickerDataTable;
			
			Response::sayJSON(DBObject::objectsToJSON($resources));
		}
	}
}

?>
