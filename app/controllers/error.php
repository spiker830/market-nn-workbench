<?php

/* ========================================================================== *
 *
 *
 *
 * 	@author: Jonathan Spiker
 *
 * ========================================================================== */

class Error extends Controller
{
	protected static $public = true;

	function index($msg = 'An unknown error occurred.')
	{
		Response::code(HTTP::HTTP_INTERNAL_SERVER_ERROR);
		
		$this->view->msg = $msg;

		$this->view->render('error');
	}

}
?>
