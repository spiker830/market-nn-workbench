<?php

/* ========================================================================== *
 *
 *
 *
 * 	@author: Jonathan Spiker
 *
 * ========================================================================== */

class Logout extends Controller
{
	protected static $public = true;

	function index()
	{
		Log::debug("Destroying current session");
		
		Session::destroy();

		Session::redirectToLogin();
	}

}

?>
