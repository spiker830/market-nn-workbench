<?php

/* ========================================================================== *
 *
 *
 *
 * 	@author: Jonathan Spiker
 *
 * ========================================================================== */

class Analyzers extends Controller
{
	function index()
	{
		$this->view->render('analyzers');
	}
	
	function analyzer_output($id)
	{
		$this->view->analyzerId = $id;
		$this->view->render('analyzer_output');
	}
}
?>
