<?php

/* ========================================================================== *
 *
 *
 *
 * 	@author: Jonathan Spiker
 *
 * ========================================================================== */

class Administration extends Controller
{
	function index()
	{
		// If the user can't view this page, throw exception
		if (!UserLevelPrivilege::authorized("view administration page"))
			throw new Exception("You are not authorized to view the administration page.");
		
		// Render view
		$this->view->render('administration');
	}
}

?>
