<?php

/* ========================================================================== *
 *
 *
 *
 * 	@author: Jonathan Spiker
 *
 * ========================================================================== */

class Genomes extends Controller
{
	function index($filters)
	{
		foreach($filters as $filter) {
			
			if ($filter->getAttribute() === "analyzers") {
				$this->view->analyzers = $filter->getValue();
			}
			
			if ($filter->getAttribute() === "target_symbol") {
				$this->view->targetSymbol = $filter->getValue();
			}
			
			if ($filter->getAttribute() === "trading_strategy") {
				$this->view->tradingStrategy = $filter->getValue();
			}
		}
		
		$this->view->render('genomes');
	}
	
	function error_graph($id)
	{
		$this->view->jobId = $id;
		$this->view->render('error_graph');
	}
	
	function data_graph($id)
	{
		$this->view->genomeId = $id;
		$this->view->render('data_graph');
	}
}
?>
