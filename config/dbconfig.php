<?php

/* ========================================================================== *
 *
 *	Database configuration
 *
 * 	@author: Jonathan Spiker
 *
 * ========================================================================== */

define('DB_TYPE', 'mysql');
define('DB_HOST', '192.168.0.13');
define('DB_NAME', 'market_prediction');
define('DB_USER', 'root');
define('DB_PASS', 'root');

?>
